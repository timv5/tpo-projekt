from django.db import models

# Create your models here.
class TemporaryBanIPModel(models.Model):
    ip_address = models.GenericIPAddressField()
    attempts = models.IntegerField(default=0)
    time_unblock = models.DateTimeField(blank=True,null=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.ip_address