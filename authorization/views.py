from django.utils import timezone
from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.shortcuts import redirect
from django.contrib import messages

import requests
from .models import TemporaryBanIPModel
from Studis.models import User
from django.contrib.auth.models import Group

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def user_login(request):
    """ if request.user.is_authenticated:
        #students = Group.objects.get(name="students").user_set.all()
        #if request.user in students:
        #    print(request.user)

        user_group = request.user.groups.values_list('name',flat=True).get()
        if user_group == 'students':
            return redirect('student_list')
        elif user_group == 'referent':
            return redirect('student_list')
        elif user_group == 'professors':
            return redirect('student_list')
    else: """
    if request.method == 'POST':
        if request.user.is_authenticated:
            return redirect('login_success')
        client_ip = get_client_ip(request)
        obj, _ = TemporaryBanIPModel.objects.get_or_create(
            defaults={
                'ip_address': client_ip
            },
            ip_address=client_ip
        )
        # if an IP is locked and unlocking time has not come
        if obj.status is True and obj.time_unblock > timezone.now():
            time_diff = obj.time_unblock - timezone.now()
            message = "'{}' IP blocked. Too many invalid attempts. Wait {}s".format(client_ip,time_diff)
            messages.warning(request, message)
            return render(request, 'login.html')
        elif obj.status is True and obj.time_unblock < timezone.now():
            # if the IP is blocked, but the release time has come, then unlock IP
            obj.status = False
            obj.attempts = 0
        email = request.POST.get('email',None)
        password = request.POST.get('password',None)
        user = authenticate(request,email=email,password=password)
        if user is not None:
            login(request,user)
            if obj is not None:
                obj.delete()
            return redirect('login_success')
        else:
            obj.attempts +=1
            if obj.attempts >= 3:
                obj.time_unblock = timezone.now() + timezone.timedelta(seconds=30)
                obj.status = True
            obj.save()
            messages.warning(request, 'Incorrect email or password.')
        return render(request, 'login.html')
    else:
        if request.user.is_authenticated:
            return redirect('login_success')
        return render(request,'login.html')

def login_success(request):
    """
    Redirects users based on whether they are in the admins group
    """
    if request.user.groups.filter(name="student").exists():
        return redirect('subject_list')
    elif request.user.groups.filter(name='profesor').exists():
        return redirect("student_list")
    else:
        return redirect("student_list")