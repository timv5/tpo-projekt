from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path
from . import views as my_auth_views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('',my_auth_views.user_login, name='login'), # Stran na katero oddaš POST request da se logiraš
    url('logout/$', auth_views.logout, {'next_page': '/'}, name="logout"),
    url(r'login_success/$', my_auth_views.login_success, name='login_success'),
    url(r'^password_reset/$', auth_views.password_reset, {'template_name': 'pwd_reset_form.html'},name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, {'template_name':'pwd_reset_done.html'},name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, {'template_name':'pwd_reset_confirm.html'},name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, {'template_name':'pwd_reset_complete.html'},name='password_reset_complete'),
    url(r'^accounts/login/$', my_auth_views.user_login, name="not_logged_in"),
]

urlpatterns = format_suffix_patterns(urlpatterns)