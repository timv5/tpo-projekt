from django.http.response import Http404
from django.contrib.auth import get_user_model


def is_osebje(user):
    if user.is_superuser:
        return True
    if user.groups.filter(name__in=["referent", "profesor", "skrbnik"]).exists():
        return True
    raise Http404('Not authorized!')

def is_student(user):
    if user.groups.filter(name__in=["student"]).exists():
        return True
    raise Http404('Not authorized!')

def is_referent(user):
    if user.is_superuser:
        return True
    if user.groups.filter(name__in=["referent"]).exists():
        return True
    raise Http404('Not authorized!')

def is_profesor(user):
    if user.is_superuser:
        return True
    if user.groups.filter(name__in=["profesor"]).exists():
        return True
    raise Http404('Not authorized!')