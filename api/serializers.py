from django.conf.urls import url, include
from rest_framework import routers, serializers, viewsets
from Studis.models import *

class StudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Student
        fields = '__all__'

class UrejanjeStudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Student
        fields = ('first_name', 'last_name','emso','datum_rojstva',
                  'drzavljanstvo','tel','ulica_prejem','posta_prejem')


class ProfesorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profesor
        fields = '__all__'


class SplosniPredmetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Splosni_Predmet
        fields = '__all__'

class ModulskiPredmetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Modulski_Predmet
        fields = '__all__'


class SplosnoIzbirniPredmetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Splosno_izbirni
        fields = '__all__'


class StrokovnoIzbirniPredmetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Strokovni_izbirni
        fields = '__all__'


class PredmetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Predmet
        fields = '__all__'

class CoinSerializer(serializers.ModelSerializer):

    class Meta:
        model = Coin
        fields = '__all__'

class VpisSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vpis
        fields = '__all__'

class IzpitSerializer(serializers.ModelSerializer):

    class Meta:
        model = Izpit
        fields = '__all__'