from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    url(r'^students/$', views.StudentList.as_view(), name="api_student_list"),
    url(r'^students/(?P<pk>[0-9]+)/$', views.StudentDetail.as_view(), name="api_student_detail"),
    url(r'^profesors/$', views.ProfesorList.as_view(), name="api_profesor_list"),
    url(r'^profesors/(?P<pk>[0-9]+)/$', views.ProfesorDetail.as_view(), name="api_profesor_detail"),
    url(r'^subjects/$', views.PredmetList.as_view(), name="api_subject_list"),
    url(r'^subjects/(?P<pk>[0-9]+)/$', views.PredmetDetail.as_view(), name="api_subject_detail"),
    url(r'^splosni_predmeti/$', views.SplosniPredmetList.as_view(), name="api_splosni_predmeti_list"),
    url(r'^splosni_predmeti/(?P<pk>[0-9]+)/$', views.SplosniPredmetDetail.as_view(), name="api_splosni_predmeti_detail"),
    url(r'^modulski_predmeti/$', views.ModulskiPredmetList.as_view(), name="api_modulski_predmeti_list"),
    url(r'^modulski_predmeti/(?P<pk>[0-9]+)/$', views.ModulskiPredmetDetail.as_view(), name="api_modulski_predmeti_detail"),
    url(r'^strokovni_predmeti/$', views.StrokovnoIzbirniPredmetList.as_view(), name="api_strokovni_predmeti_list"),
    url(r'^strokovni_predmeti/(?P<pk>[0-9]+)/$', views.StrokovnoIzbirniPredmetDetail.as_view(), name="api_strokovni_predmeti_detail"),
    url(r'^splosno_izb_predmeti/$', views.SplosnoIzbirniPredmetList.as_view(), name="api_splosno_izb_predmeti_list"),
    url(r'^splosno_izb_predmeti/(?P<pk>[0-9]+)/$', views.SplosnoIzbirniPredmetDetail.as_view(), name="api_splosno_izb_predmeti_detail"),
    url(r'^vpisi_studenta/(?P<pk>[0-9]+)/$', views.VpisiStudentaDetail.as_view(), name='vpisi_studenta_detail'),
    url(r'^zetoni_studenta/(?P<pk>[0-9]+)/$', views.CoinStudentDetail.as_view(), name='zetoni_studenta_detail'),

]

urlpatterns = format_suffix_patterns(urlpatterns)