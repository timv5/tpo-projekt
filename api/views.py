from rest_framework import status
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from Studis.models import *
from api.serializers import *
from rest_framework import generics


class StudentList(generics.ListCreateAPIView):
    """
    Seznam studentov
    REST storitev za kreiranje studenta
    """
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class StudentDetail(APIView):

    def get_object(self, pk):
        try:
            return Student.objects.get(pk=pk)
        except Student.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        student = self.get_object(pk)
        serializer = StudentSerializer(student)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        student = self.get_object(pk)
        serializer = UrejanjeStudentSerializer(student, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        student = self.get_object(pk)
        student.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ProfesorList(generics.ListCreateAPIView):
    """
    Seznam vseh profesorjev

    REST storitev za kreiranje profesorja
    """

    queryset = Profesor.objects.all()
    serializer_class = ProfesorSerializer

class ProfesorDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Profesor.objects.all()
    serializer_class = ProfesorSerializer


class PredmetList(generics.ListCreateAPIView):
    """
    Seznam vseh predmetov

    REST storitev za kreiranje predmeta
    """

    queryset = Predmet.objects.all()
    serializer_class = PredmetSerializer

class PredmetDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Predmet.objects.all()
    serializer_class = PredmetSerializer


class SplosniPredmetList(generics.ListCreateAPIView):
    """
    Seznam vseh splosnih predmetov

    REST storitev za kreiranje splosnih predmeta
    """

    queryset = Splosni_Predmet.objects.all()
    serializer_class = SplosniPredmetSerializer

class SplosniPredmetDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Splosni_Predmet.objects.all()
    serializer_class = SplosniPredmetSerializer

class ModulskiPredmetList(generics.ListCreateAPIView):
    """
    Seznam vseh modulskih predmetov

    REST storitev za kreiranje modulskega predmeta
    """

    queryset = Modulski_Predmet.objects.all()
    serializer_class = ModulskiPredmetSerializer

class ModulskiPredmetDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Modulski_Predmet.objects.all()
    serializer_class = ModulskiPredmetSerializer


class SplosnoIzbirniPredmetList(generics.ListCreateAPIView):
    """
    Seznam vseh splosno izbirnih predmetov

    REST storitev za kreiranje splosno izbernega predmeta
    """

    queryset = Splosno_izbirni.objects.all()
    serializer_class = SplosnoIzbirniPredmetSerializer

class SplosnoIzbirniPredmetDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Splosno_izbirni.objects.all()
    serializer_class = SplosnoIzbirniPredmetSerializer


class StrokovnoIzbirniPredmetList(generics.ListCreateAPIView):
    """
    Seznam vseh strokovno izbirnih predmetov

    REST storitev za kreiranje splosno izbernega predmeta
    """

    queryset = Strokovni_izbirni.objects.all()
    serializer_class = StrokovnoIzbirniPredmetSerializer

class StrokovnoIzbirniPredmetDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Strokovni_izbirni.objects.all()
    serializer_class = StrokovnoIzbirniPredmetSerializer

""" class IzpitAddView(APIView):
    def post(self, request, format=None):
        serializer = IzpitSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST) """

class CoinStudentDetail(APIView):
    def get_object(self,pk):
        try:
            return Student.objects.get(pk=pk).coin_set.filter(used = False).latest('id')
        except Coin.DoesNotExist:
            raise Http404
        
    def get(self,request,pk,format=None):
        coin = self.get_object(pk)
        serializer = CoinSerializer(coin)
        return Response(serializer.data)

    def delete(self, request, pk, format=None):
        coin = self.get_object(pk)
        coin.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, pk, format=None):
        coin = self.get_object(pk)
        serializer = CoinSerializer(coin, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class VpisiStudentaDetail(APIView):
    def get_object(self,pk):
        try:
            return Student.objects.get(pk=pk).vpis_set.latest('id')
        except Vpis.DoesNotExist:
            raise Http404
    
    def get(self,request,pk,format=None):
        vpisi = self.get_object(pk)
        serializer = VpisSerializer(vpisi)
        return Response(serializer.data)