$(document).ready(function () {

    // Najbl nagravžna koda, ki sem jo sproduciral. Ne preveč spreminjat xD

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');
    //console.log(csrftoken);
    var student_id = null;
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    });


    //______________________________Logika za žetode ipd.______________________
    //_________________________________________________________________________
    //_________________________________________________________________________
    eventTarget = null;
    // Funkcija za loada-nje žeton podatkov glede na zadnjega vpisa
    $('#add_zeton_modal').on('show.bs.modal', function (event) {
        eventTarget = $(event.relatedTarget);
        var student_id = $(event.relatedTarget).data('id');
        var url = '/api/vpisi_studenta/' + student_id;
        $("#add_zeton_form").attr("action", "osebje/add_zeton/" + student_id + "/");
        $.get(url, function () {})
            .done(function (response) {
                data = response;
                $("#id_smer").val(data['smer']);
                var letnik = parseInt(data['letnik'])
                console.log(letnik)
                if (letnik < 3) {
                    letnik++;
                }
                $("#id_pravica_izbire").css("display", "block");
                $("#pravicaLabel").css("display", "block");
                $("#id_letnik").val(letnik);
                $("#id_vrsta").val(data['vrsta']);
                $("#id_nacin").val(data['tip']);
                $("#id_oblika").val(data['oblika']);
                $("#id_oblika option")[0].selected = true;
            })
            .fail(function () {
                $("#id_smer option")[0].selected = true;
                $('#id_letnik option')[0].selected = true;
                $("#id_letnik option")[0].selected = true;
                $("#id_vrsta option")[0].selected = true;
                $("#id_nacin option")[0].selected = true;
                $("#id_oblika option")[0].selected = true;
                $("#pravicaLabel").css("display", "none");
                $("#id_pravica_izbire").css("display", "none");
            });
    });
    // _____________________________________________________
    $("#id_letnik").change(function () {
        $this = $(this);
        if ($this.val() === "3") {
            $("#id_pravica_izbire").css("display", "block");
            $("#pravicaLabel").css("display", "block");
        } else {
            $("#pravicaLabel").css("display", "none");
            $("#id_pravica_izbire").css("display", "none");
        }
    });
    //
    $('#add_zeton_form').submit(function (event) {
        event.preventDefault();
        $.ajax({
            data: $(this).serialize(),
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            success: function (data) {
                $('#add_zeton_modal').modal('hide');
                $('#zeton_modal_response_success').modal('show');
                $(eventTarget).attr("data-target", "#edit_zeton_modal");
                $(eventTarget).text("Uredi žeton");
            },
            error: function (data) {
                $('#add_zeton_modal').modal('hide');
                $('#zeton_modal_response_failure').modal('show');
            }
        });
        return false;
    });

    $("[id=id_letnik]:eq(1)").change(function () {
        $this = $(this);
        if ($this.val() === "3") {
            $("[id=id_pravica_izbire]:eq(1)").css("display", "block");
            $("[id=pravicaLabel]:eq(1)").css("display", "block");
        } else {
            $("[id=pravicaLabel]:eq(1)").css("display", "none");
            $("[id=id_pravica_izbire]:eq(1)").css("display", "none");
        }
    });
    // Edit zeton modal logic
    $('#edit_zeton_modal').on('show.bs.modal', function (event) {
        eventTarget = $(event.relatedTarget);
        student_id = $(event.relatedTarget).data('id');
        var url = '/api/zetoni_studenta/' + student_id;
        $("#edit_zeton_form").attr("action", "/api/zetoni_studenta/" + student_id + "/");
        $("#deleteZeton").attr("action", "/api/zetoni_studenta/" + student_id + "/");
        $.get(url, function () {})
            .done(function (response) {
                data = response;
                console.log(data);
                var letnik = parseInt(data['letnik'])
                if (letnik == 3) {
                    $("[id=id_pravica_izbire]:eq(1)").css("display", "block");
                    $("[id=pravicaLabel]:eq(1)").css("display", "block");
                    if (data['pravica_izbire'] == true) {
                        $("[id=id_pravica_izbire]:eq(1)").prop('checked', true);
                    }
                } else {
                    $("[id=id_pravica_izbire]:eq(1)").css("display", "none");
                    $("[id=pravicaLabel]:eq(1)").css("display", "none");
                    $("[id=id_pravica_izbire]:eq(1)").prop('checked', false);
                }
                $("[id=id_smer]:eq(1)").val(data['smer']);
                $("[id=id_letnik]:eq(1)").val(letnik);
                $("[id=id_vrsta]:eq(1)").val(data['vrsta']);
                $("[id=id_nacin]:eq(1)").val(data['nacin']);
                $("[id=id_oblika]:eq(1)").val(data['oblika']);
            })
            .fail(function () {
                $("#id_smer option")[0].selected = true;
                $('#id_letnik option')[0].selected = true;
                $("#id_letnik option")[0].selected = true;
                $("#id_vrsta option")[0].selected = true;
                $("#id_nacin option")[0].selected = true;
                $("#id_oblika option")[0].selected = true;
                $("#pravicaLabel").css("display", "none");
                $("#id_pravica_izbire").css("display", "none");
            });
    });

    $('#edit_zeton_form').submit(function (event) {
        event.preventDefault();
        $.ajax({
            data: $(this).serialize() + '&student=' + student_id,
            type: 'PUT',
            url: $(this).attr('action'),
            success: function () {
                $('#edit_zeton_modal').modal('hide');
                $('#zeton_modal_edit_success').modal('show');
            },
            error: function () {
                $('#edit_zeton_modal').modal('hide');
                $('#zeton_modal_edit_failure').modal('show');
            }
        });
        return false;
    });

    $('#deleteZeton').submit(function (event) {
        event.preventDefault();
        $.ajax({
            data: {},
            type: 'DELETE',
            url: $(this).attr('action'),
            success: function () {
                $('#edit_zeton_modal').modal('hide');
                $('#zeton_modal_delete_success').modal('show');
                $(eventTarget).attr("data-target", "#add_zeton_modal");
                $(eventTarget).text("Kreiraj žeton");
            },
            error: function () {
                $('#edit_zeton_modal').modal('hide');
                $('#zeton_modal_delete_failure').modal('show');
            }
        });
        return false;
    });

    // _____________________________________________________________________________________________
    // _____________________________________________________________________________________________
    // _____________________________________________________________________________________________

    // Piši tu dalje

    // AJAX klic za dodajanje izpitnega roka
    $('#add_rok_form').submit(function (event) {
        event.preventDefault();
        $.ajax({
            data: $(this).serialize(),
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            success: function (data) {
                $('#add_rok_modal').modal('hide');
                $('#rok_modal_response_success').modal('show');
            },
            error: function (data) {
                $('#add_rok_modal').modal('hide');
                error = jQuery.parseJSON(data.responseText);
                error_msg = error["__all__"];
                console.log(error_msg);
                $('#add_rok_error_msg').text(error_msg);
                $('#rok_modal_response_failure').modal('show');
            }
        });
        return false;
    });
});