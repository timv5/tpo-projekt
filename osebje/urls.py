from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path
from . import views as osebje_views


urlpatterns = [
    # seznam studentov
    url(r'students/(?P<pk>[0-9]+)/$', osebje_views.StudentMulti, name="student_detail"),
    url(r'students/pdf/(?P<pk>[0-9]+)/$', osebje_views.generate_new_pdf_student, name="student_pdf_new"),
    url(r'indeks/pdf/(?P<pk>[0-9]+)/$', osebje_views.index_pdf, name="index_pdf"),
    url(r'indeks/csv/(?P<pk>[0-9]+)/$', osebje_views.index_csv, name="index_csv"),
    url(r'indeks1/pdf/(?P<pk>[0-9]+)/$', osebje_views.generate_new_pdf_student_kartoteka, name="generate_new_pdf_student_kartoteka"),
    url(r'indeks1/csv/(?P<pk>[0-9]+)/$', osebje_views.generate_new_csv_student_kartoteka, name="generate_new_csv_student_kartoteka"),
    url(r'students/csv/(?P<pk>[0-9]+)/$', osebje_views.generate_csv_student, name="student_csv"),
    url(r'students/pdf/$', osebje_views.generate_new_pdf, name="students_pdf"),
    url(r'students/csv/$', osebje_views.generate_csv_get_all, name="students_csv"),
    url(r'students/$', osebje_views.StudentList, name='student_list'),
    url(r'students_added/$', osebje_views.StudentAdded, name='student_added'),
    url(r'add_zeton/(?P<pk>[0-9]+)/$', osebje_views.AddZeton, name="student_add_zeton"),
    url(r'predmeti/$',osebje_views.predmet_list, name="predmet_list"),
    url(r'predmeti/(?P<pk>[0-9]+)/$', osebje_views.predmet_details, name="predmet_details"),
    url(r'predmeti/(?P<pk>[0-9]+)/vpisioceno/(?P<pk1>[0-9]+)/$', osebje_views.DirektenVpisOcene, name="DirektenVpisOcene"),
    url(r'predmeti/(?P<pk1>[0-9]+)/(?P<pk>[0-9]+)/seznamPrijavljenihIzpis/$', osebje_views.seznam_prijavljenih_izpis, name="seznam_prijavljenih_izpis"),
    url(r'predmeti/(?P<pk1>[0-9]+)/(?P<pk>[0-9]+)/seznamPrijavljenihIzpis/pdf$', osebje_views.seznam_prijavljenih_izpis_pdf, name="seznam_prijavljenih_izpis_pdf"),
    url(r'predmeti/(?P<pk1>[0-9]+)/(?P<pk>[0-9]+)/seznamPrijavljenihIzpis/csv$', osebje_views.seznam_prijavljenih_izpis_csv, name="seznam_prijavljenih_izpis_csv"),
    url(r'predmeti/(?P<pk1>[0-9]+)/(?P<pk>[0-9]+)/seznamPrijavljenih/$', osebje_views.seznam_prijavljenih, name="seznam_prijavljenih"),
    url(r'predmeti/(?P<pk1>[0-9]+)/(?P<pk>[0-9]+)/seznamPrijavljenih/(?P<pk2>[0-9]+)/$', osebje_views.seznam_prijavljenih_vpis, name="seznam_prijavljenih_vpis"),
    url(r'predmeti/csv/(?P<pk>[0-9]+)/$', osebje_views.generate_csv_predmeti, name="predmet_details_csv"),
    url(r'predmeti/pdf/(?P<pk>[0-9]+)/$', osebje_views.generate_pdf_predmeti, name="predmet_details_pdf"),
    url(r'potrdivpis/$', osebje_views.potrdi_vpis, name="potrdi_vpis"),
    url(r'NarocenaPotrdila/$', osebje_views.NarocenaPotrdila, name="NarocenaPotrdila"),
    url(r'NarocenaPotrdila/(?P<pk>[0-9]+)/$', osebje_views.NatisniNarocenaPotrdila, name="NatisniNarocenaPotrdila"),
    url(r'potrdivpis/(?P<pk>[0-9]+)/$', osebje_views.funkcija, name="potrdifunkcija"),
    url(r'potrdivpis/pdf/(?P<pk>[0-9]+)/$', osebje_views.potrdilo_vpis, name="natisnipotrdilo"),
    url(r'add_izpit/(?P<pk>[0-9]+)/$', osebje_views.AddIzpit, name="add_izpit"),
    url(r'kartoteka/(?P<pk>[0-9]+)/$', osebje_views.Kartoteka, name="kartoteka"),
    url(r'indeks/(?P<pk>[0-9]+)/$', osebje_views.Elektronski_Indeks, name="elektronski_indeks"),
    url(r'predmeti/(?P<pk1>[0-9]+)/(?P<pk>[0-9]+)/seznamPrijavljenih/odjavi/(?P<pk2>[0-9]+)/$',osebje_views.odjavistudent, name="odjavistudent"),
    url(r'predmeti/(?P<pk1>[0-9]+)/(?P<pk>[0-9]+)/seznamPrijavljenih/prijavi/(?P<pk2>[0-9]+)/$',osebje_views.prijavistudent, name="prijavistudent"),
    url(r'predmeti/(?P<pk1>[0-9]+)/(?P<pk>[0-9]+)/seznamPrijavljenih/prijavi/(?P<pk2>[0-9]+)/opozorilo/$',osebje_views.OpozoriloPrijava, name="OpozoriloPrijava"),
    url(r'predmeti/(?P<pk1>[0-9]+)/(?P<pk>[0-9]+)/seznamPrijavljenih/prijavi/(?P<pk2>[0-9]+)/opozorilo/nazaj/$',osebje_views.nazajPrijava, name="NazajPrijava"),
    url(r'predmeti/(?P<pk1>[0-9]+)/(?P<pk>[0-9]+)/seznamPrijavljenih/prijavi/(?P<pk2>[0-9]+)/opozorilo/potrdi/$',osebje_views.PotrjenaPrijava, name="PotrdiPrijava"),
    url(r'predmeti/(?P<pk>[0-9]+)/spremeni/(?P<pk1>[0-9]+)/$', osebje_views.SpremeniIzpit, name="spremeniIzpit"),
    url(r'predmeti/(?P<pk>[0-9]+)/spremeni/(?P<pk1>[0-9]+)/nazaj$', osebje_views.nazaj, name="nazaj"),
    url(r'predmeti/(?P<pk>[0-9]+)/izbrisi/(?P<pk1>[0-9]+)/$', osebje_views.BrisiIzpit, name="izbrisiIzpit"),
    url(r'predmeti/(?P<pk>[0-9]+)/izbrisi/(?P<pk1>[0-9]+)/opozorilo/$', osebje_views.OpozoriloIzpit, name="opozoriloIzpit"),
    url(r'predmeti/(?P<pk>[0-9]+)/izbrisi/(?P<pk1>[0-9]+)/opozorilo/nazaj/$', osebje_views.nazaj, name="nazaj"),
    url(r'predmeti/(?P<pk>[0-9]+)/izbrisi/(?P<pk1>[0-9]+)/opozorilo/potrdi/$', osebje_views.PotrjenBris, name="potrdi"),
    url(r'narocila/seznam', osebje_views.seznam_narocil, name="narocila_seznam"),

]

urlpatterns = format_suffix_patterns(urlpatterns)