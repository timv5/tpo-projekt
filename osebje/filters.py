from Studis.models import Student, Predmet, Predmet_leto, Vpis, Studijsko_leto, Evidenca_izpit
import django_filters
from django import forms

class Student_list_filter(django_filters.FilterSet):
    def __init__(self, *args, **kwargs):
        super(Student_list_filter, self).__init__(*args, **kwargs)
        for name, field in self.filters.items():
            if isinstance(field, django_filters.ChoiceFilter):
                field.extra['empty_label'] = None

    STATUS_CHOICES = (
    ('', ("Vsi letniki")),
    (1, ("1. letnik")),
    (2, ("2. letnik")),
    (3, ("3. letnik")))
    student__first_name = django_filters.CharFilter(lookup_expr='istartswith',widget=forms.TextInput(attrs={'placeholder':'Ime'}))
    student__last_name = django_filters.CharFilter(lookup_expr='istartswith',widget=forms.TextInput(attrs={'placeholder':'Priimek'}))
    student__vpisna = django_filters.CharFilter(lookup_expr='contains',widget=forms.TextInput(attrs={'placeholder':'Vpisna'}))
    letnik = django_filters.ChoiceFilter(choices=STATUS_CHOICES)
    leto_vpisa = django_filters.ModelChoiceFilter(queryset=Studijsko_leto.objects.all())
    
    class Meta:
        model = Vpis
        fields = ['letnik', 'student__vpisna', 'student__first_name', 'student__last_name', 'leto_vpisa',]


class Student_list_predmet_filter(django_filters.FilterSet):
    def __init__(self, *args, **kwargs):
        super(Student_list_predmet_filter, self).__init__(*args, **kwargs)
        for name, field in self.filters.items():
            if isinstance(field, django_filters.ChoiceFilter):
                field.extra['empty_label'] = None


    first_name = django_filters.CharFilter(lookup_expr='istartswith',
                                                    widget=forms.TextInput(attrs={'placeholder': 'Ime'}))
    last_name = django_filters.CharFilter(lookup_expr='istartswith',
                                                   widget=forms.TextInput(attrs={'placeholder': 'Priimek'}))
    vpisna = django_filters.CharFilter(lookup_expr='contains',
                                                widget=forms.TextInput(attrs={'placeholder': 'Vpisna'}))

    class Meta:
        model = Student
        fields = ['vpisna', 'first_name', 'last_name', ]

class Evidenca_izpit_filter(django_filters.FilterSet):
    student__first_name = django_filters.CharFilter(lookup_expr='istartswith',widget=forms.TextInput(attrs={'placeholder':'Ime'}))
    student__last_name = django_filters.CharFilter(lookup_expr='istartswith',widget=forms.TextInput(attrs={'placeholder':'Priimek'}))
    student__vpisna = django_filters.CharFilter(lookup_expr='contains',widget=forms.TextInput(attrs={'placeholder':'Vpisna'}))
    class Meta:
        model = Evidenca_izpit
        fields = ['student' ]

def subjects(request):
    if request is None:
        return Predmet.objects.all()
    else:
        predmeti = []
        predmet_leto = Predmet_leto.objects.filter(profesor=request.user.id)
        for p in predmet_leto:
            if p.predmet not in predmeti:
                predmeti.append(p.predmet.id)
        return Predmet.objects.filter(id__in=predmeti)
    
class Subject_list_filter(django_filters.FilterSet):
    predmet__sifra = django_filters.CharFilter(lookup_expr='icontains',widget=forms.TextInput(attrs={'placeholder':'Šifra predmeta'}))
    studijsko_leto = django_filters.ModelChoiceFilter(queryset= Studijsko_leto.objects.all(), empty_label = None, initial = '4')
    predmet = django_filters.ModelChoiceFilter(queryset=subjects, empty_label = 'Vsi predmeti')

    class Meta:
        model = Predmet_leto
        fields = ['studijsko_leto','predmet']

class Vpisi_filter(django_filters.FilterSet):

    def __init__(self, *args, **kwargs):
        super(Vpisi_filter, self).__init__(*args, **kwargs)
        for name, field in self.filters.items():
            if isinstance(field, django_filters.ChoiceFilter):
                field.extra['empty_label'] = None


    ADDRESSED_CHOICES = (
    ('','Vsi vpisi'),
    ('False','Nepotrjeni vpisi'),
    ('True','Potrjeni vpisi'),
    )
    student__first_name = django_filters.CharFilter(lookup_expr='istartswith',widget=forms.TextInput(attrs={'placeholder':'Ime'}))
    student__last_name = django_filters.CharFilter(lookup_expr='istartswith',widget=forms.TextInput(attrs={'placeholder':'Priimek'}))
    student__vpisna = django_filters.CharFilter(lookup_expr='contains',widget=forms.TextInput(attrs={'placeholder':'Vpisna'}))
    potrjen = django_filters.ChoiceFilter(choices=ADDRESSED_CHOICES)
    class Meta:
        model = Vpis
        fields = ['student', 'potrjen',]
        