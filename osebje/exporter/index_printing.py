from reportlab.lib.pagesizes import letter, A4
from Studis.models import Student, Coin, Predmet_leto, Predmet, Evidenca_student, Evidenca_izpit, SteviloOpravljanj, Vpis, Izpit
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, PageBreak, ListFlowable, \
    BaseDocTemplate, PageTemplate, Frame, Spacer, \
    TableStyle, BaseDocTemplate, PageTemplate, Frame, Table
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle, ListStyle
from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_RIGHT
from reportlab.lib.units import inch
from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.lib import colors
from reportlab.lib.colors import red, black, yellow
import datetime
from reportlab.pdfbase.pdfmetrics import stringWidth


class NumberedCanvas(canvas.Canvas):
    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self._saved_page_states = []

    def showPage(self):
        self._saved_page_states.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        """add page info to each page (page x of y)"""
        num_pages = len(self._saved_page_states)
        for state in self._saved_page_states:
            self.__dict__.update(state)
            self.draw_page_number(num_pages)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_page_number(self, page_count):
        # Change the position of this to wherever you want the page number to be
        self.drawRightString(115 * mm, 0.2 * mm + (0.4 * inch),
                             "%d od %d" % (self._pageNumber, page_count))


class MyPrintIndeks:
    def __init__(self, buffer, pagesize, pk):
        self.pk = pk
        self.buffer = buffer
        if pagesize == 'A4':
            self.pagesize = A4
        elif pagesize == 'Letter':
            self.pagesize = letter
        self.width, self.height = self.pagesize

    @staticmethod
    def _header_footer(canvas, doc):
        # Save the state of our canvas so we can draw on it
        canvas.saveState()
        styles = getSampleStyleSheet()

        normaln = styles['Normal']
        normaln.fontName = 'Arial'

        # Header
        header = Paragraph('Univerza v Ljubljani - Fakulteta za računalništvo in informatiko ', normaln)
        w, h = header.wrap(doc.width, doc.topMargin)
        header.drawOn(canvas, inch * 3, doc.height + doc.topMargin - h)

        date = datetime.date.today().strftime("%B %d, %Y")
        tmp_date = []
        tmp_date = date.split(" ")
        if tmp_date[0] == "May":
            tmp_date[0] = "Maj"
        elif tmp_date[0] == "Aug":
            tmp_date[0] = "Avg"
        elif tmp_date[0] == "Oct":
            tmp_date[0] = "Okt"

        dan = tmp_date[1].split(",")
        date = dan[0] + ' ' + tmp_date[0] + ', ' + tmp_date[2]
        # Footer
        footer = Paragraph('Datum: ' + date, normaln)
        w, h = footer.wrap(doc.width, doc.bottomMargin)
        footer.drawOn(canvas, inch / 4, h)

        # Release the canvas
        canvas.restoreState()


    def print_users(self):
        buffer = self.buffer
        doc = SimpleDocTemplate(buffer,
                                rightMargin=inch / 4,
                                leftMargin=inch,
                                topMargin=inch / 2,
                                bottomMargin=inch / 4,
                                pagesize=self.pagesize)

        # Our container for 'Flowable' objects
        elements = []
        # A large collection of style sheets pre-made for us
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='centered', alignment=TA_CENTER))
        naslov1 = styles['Heading1']
        naslov1.alignment = TA_CENTER
        naslov1.fontName = 'Arial'

        naslov2 = styles['Heading2']
        naslov2.alignment = TA_LEFT
        naslov2.fontName = 'Arial'

        normaln = styles['Normal']
        normaln.fontName = 'Arial'

        podnaslov = styles['Heading2']
        podnaslov.alignment = TA_CENTER
        podnaslov.fontName = 'Arial'

        #PRIDOBI PODATKE
        student = Student.objects.get(pk=self.pk)
        izpiti_orig = Evidenca_izpit.objects.filter(student=student,ocena__gt=5, status=True).order_by('izpit')
        
        rev = izpiti_orig[::-1]
        seen = {}
        new_list = []
        for izpit in rev:
            if izpit.izpit.predmet.predmet not in seen:
                new_list.append(izpit)
                seen[izpit.izpit.predmet.predmet] = True 
        izpiti_orig = new_list[::-1]

        sumECTS = 0
        for izpit in izpiti_orig:
            sumECTS += izpit.izpit.predmet.predmet.kreditne_tocke

        stOpravljanj = []
        for izpit in izpiti_orig:
            st = SteviloOpravljanj.objects.get(student=student,predmet=izpit.izpit.predmet.predmet)
            stOpravljanj.append(st)
        
        vpisi = Vpis.objects.filter(student=student)
        data = [[x] for x in vpisi]
        predmeti_evidenca = Evidenca_student.objects.filter(student=student)
        predmeti_id = [x.predmet.id for x in predmeti_evidenca]
        izpit_evidenca = Evidenca_izpit.objects.filter(student=student)
        ocene_global = []
        global_ECTS = []
        for i,vpis in enumerate(vpisi):
            subjects = Predmet_leto.objects.filter(studijsko_leto=vpis.leto_vpisa,id__in=predmeti_id)
            izpiti = Izpit.objects.filter(predmet__in=subjects).order_by('datum')
            predmeti = [[x]for x in subjects]
            ocene = []
            doneECTS = 0
            for predmet in predmeti:
                izpiti_studenta = izpiti.filter(predmet_id=predmet[0].id)
                izpiti_evidenca = izpit_evidenca.filter(izpit__in=izpiti_studenta).order_by('izpit__datum')
                if izpiti_evidenca.last() is not None and izpiti_evidenca.last().ocena is not None:
                    ocena = izpiti_evidenca.last().ocena
                    if ocena > 5:
                        ocene.append(ocena)
                        ocene_global.append(ocena)
                        doneECTS += predmet[0].predmet.kreditne_tocke
                        global_ECTS.append(predmet[0].predmet.kreditne_tocke)
            avg = 0
            if len(ocene) is not 0:
                avg = sum(ocene) / len(ocene)
            extra = {'avg' : avg, 'ECTS' : doneECTS}
            data[i].append(extra)
            data[i].append(ocene)

        avg = 0
        if len(ocene_global) is not 0:
            avg = sum(ocene_global) / len (ocene_global)
        go = {'len' : len(ocene_global),
                    'ECTS': sumECTS,
                    'avg' : avg}



        #PRIKAZ NA PDF
        elements.append(Spacer(1, 0.15 * inch))
        elements.append(Paragraph('Elektronski indeks', naslov1))
        elements.append(Paragraph(str(student.vpisna) + ' ' + student.first_name + ' ' + student.last_name, naslov1))
        elements.append(Spacer(1, 0.15*inch))

        vrstica = []
        vrstica.append(['#', 'Šifra', 'Naziv predmeta', 'Ocenil', 'Datum', 'Opravljanje', 'ECTS', 'Ocena'])
        j = 1
        for entry in izpiti_orig:
            tmp_datum = []
            tmp_datum = str(entry.izpit.datum).split('-')
            neki = []
            neki.append(str(j))
            neki.append(str(entry.izpit.predmet.predmet.sifra))
            neki.append(str(entry.izpit.predmet.predmet.naslov_predmeta))
            neki.append(str(entry.izpit.predmet.profesor.first_name + ' ' + entry.izpit.predmet.profesor.last_name))
            neki.append(tmp_datum[2] + '.' + tmp_datum[1] + '.'  + tmp_datum[0])
            neki.append(str(stOpravljanj[j-1].stVseh))
            neki.append(str(entry.izpit.predmet.predmet.kreditne_tocke))
            neki.append(str(entry.ocena))
            vrstica.append(neki)
            j = j + 1

        final_tabela = Table(vrstica)
        final_tabela.setStyle(TableStyle([('FONT', (0, 0), (-1, -1), 'Arial')]))
        elements.append(final_tabela)
        elements.append(PageBreak())

        #DRUGA TABELA
        elements.append(Spacer(1, 0.15 * inch))
        elements.append(Spacer(1, 0.15 * inch))
        elements.append(Paragraph('Povprečne ocene po študijskih letih', naslov1))
        elements.append(Spacer(1, 0.15 * inch))

        vrstica = []
        j = 1
        vrstica.append(['#', 'Študijsko leto', 'Št. opravljenih izpitov', 'Kreditne točke', 'Skupno povprečje'])
        for entry in data:
            neki = []
            neki.append(str(j))
            neki.append(str(entry[0].leto_vpisa.studijsko_leto))
            neki.append(str(len(entry[2])))
            neki.append(str(entry[1]['avg']))
            neki.append(str(entry[1]['ECTS']))
            j = j + 1
            vrstica.append(neki)
        
        final_tabela = Table(vrstica)
        final_tabela.setStyle(TableStyle([('FONT', (0, 0), (-1, -1), 'Arial')]))
        elements.append(final_tabela)


        #TRETJI SKLOP
        elements.append(Spacer(1, 0.15 * inch))
        elements.append(Spacer(1, 0.15 * inch))
        elements.append(Paragraph('Skupna povprečna ocena', naslov1))
        elements.append(Spacer(1, 0.15 * inch))

        vrstica = []

        vrstica.append(['Število opravljenih izpitov', 'Kreditne točke', 'Skupno povprečje'])
        for entry in data:
            neki = []
            neki.append(str(len(ocene_global)))
            neki.append(str(go['ECTS']))
            neki.append(str(go['avg']))
            vrstica.append(neki)
            break
        
        final_tabela = Table(vrstica)
        final_tabela.setStyle(TableStyle([('FONT', (0, 0), (-1, -1), 'Arial')]))
        elements.append(final_tabela)

        doc.build(elements, onFirstPage=self._header_footer, onLaterPages=self._header_footer,
                  canvasmaker=NumberedCanvas)
        # Get the value of the BytesIO buffer and write it to the response.
        pdf = buffer.getvalue()
        buffer.close()
        return pdf
