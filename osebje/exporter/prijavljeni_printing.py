from reportlab.lib.pagesizes import letter, A4
from Studis.models import Student, Coin, Predmet_leto, Predmet, Evidenca_student, Evidenca_izpit, Evidenca_profesor, \
    Evidenca_student, Izpit, SteviloOpravljanj
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, ListFlowable, BaseDocTemplate, PageTemplate, Frame, Spacer, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle, ListStyle
from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_RIGHT
from reportlab.lib.units import inch
from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.lib import colors
from reportlab.lib.colors import red, black, yellow
import datetime
from reportlab.pdfbase.pdfmetrics import stringWidth


class NumberedCanvas(canvas.Canvas):
    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self._saved_page_states = []

    def showPage(self):
        self._saved_page_states.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        """add page info to each page (page x of y)"""
        num_pages = len(self._saved_page_states)
        for state in self._saved_page_states:
            self.__dict__.update(state)
            self.draw_page_number(num_pages)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_page_number(self, page_count):
        # Change the position of this to wherever you want the page number to be
        self.drawRightString(115 * mm, 0.2 * mm + (0.4 * inch),
                             "%d od %d" % (self._pageNumber, page_count))


class MyPrint:
    def __init__(self, buffer, pagesize, pk1, pk):
        self.pk1 = pk1
        self.pk = pk
        self.buffer = buffer
        if pagesize == 'A4':
            self.pagesize = A4
        elif pagesize == 'Letter':
            self.pagesize = letter
        self.width, self.height = self.pagesize

    @staticmethod
    def _header_footer(canvas, doc):
        # Save the state of our canvas so we can draw on it
        canvas.saveState()
        styles = getSampleStyleSheet()

        normaln = styles['Normal']
        normaln.fontName = 'Arial'

        # Header
        header = Paragraph('Univerza v Ljubljani - Fakulteta za računalništvo in informatiko ', normaln)
        w, h = header.wrap(doc.width, doc.topMargin)
        header.drawOn(canvas, inch * 3, doc.height + doc.topMargin - h)

        date = datetime.date.today().strftime("%B %d, %Y")
        tmp_date = []
        tmp_date = date.split(" ")
        if tmp_date[0] == "May":
            tmp_date[0] = "Maj"
        elif tmp_date[0] == "Aug":
            tmp_date[0] = "Avg"
        elif tmp_date[0] == "Oct":
            tmp_date[0] = "Okt"

        dan = tmp_date[1].split(",")
        date = dan[0] + ' ' + tmp_date[0] + ', ' + tmp_date[2]
        # Footer
        footer = Paragraph('Datum: ' + date, normaln)
        w, h = footer.wrap(doc.width, doc.bottomMargin)
        footer.drawOn(canvas, inch / 4, h)

        # Release the canvas
        canvas.restoreState()

    def print_users(self):
        buffer = self.buffer
        doc = SimpleDocTemplate(buffer,
                                rightMargin=inch / 4,
                                leftMargin=inch,
                                topMargin=inch / 2,
                                bottomMargin=inch / 4,
                                pagesize=self.pagesize)

        # Our container for 'Flowable' objects
        elements = []
        # A large collection of style sheets pre-made for us
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='centered', alignment=TA_CENTER))
        naslov1 = styles['Heading1']
        naslov1.alignment = TA_CENTER
        naslov1.fontName = 'Arial'

        naslov2 = styles['Heading2']
        naslov2.alignment = TA_LEFT
        naslov2.fontName = 'Arial'

        normaln = styles['Normal']
        normaln.fontName = 'Arial'

        print(self.pk1)
        print(self.pk)
        

        #PODATKI
        izpit = Izpit.objects.get(pk=self.pk)
        predmet = Predmet_leto.objects.select_related('predmet', 'profesor', 'studijsko_leto').get(pk=self.pk1)
        prijavljeni_izpit = Evidenca_izpit.objects.filter(izpit=izpit).all()
        filtriran_datum = self.single_date_filter(izpit)

        # stevilo opravljanj
        polaganja = []
        polaganja_pon = []
        polaganja_letos = []
        for x in prijavljeni_izpit:
            stOpravljanjLetos = Evidenca_izpit.objects.filter(izpit__predmet=izpit.predmet, student_id=x.student.id,
                                                              ocena__isnull=False).count()
            stOpr = SteviloOpravljanj.objects.filter(student_id=x.student.id,
                                                     predmet=izpit.predmet.predmet).last()

            stVeljavnihOpravljanj = 0
            stDejanskihOpravljanj = 0
            stPopravljanj = 0
            if stOpr is not None:
                stDejanskihOpravljanj = stOpr.stVseh
                stPopravljanj = stOpr.stPop
                stVeljavnihOpravljanj = stDejanskihOpravljanj - stPopravljanj
            polaganja.append(stDejanskihOpravljanj + 1)
            polaganja_letos.append(stOpravljanjLetos)
            polaganja_pon.append(stPopravljanj)

        polaganja = polaganja[::-1]
        polaganja_pon = polaganja_pon[::-1]
        polaganja_letos = polaganja_letos[::-1]
        polaganja_pon_1 = polaganja_pon.copy()

        elements.append(Spacer(1, 0.15 * inch))
        elements.append(Paragraph('Seznam prijavljenih študentov na izpit', naslov1))
        elements.append(Spacer(1, 0.125*inch))

        elements.append(Paragraph('Predmet: ' + str(predmet), normaln))
        elements.append(Paragraph('Šifra predmeta: ' + str(predmet.predmet.sifra), normaln))
        elements.append(Paragraph('Študijsko leto: ' + str(predmet.studijsko_leto), normaln))
        elements.append(Paragraph('Izpraševalec: ' + str(predmet.profesor.first_name + ' ' + predmet.profesor.last_name), normaln))
        elements.append(Paragraph('Smer: ' + str(predmet.predmet.smer), normaln))
        elements.append(Paragraph('Datum izpita: ' + str(filtriran_datum), normaln))
        elements.append(Paragraph('Prostor izpita: ' + str(izpit.prostor), normaln))
        elements.append(Paragraph('Ura izpita: ' + str(izpit.cas), normaln))
        elements.append(Spacer(1, 0.25*inch))

        j = 1
        vrstica = []
        vrstica.append(['#', 'Vp. št.', 'Ime', 'Priimek', 'Študijsko leto', 'Zap. št. pol. sk.', 'Zap. št. pol. letos', 'Točke', 'Kon. ocena'])
        for evidenca in prijavljeni_izpit:
            tocke_tmp = '/'
            kocna_ocena_tmp = '/'
            if evidenca.tocke is not None:
                tocke_tmp = evidenca.tocke
            if evidenca.ocena is not None:
                kocna_ocena_tmp = evidenca.ocena

            if evidenca.status is True:
                if polaganja_pon_1.pop() == 0:
                    vrstica.append([j, evidenca.student.vpisna, evidenca.student.first_name, evidenca.student.last_name, predmet.studijsko_leto, polaganja.pop(), polaganja_letos.pop(), tocke_tmp, kocna_ocena_tmp])
                else:
                    vrstica.append([j, evidenca.student.vpisna, evidenca.student.first_name, evidenca.student.last_name, predmet.studijsko_leto, polaganja.pop() + '-' + polaganja_pon.pop(), polaganja_letos.pop(), tocke_tmp, kocna_ocena_tmp])
            else:
                if polaganja_pon_1.pop() == 0:
                    vrstica.append([j, evidenca.student.vpisna, evidenca.student.first_name, evidenca.student.last_name, predmet.studijsko_leto, polaganja.pop(), polaganja_letos.pop(), tocke_tmp, 'VP'])
                else:
                    vrstica.append([j, evidenca.student.vpisna, evidenca.student.first_name, evidenca.student.last_name,
                                    predmet.studijsko_leto, polaganja.pop() + '-' + polaganja_pon, polaganja_letos.pop(), tocke_tmp, 'VP'])
            j = j + 1


        final_tabela = Table(vrstica)
        final_tabela.setStyle(TableStyle([('FONT', (0, 0), (-1, -1), 'Arial')]))
        elements.append(final_tabela)

        doc.build(elements, onFirstPage=self._header_footer, onLaterPages=self._header_footer,
                  canvasmaker=NumberedCanvas)
        # Get the value of the BytesIO buffer and write it to the response.
        pdf = buffer.getvalue()
        buffer.close()
        return pdf

    def single_date_filter(self, izpit):
        tmp_date = []
        tmp_date = izpit.datum.strftime("%B %d, %Y").split(" ")
        if tmp_date[0] == "May":
            tmp_date[0] = "Maj"
        elif tmp_date[0] == "Aug":
            tmp_date[0] = "Avg"
        elif tmp_date[0] == "Oct":
            tmp_date[0] = "Okt"
        dan = tmp_date[1].split(",")
        date = dan[0] + ' ' + tmp_date[0] + ', ' + tmp_date[2]
        return date