from reportlab.lib.pagesizes import letter, A4
from Studis.models import Student, Coin, Predmet_leto, Predmet, Evidenca_student
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, ListFlowable, BaseDocTemplate, PageTemplate, Frame, Spacer, PageBreak, TableStyle, BaseDocTemplate, PageTemplate, Frame, Table
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle, ListStyle
from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_RIGHT
from reportlab.lib.units import inch
from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.lib import colors
from reportlab.lib.colors import red, black, yellow
import datetime
from reportlab.pdfbase.pdfmetrics import stringWidth
from Studis.models import Vpis, Izpit, SteviloOpravljanj, Evidenca_izpit, Evidenca_profesor, Evidenca_student, Evidenca_Vpis, Predmet_leto, Predmet


class NumberedCanvas(canvas.Canvas):
    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self._saved_page_states = []

    def showPage(self):
        self._saved_page_states.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        """add page info to each page (page x of y)"""
        num_pages = len(self._saved_page_states)
        for state in self._saved_page_states:
            self.__dict__.update(state)
            self.draw_page_number(num_pages)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_page_number(self, page_count):
        # Change the position of this to wherever you want the page number to be
        self.drawRightString(115 * mm, 0.2 * mm + (0.4 * inch),
                             "%d od %d" % (self._pageNumber, page_count))


class MyPrintKartoteka:
    def __init__(self, buffer, pagesize, pk, studijsko_leto, polaganja):
        self.pk = pk
        self.studijsko_leto = studijsko_leto
        self.polaganja = polaganja
        self.buffer = buffer
        if pagesize == 'A4':
            self.pagesize = A4
        elif pagesize == 'Letter':
            self.pagesize = letter
        self.width, self.height = self.pagesize

    @staticmethod
    def _header_footer(canvas, doc):
        # Save the state of our canvas so we can draw on it
        canvas.saveState()
        styles = getSampleStyleSheet()

        normaln = styles['Normal']
        normaln.fontName = 'Arial'

        # Header
        header = Paragraph('Univerza v Ljubljani - Fakulteta za računalništvo in informatiko ', normaln)
        w, h = header.wrap(doc.width, doc.topMargin)
        header.drawOn(canvas, inch * 3, doc.height + doc.topMargin - h)

        date = datetime.date.today().strftime("%B %d, %Y")
        tmp_date = []
        tmp_date = date.split(" ")
        if tmp_date[0] == "May":
            tmp_date[0] = "Maj"
        elif tmp_date[0] == "Aug":
            tmp_date[0] = "Avg"
        elif tmp_date[0] == "Oct":
            tmp_date[0] = "Okt"

        dan = tmp_date[1].split(",")
        date = dan[0] + ' ' + tmp_date[0] + ', ' + tmp_date[2]
        # Footer
        footer = Paragraph('Datum: ' + date, normaln)
        w, h = footer.wrap(doc.width, doc.bottomMargin)
        footer.drawOn(canvas, inch / 4, h)

        # Release the canvas
        canvas.restoreState()


    def print_users(self):
        buffer = self.buffer
        doc = SimpleDocTemplate(buffer,
                                rightMargin=inch / 4,
                                leftMargin=inch,
                                topMargin=inch / 2,
                                bottomMargin=inch / 4,
                                pagesize=self.pagesize)

        # Our container for 'Flowable' objects
        elements = []
        # A large collection of style sheets pre-made for us
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='centered', alignment=TA_CENTER))
        naslov1 = styles['Heading1']
        naslov1.alignment = TA_CENTER
        naslov1.fontName = 'Arial'

        naslov2 = styles['Heading2']
        naslov2.alignment = TA_LEFT
        naslov2.fontName = 'Arial'

        normaln = styles['Normal']
        normaln.fontName = 'Arial'

        podnaslov = styles['Heading2']
        podnaslov.alignment = TA_CENTER
        podnaslov.fontName = 'Arial'

        #PODATKI
        student = Student.objects.get(pk=self.pk)
        if self.studijsko_leto is '0' or self.studijsko_leto is None:
            vpisi = Vpis.objects.filter(student=student)
        else:
            vpisi = Vpis.objects.filter(student=student,leto_vpisa_id=self.studijsko_leto)

        vpisi = Vpis.objects.filter(student=student)
        data = [[x] for x in vpisi] 
        predmeti_evidenca = Evidenca_student.objects.filter(student=student)
        predmeti_id = [x.predmet.id for x in predmeti_evidenca]
        for s,vpis in enumerate(vpisi):
            ocene = []
            maxECTS = 0
            doneECTS = 0

            entry = []
            subjects_help = Predmet_leto.objects.filter(studijsko_leto=vpis.leto_vpisa,id__in=predmeti_id)
            subjects = [x.predmet for x in subjects_help]

            for subject in subjects:
                xs = [subject]
                izpitus = []
                maxECTS += subject.kreditne_tocke
                for vpis_dup in vpisi:
                    c = Predmet_leto.objects.filter(predmet=subject,studijsko_leto=vpis_dup.leto_vpisa)
                    p_a = [x.id for x in c]
                    b = Evidenca_student.objects.filter(student=student,predmet__in=p_a)
                    p_l = [x.predmet for x in b]
                    izpiti = Izpit.objects.filter(predmet__in=p_l).order_by('datum')
                    izpit_student = Evidenca_izpit.objects.filter(student=student,izpit__in=izpiti,status=True)
                    for i,izpit in enumerate(izpit_student):
                        container = []
                        container.append(izpit)
                        extra_letos = str(i+1)
                        #print(extra_letos)
                        container.append(extra_letos)
                        izpitus.append(container)
                ####
                stevilo_opravljanj = SteviloOpravljanj.objects.get(student=student,predmet=subject)
                for i,izpit in enumerate(izpitus):
                    if i+1 <= 3:
                        extra_op = '(' + str(i+1) + '-0)'
                        izpit.append(extra_op)
                        #print(extra_op)
                    elif i+1 > 3:
                        extra_op = '(' + str(i+1) + '-' + str(stevilo_opravljanj.stPop) + ')'
                        #print(extra_op)
                        izpit.append(extra_op)

                last_izpit = None
                if len(izpitus) is not 0:
                    last_izpit = izpitus[-1:][0][0]
                print(last_izpit)
                if last_izpit is not None:
                    ocena = last_izpit.ocena
                    if ocena is not None and ocena > 5:
                        ocene.append(ocena)
                        doneECTS += subject.kreditne_tocke
                xs.append(izpitus)
                entry.append(xs)
            avg = 0
            if len(ocene) is not 0:
                avg = sum(ocene) / len(ocene)
            ECTS = str(doneECTS) + "/" + str(maxECTS)
            extra = {'avg' : avg, 'ECTS' : ECTS}
            data[s].append(entry)   
            data[s].append(extra)

        if self.studijsko_leto is '0' or self.studijsko_leto is None:
            pass
        else:
            mark = 0
            if self.studijsko_leto == '1':
                for vpis in data:
                    if vpis[0].leto_vpisa.studijsko_leto == '2014/2015':
                        data = [vpis]
                        mark = 1
            if self.studijsko_leto == '2':
                for vpis in data:
                    if vpis[0].leto_vpisa.studijsko_leto == '2015/2016':
                        data = [vpis]
                        mark = 1
            if self.studijsko_leto == '3':
                for vpis in data:
                    if vpis[0].leto_vpisa.studijsko_leto == '2016/2017':
                        data = [vpis]
                        mark = 1
            if self.studijsko_leto == '4':
                for vpis in data:
                    if vpis[0].leto_vpisa.studijsko_leto == '2017/2018':
                        data = [vpis]
                        mark = 1
            if self.studijsko_leto == '5':
                for vpis in data:
                    if vpis[0].leto_vpisa.studijsko_leto == '2018/2019':
                        data = [vpis]
                        mark = 1
            
            if mark == 0:
                data = []

        #PRIKAZ PODATKOV

        elements.append(Spacer(1, 0.15 * inch))
        elements.append(Paragraph('Kartotečni list', naslov1))
        elements.append(Paragraph(str(student.vpisna) + ' ' + student.first_name + ' ' + student.last_name, naslov1))
        elements.append(Spacer(1, 0.15*inch))

        for entry in data:
            elements.append(Spacer(1, 0.25*inch))
            elements.append(Paragraph('Študijsko leto: ' + str(entry[0].leto_vpisa), normaln))
            elements.append(Paragraph('Smer študija: ' + str(entry[0].smer), normaln))
            elements.append(Paragraph('Letnik: ' + str(entry[0].letnik), normaln))
            elements.append(Paragraph('Vrsta: ' + str(entry[0].vrsta), normaln))
            elements.append(Paragraph('Način: ' + str(entry[0].tip), normaln))
            elements.append(Paragraph('Skupina: LJUBLJANA', normaln))
            elements.append(Spacer(1, 0.25*inch))
            vrstica = []
            vrstica.append(['#', 'Šifra', 'Naziv predmeta', 'ECTS', 'Izpraševalec', 'Datum', 'Ocena', 'Št. polaganj'])

            j = 1
            num = 2
            for subject in entry[1]:

                    if num % 30 == 0:
                        vrstica.append([])
                        vrstica.append([])

                    neki = []
                    neki.append(str(j))
                    neki.append(str(subject[0].sifra))
                    neki.append(str(subject[0].naslov_predmeta))
                    neki.append(str(subject[0].kreditne_tocke))

                    vrstica.append(neki)
                    k = 0
                    num = num + 1
                    for izpit_pos in subject[1]:
                        if k >= 0:
                            num = num + 1
                            neki = []
                            neki.append('')
                            neki.append('')
                            neki.append('')
                            neki.append('')

                            if izpit_pos[0].izpit.izprasevalec is not None:
                                neki.append(str(izpit_pos[0].izpit.izprasevalec.first_name + ' ' + izpit_pos[0].izpit.izprasevalec.last_name))
                            else:
                                neki.append('/')

                            date1 = str(izpit_pos[0].izpit.datum)
                            tmp_date = []
                            tmp_date = date1.split("-")
                            date1 = tmp_date[2] + '.' + tmp_date[1] + '.' + tmp_date[0]
                            
                            neki.append(date1)
                            if izpit_pos[0].ocena is not None:
                                neki.append(str(izpit_pos[0].ocena))
                            else:
                                neki.append('/')
                            neki.append(str(izpit_pos[1] + '-' + izpit_pos[2]))
                            vrstica.append(neki)
                        k = k + 1
                    j = j + 1
                #elements.append(Spacer(1, 0.15*inch))
            
            final_tabela = Table(vrstica)
            final_tabela.setStyle(TableStyle([('FONT', (0, 0), (-1, -1), 'Arial')]))
            elements.append(final_tabela)
            elements.append(Spacer(1, 0.25*inch))

            elements.append(Paragraph('Skupno število kreditnih točk: ' + str(entry[2]['ECTS']), normaln))
            elements.append(Paragraph('Skupna poprečna ocena: ' + str(entry[2]['avg']), normaln))

            elements.append(PageBreak())

        doc.build(elements, onFirstPage=self._header_footer, onLaterPages=self._header_footer,
                  canvasmaker=NumberedCanvas)
        # Get the value of the BytesIO buffer and write it to the response.
        pdf = buffer.getvalue()
        buffer.close()
        return pdf
