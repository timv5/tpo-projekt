from reportlab.lib.pagesizes import letter, A4
from Studis.models import Student, Coin, Predmet_leto, Predmet, Evidenca_student
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, ListFlowable, \
    BaseDocTemplate, PageTemplate, Frame, Spacer, \
    TableStyle, BaseDocTemplate, PageTemplate, Frame, Table
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle, ListStyle
from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_RIGHT
from reportlab.lib.units import inch
from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.lib import colors
from reportlab.lib.colors import red, black, yellow
import datetime
from reportlab.pdfbase.pdfmetrics import stringWidth


class NumberedCanvas(canvas.Canvas):
    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self._saved_page_states = []

    def showPage(self):
        self._saved_page_states.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        """add page info to each page (page x of y)"""
        num_pages = len(self._saved_page_states)
        for state in self._saved_page_states:
            self.__dict__.update(state)
            self.draw_page_number(num_pages)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_page_number(self, page_count):
        # Change the position of this to wherever you want the page number to be
        self.drawRightString(115 * mm, 0.2 * mm + (0.4 * inch),
                             "%d od %d" % (self._pageNumber, page_count))


class MyPrint:
    def __init__(self, buffer, pagesize, pk):
        self.pk = pk
        self.buffer = buffer
        if pagesize == 'A4':
            self.pagesize = A4
        elif pagesize == 'Letter':
            self.pagesize = letter
        self.width, self.height = self.pagesize

    @staticmethod
    def _header_footer(canvas, doc):
        # Save the state of our canvas so we can draw on it
        canvas.saveState()
        styles = getSampleStyleSheet()

        normaln = styles['Normal']
        normaln.fontName = 'Arial'

        # Header
        header = Paragraph('Univerza v Ljubljani - Fakulteta za računalništvo in informatiko ', normaln)
        w, h = header.wrap(doc.width, doc.topMargin)
        header.drawOn(canvas, inch * 3, doc.height + doc.topMargin - h)

        date = datetime.date.today().strftime("%B %d, %Y")
        tmp_date = []
        tmp_date = date.split(" ")
        if tmp_date[0] == "May":
            tmp_date[0] = "Maj"
        elif tmp_date[0] == "Aug":
            tmp_date[0] = "Avg"
        elif tmp_date[0] == "Oct":
            tmp_date[0] = "Okt"

        dan = tmp_date[1].split(",")
        date = dan[0] + ' ' + tmp_date[0] + ', ' + tmp_date[2]
        # Footer
        footer = Paragraph('Datum: ' + date, normaln)
        w, h = footer.wrap(doc.width, doc.bottomMargin)
        footer.drawOn(canvas, inch / 4, h)

        # Release the canvas
        canvas.restoreState()


    def print_users(self):
        buffer = self.buffer
        doc = SimpleDocTemplate(buffer,
                                rightMargin=inch / 4,
                                leftMargin=inch,
                                topMargin=inch / 2,
                                bottomMargin=inch / 4,
                                pagesize=self.pagesize)

        # Our container for 'Flowable' objects
        elements = []
        # A large collection of style sheets pre-made for us
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='centered', alignment=TA_CENTER))
        naslov1 = styles['Heading1']
        naslov1.alignment = TA_CENTER
        naslov1.fontName = 'Arial'

        naslov2 = styles['Heading2']
        naslov2.alignment = TA_LEFT
        naslov2.fontName = 'Arial'

        normaln = styles['Normal']
        normaln.fontName = 'Arial'

        #PODATKI
        predmet = Predmet_leto.objects.select_related('predmet', 'profesor', 'studijsko_leto').get(pk=self.pk)
        students_ref = Evidenca_student.objects.filter(predmet=predmet)
        students = Student.objects.prefetch_related('coin_set').filter(id__in=students_ref.values('student_id'))

        elements.append(Spacer(1, 0.15 * inch))
        elements.append(Paragraph('Pregled vpisanih v predmet ' + predmet.predmet.naslov_predmeta, naslov1))
        elements.append(Spacer(1, 0.125*inch))

        elements.append(Paragraph('Splošni podatki predmeta',naslov2))
        elements.append(Spacer(1, 0.1 * inch))
        elements.append(Paragraph('Predmet: ' + predmet.predmet.naslov_predmeta, normaln))
        elements.append(Paragraph('Sifra predmeta: ' + str(predmet.predmet.sifra), normaln))
        elements.append(Paragraph('Nosilec predmeta: ' + predmet.profesor.first_name + ' ' + predmet.profesor.last_name, normaln))
        elements.append(Paragraph('Smer: ' + str(predmet.predmet.smer), normaln))
        elements.append(Paragraph('Študijsko leto: ' + str(predmet.studijsko_leto), normaln))

        if students:
            elements.append(Spacer(1, 0.125 * inch))
            elements.append(Paragraph('Pregled vpisanih', naslov2))
            elements.append(Spacer(1, 0.1 * inch))
            vrstica = []
            k = 1
            vrstica.append(['#','Vpisna številka: ', 'Ime, priimek', 'Email', 'Vrsta vpisa'])
            for student in students:
                neki = []
                neki.append(str(k))
                neki.append(str(student.vpisna))
                neki.append(student.first_name + ' ' + student.last_name)
                neki.append(student.email)
                if student.vpis_set.last() is not None:
                    neki.append(student.vpis_set.last().vrsta.sifra + " - " + student.vpis_set.last().vrsta.vrsta)
                else:
                    neki.append("/")
                vrstica.append(neki)
                k = k + 1

            final_tabela = Table(vrstica)
            final_tabela.setStyle(TableStyle([('FONT', (0, 0), (-1, -1), 'Arial')]))
            elements.append(final_tabela)

        doc.build(elements, onFirstPage=self._header_footer, onLaterPages=self._header_footer,
                  canvasmaker=NumberedCanvas)
        # Get the value of the BytesIO buffer and write it to the response.
        pdf = buffer.getvalue()
        buffer.close()
        return pdf
