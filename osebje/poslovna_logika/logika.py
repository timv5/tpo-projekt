from Studis.models import *
from django.contrib.auth.models import Group
import unicodedata
import random
import datetime

def handle_uploaded_file(f):
    n = 0
    lista = [line.decode('utf8') for line in f]
    group = Group.objects.filter(name='student').first()
    now = datetime.datetime.now().year
    now_vpisna = str(datetime.datetime.now().year)[-2:]
    now_plus = now+1
    now = str(now)+'/'+str(now_plus)

    vrsta = Vrsta_vpis.objects.get(sifra="01")
    nacin = Nacin_studij.objects.get(sifra="1")
    oblika = Oblika_studij.objects.get(sifra="1")
    smeri = Smer.objects.all()
    for person in lista:
        line = []
        x = 0
        line.append(person[0:30])
        line.append(person[30:30+29])
        line.append(person[30+30:30+30+7])
        line.append(person[30+30+7:30+30+7+30])
        line[0] = line[0].strip()
        line[1] = line[1].strip()
        line[2] = line[2].strip()
        line[3] = line[3].strip()
        print(line,line[3])

        created = False
        obj = None
        try:
            obj = Student.objects.get(osebni_email=line[3])
        except Student.DoesNotExist:
            created = True

        print(obj,created)
        if created:
            ##generiranje studenta
            evidenca_vpis = Evidenca_Vpis.objects.filter(studijsko_leto= Studijsko_leto.objects.get(studijsko_leto=now)).first()
            vpisna = '63' + now_vpisna + '{num:04d}'.format(num=evidenca_vpis.st_vpisov+800)
            evidenca_vpis.st_vpisov=evidenca_vpis.st_vpisov+1
            evidenca_vpis.save()
            ###email
            first_name_encoded = unicodedata.normalize('NFD', line[0].lower()).encode('ascii', 'ignore')
            last_name_encoded = unicodedata.normalize('NFD', line[1].lower()).encode('ascii', 'ignore')
            first_name_initial = first_name_encoded.decode()[0]
            last_name_initial = last_name_encoded.decode()[0]
            rand = random.choice('0123456789')
            rand1 = random.choice('0123456789')
            rand2 = random.choice('0123456789')
            rand3 = random.choice('0123456789')
            email=first_name_initial+last_name_initial+rand+rand1+rand2+rand3+'@student.uni-lj.si'
                ###geslo
            geslo=first_name_encoded.decode()[0]+last_name_encoded.decode()+rand+rand1+rand2+rand3
            print(email)
            obj = Student(osebni_email=line[3],first_name=line[0],last_name=line[1],email=email,vpisna=vpisna)
            obj.set_password(geslo)
            obj.save()
            obj.groups.add(group)

            smer = None
            if line[2] == '1000468':
                smer = smeri[0]
            else:
                smer = smeri[1]
            Coin(student=obj,vrsta=vrsta,oblika=oblika,nacin=nacin,smer=smer,letnik=1,pravica_izbire=False).save()
            
            n +=1
        else:
            obj.first_name=line[0]
            obj.last_name=line[1]
            obj.save()

            for coin in Coin.objects.filter(student=obj):
                coin.delete()
            
            smer = None
            if line[2] == '1000468':
                smer = smeri[0]
            else:
                smer = smeri[1]
            Coin(student=obj,vrsta=vrsta,oblika=oblika,nacin=nacin,smer=smer,letnik=1,pravica_izbire=False).save()
            
            n +=1
    return n

"""def handle_uploaded_file(f):
    n = 0
    lista = [line.decode('utf8').split() for line in f]
    group = Group.objects.filter(name='student').first()
    print(lista)
    now = datetime.datetime.now().year
    now_vpisna = str(datetime.datetime.now().year)[-2:]
    now_plus = now+1
    now = str(now)+'/'+str(now_plus)
    for line in lista:
        print(line)
        obj, created = Student.objects.get_or_create(osebni_email=line[3])
        print(obj)
        if created:
            ##generiranje studenta
            evidenca_vpis = Evidenca_Vpis.objects.filter(studijsko_leto= Studijsko_leto.objects.get(studijsko_leto=now)).first()
            vpisna = '63' + now_vpisna + '{num:04d}'.format(num=evidenca_vpis.st_vpisov)
            evidenca_vpis.st_vpisov=evidenca_vpis.st_vpisov+1
            evidenca_vpis.save()
            ###email
            first_name_encoded = unicodedata.normalize('NFD', line[0].lower()).encode('ascii', 'ignore')
            last_name_encoded = unicodedata.normalize('NFD', line[1].lower()).encode('ascii', 'ignore')
            first_name_initial = first_name_encoded.decode()[0]
            last_name_initial = last_name_encoded.decode()[0]
            rand = random.choice('0123456789')
            rand1 = random.choice('0123456789')
            rand2 = random.choice('0123456789')
            rand3 = random.choice('0123456789')
            email=first_name_initial+last_name_initial+rand+rand1+rand2+rand3+'@student.uni-lj.si'
                ###geslo
            geslo=first_name_encoded.decode()[0]+last_name_encoded.decode()+rand+rand1+rand2+rand3

            obj.first_name=line[0]
            obj.last_name=line[1]
            obj.email=email
            obj.vpisna=vpisna
            obj.set_password(geslo)
            obj.save()
            obj.groups.add(group)
            n +=1
        else:
            obj.first_name=line[0]
            obj.last_name=line[1]
            obj.save()
            for vpis in Vpis.objects.filter(student=obj)
                vpis.delete()
            smer = Smer.objects.first()
            nacin = Nacin_studij.objects.first()
            leto = Studijsko_leto.objects.all()
            vrsta = Vrsta_vpis.objects.all()

            Vpis(letnik=1, student=student, leto_vpisa=leto[0],
                smer=smer, tip=nacin, vrsta=vrsta.first()).save()
    return n """