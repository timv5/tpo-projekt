import requests
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .poslovna_logika.logika import handle_uploaded_file
from .form_config.vpisi_student.forms import UploadFileForm
from .filters import Student_list_filter, Subject_list_filter, Evidenca_izpit_filter, Vpisi_filter, \
    Student_list_predmet_filter
from Studis.models import Student, Coin, Predmet_leto, Predmet, Evidenca_student, Vpis, PotrdiloVpisa, Izpit, \
    Evidenca_izpit, SteviloOpravljanj, User, Nosilci, NarociloPotrdil
from vpis.form_config.zeton.forms import ZetonForm
from decorators import *
from .form_config.izpitni_rok.forms import IzpitForm, IzpitForm1
from .form_config.DirektenVpisOcene.form import DirektenVpisOceneForm
from .form_config.ocene_tocke_izpit.forms import IzpitOceneTockeForm, IzpitOceneTockeFormSet
from django.db import IntegrityError
from django.views.generic import View
from django.template.loader import get_template
from django.http import HttpResponse
import csv
import datetime
from .exporter.printing import MyPrint
from .exporter.kartotecni_list_printing import MyPrintKartoteka
from .exporter.index_printing import MyPrintIndeks
from .exporter.student_printing import MyPrint as MyPrintStudent
from .exporter.predmet_printing import MyPrint as MyPrintPredmet
from .exporter.prijavljeni_printing import MyPrint as MyPrintPrijavljeniSeznam
from django.conf import settings
from io import BytesIO
from easy_pdf.views import PDFTemplateView
from django.contrib import messages
from io import BytesIO
from reportlab.pdfgen import canvas
from django.http import HttpResponse
from xhtml2pdf import pisa
import json as simplejson
from datetime import date, timedelta
from django.http import HttpResponseRedirect
from Studis.poslovna_logika.poslovna_logika import studijskoLeto


# Create your views here.


@login_required
@user_passes_test(is_osebje)
def StudentList(request):
    user_list = Vpis.objects.filter().all().order_by('student__last_name')
    user_filter = Student_list_filter(request.GET,queryset=user_list)
    filter_form = user_filter.form
    paginator = Paginator(user_filter.qs, 10)
    page = request.GET.get('page')

    try:
        response_data = paginator.page(page)
    except PageNotAnInteger:
        response_data = paginator.page(1)
    except EmptyPage:
        response_data = paginator.page(paginator.num_pages)
    # Dodaj form fields za uvoz študentov
    form_uvoz = UploadFileForm()
    form_zeton = ZetonForm()
    if request.method == 'GET':
        return render(request, 'student_list.html', {'students': response_data,
                 'form' : filter_form, 'form_uvoz' : form_uvoz, 'form_zeton' : form_zeton})

    elif request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                st_dodanih = handle_uploaded_file(request.FILES['datoteka'])
                print(st_dodanih)
                return redirect(reverse('student_added') + '?added=' + str(st_dodanih))
            except Exception as e:
                print(str(e))
                error = "Pri uvozu datoteke je prišlo do napake. Poskusite znova. "
                return render(request, 'student_list.html', {'students': response_data,
                            'form' : filter_form, 'form_uvoz' : form_uvoz, 'error' : error, 'form_zeton' : form_zeton})

@login_required
@user_passes_test(is_osebje)
def StudentAdded(request):
    if request.method == 'GET':
        count = request.GET.get('added')
        user_list = Student.objects.all().order_by('-id')[:int(count)]
        paginator = Paginator(user_list, 10)
        page = request.GET.get('page')

        try:
            response_data = paginator.page(page)
        except PageNotAnInteger:
            response_data = paginator.page(1)
        except EmptyPage:
            response_data = paginator.page(paginator.num_pages)

        return render(request, 'pregled_dodanih.html', {'students': response_data})

def StudentMulti(request, pk):
    if request.method == 'GET':
        student = Student.objects.select_related('posta_prejem','posta_naslov').get(pk=pk)
        vpisi = student.vpis_set.all().select_related()
        return render(request, 'student_details.html', {'student': student, 'vpisi' : vpisi})

def AddZeton(request,pk):
    if request.method == 'POST':
        form = ZetonForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            try:
                coin = Coin(student_id=pk,**data)
                coin.save()
            except IntegrityError as e:
                pass
            return redirect('student_list')

def AddIzpit(request,pk):
    if request.method == 'POST':
        form = IzpitForm(request.POST,pk=pk)
        if form.is_valid():
            data = form.cleaned_data
            try:
                izpit = Izpit(predmet_id=pk,datum=data['datum'],prostor=data['prostor'],cas=data['cas'],izredni=data['izredni'], izprasevalec=data['izprasevalec']).save()
                print(data)
            except IntegrityError as e:
                pass
            return redirect('student_list')
        else:
            errors = form.errors
            return HttpResponse(status=400,content=simplejson.dumps(errors))

#uporabljen
def generate_new_pdf(request):
    # Create the HttpResponse object with the appropriate PDF headers.
    user_list = Vpis.objects.filter(leto_vpisa__studijsko_leto=studijskoLeto()).all().order_by('student__last_name')
    user_filter = Student_list_filter(request.GET,queryset=user_list)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="Students.pdf"'
    buffer = BytesIO()
    report = MyPrint(buffer, 'Letter',user_filter.qs)
    pdf = report.print_users()
    response.write(pdf)
    return response

#uporabljen
def generate_new_pdf_student(request, pk):
    student = Student.objects.select_related('posta_prejem', 'posta_naslov').get(pk=pk)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="%s.pdf"' %(student.vpisna)
    buffer = BytesIO()
    report = MyPrintStudent(buffer, 'Letter', pk)
    pdf = report.print_users()
    response.write(pdf)
    return response

#uporabljen
def generate_csv_get_all(request):
    user_list = Vpis.objects.filter(leto_vpisa__studijsko_leto=studijskoLeto()).all().order_by('student__last_name')
    user_filter = Student_list_filter(request.GET,queryset=user_list)
    response = HttpResponse(content_type='text/csv')
    filename = "Students.csv"
    response['Content-Disposition'] = 'attachment; filename="%s"' % (filename)
    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)

    date = datetime.date.today()
    writer.writerow(['Datum izpisa', date])
    writer.writerow([])
    writer.writerow([])
    writer.writerow(['Pregled vseh študentov'])
    writer.writerow(['Št.','Ime', 'Priimek', 'Vpisna', 'Univezitetni e-mail'])
    k = 1
    for student in user_filter.qs:
        writer.writerow([k, student.student.first_name, student.student.last_name, student.student.vpisna, student.student.email])
        k = k + 1

    return response

#uporabljen
def generate_csv_student(request, pk):
    student = Student.objects.select_related('posta_prejem', 'posta_naslov').get(pk=pk)
    vpisi = student.vpis_set.all().select_related()
    response = HttpResponse(content_type='text/csv')
    filename = "Student_%s.csv" % (student.vpisna)
    response['Content-Disposition'] = 'attachment; filename="%s"' % (filename)

    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)
    date = datetime.date.today()
    writer.writerow(['Datum izpisa', date])
    writer.writerow([])
    writer.writerow([])
    writer.writerow([])
    writer.writerow(['Osebni podatki študenta'])
    writer.writerow(['Ime', student.first_name])
    writer.writerow(['Priimek', student.last_name])
    writer.writerow(['Vpisna številka', student.vpisna])
    writer.writerow(['Univerzitetni e-mail', student.email])
    writer.writerow(['Telefonska številka', student.tel])
    writer.writerow(['Osebni e-mail', student.osebni_email])
    writer.writerow([''])
    writer.writerow([''])

    writer.writerow(['Naslov stalnega prebivališča'])
    writer.writerow(['Stalno prebivališče - Država', student.posta_naslov.drzava])
    writer.writerow(['Stalno prebivališče - Ulica', student.ulica_naslov])
    writer.writerow(['Stalno prebivališče - Pošta', student.posta_naslov.postna_stevilka])
    writer.writerow(['Stalno prebivališče - Kraj', student.posta_naslov.kraj])
    writer.writerow([''])
    writer.writerow([''])

    writer.writerow(['Naslov za dostavo pošte'])
    writer.writerow(['Prejem - Država', student.posta_prejem.drzava])
    writer.writerow(['Prejem - Ulica', student.ulica_prejem])
    writer.writerow(['Prejem - Pošta', student.posta_prejem.postna_stevilka])
    writer.writerow(['Prejem - Kraj', student.posta_prejem.kraj])
    writer.writerow([''])
    writer.writerow([''])

    if vpisi:
        writer.writerow(['Podatki o vpisih'])
        for vpis in vpisi:
            writer.writerow(['Študijsko leto',vpis.leto_vpisa.studijsko_leto])
            writer.writerow(['Letnik',vpis.letnik])
            writer.writerow(['Študijski program',vpis.smer.sifra + ' - ' + vpis.smer.ime])
            writer.writerow(['Vrsta vpisa',vpis.vrsta.sifra + ' - ' + vpis.vrsta.vrsta])
            writer.writerow(['Način študija',vpis.tip.sifra + ' - ' + vpis.tip.nacin])
            writer.writerow([''])

    return response


def generate_csv_predmeti(request, pk):
    predmet = Predmet_leto.objects.select_related('predmet', 'profesor', 'studijsko_leto').get(pk=pk)
    students_ref = Evidenca_student.objects.filter(predmet=predmet)
    students = Student.objects.prefetch_related('coin_set').filter(id__in=students_ref.values('student_id'))

    response = HttpResponse(content_type='text/csv')
    filename = "VpisaniPredmet.csv"
    response['Content-Disposition'] = 'attachment; filename="%s"' % (filename)
    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)

    date = datetime.date.today()
    writer.writerow(['Datum izpisa', date])
    writer.writerow(['Pregled vpisanih študentov v predmet'])
    writer.writerow([])
    writer.writerow(['Predmet',predmet.predmet.naslov_predmeta])
    writer.writerow(['Sifra predmeta',predmet.predmet.sifra])
    writer.writerow(['Nosilec predmeta',predmet.profesor.first_name + ' ' + predmet.profesor.last_name])
    writer.writerow(['Smer',predmet.predmet.smer])
    writer.writerow(['Študijsko leto',predmet.studijsko_leto])

    if students:
        writer.writerow([])
        writer.writerow([])
        writer.writerow(['Vpisani v predmet'])
        writer.writerow(['Št.','Vpisna številka','Ime','Priimek','Email','Vrsta vpisa'])
        k = 1
        for student in students:
            writer.writerow([k, student.vpisna,student.first_name,student.last_name,student.email,student.vpis_set.last().vrsta.sifra + " - " + student.vpis_set.last().vrsta.vrsta])
            k = k + 1
    return response

def generate_pdf_predmeti(request, pk):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="VpisaniPredmet.pdf"'
    buffer = BytesIO()
    report = MyPrintPredmet(buffer, 'Letter', pk)
    pdf = report.print_users()
    response.write(pdf)
    return response

@login_required
@user_passes_test(is_osebje)
def predmet_list(request):
    request_copy = request.GET.copy()
    if 'studijsko_leto' not in request_copy:
        request_copy['studijsko_leto'] = '4'
    if request.method == 'GET':
        if request.user.groups.filter(name="profesor").exists():
            predmeti = Predmet_leto.objects.filter(profesor=request.user)
            subject_filter = Subject_list_filter(request_copy,request=request,queryset=predmeti)
            filter_form = subject_filter.form
            paginator = Paginator(subject_filter.qs, 10)
            page = request.GET.get('page')
            try:
                response_data = paginator.page(page)
            except PageNotAnInteger:
                response_data = paginator.page(1)
            except EmptyPage:
                response_data = paginator.page(paginator.num_pages)
            return render(request, 'predmet_list.html', { 'subjects': response_data, 'form' : filter_form})
        else:
            predmeti = Predmet_leto.objects.select_related('studijsko_leto','predmet').all()
            subject_filter = Subject_list_filter(request_copy,queryset=predmeti)
            filter_form = subject_filter.form
            paginator = Paginator(subject_filter.qs, 10)
            page = request.GET.get('page')
            try:
                response_data = paginator.page(page)
            except PageNotAnInteger:
                response_data = paginator.page(1)
            except EmptyPage:
                response_data = paginator.page(paginator.num_pages)
            return render(request, 'predmet_list.html', { 'subjects': response_data, 'form' : filter_form})


@login_required
@user_passes_test(is_osebje)
def seznam_prijavljenih_izpis(request, pk1, pk):
    """

    :param request:
    :param pk1: predmet_leto
    :param pk: izpit
    :return:
    """

    if request.method == 'GET':

        ''' POTREBNO DOPOLNITI DA SE OB KLICU GET NAFILAJO PODATKI FORMA - OCENA, TOCKE, KER TAKO ZAHTEVA ZGODBA - GREM SPAT'''
        izpit = Izpit.objects.get(pk=pk)
        predmet = Predmet_leto.objects.select_related('predmet', 'profesor', 'studijsko_leto').get(pk=pk1)
        prijavljeni_izpit = Evidenca_izpit.objects.filter(izpit=izpit).all().order_by('student__last_name')
        print(predmet.profesor.pk)
        nosilci = Nosilci.objects.filter(predmet_leto=predmet).all()
        for x in nosilci:
            print(x.nosilec)
        polaganja = []
        polaganja_pon = []
        polaganja_letos = []
        stud_leto_prev = studijskoLeto(1)
        for x in prijavljeni_izpit:
            stOpravljanjLetos = Evidenca_izpit.objects.filter(izpit__predmet=izpit.predmet, student_id=x.student.id,
                                                              ocena__isnull=False).count()
            stOpr = SteviloOpravljanj.objects.filter(student_id=x.student.id,
                                                     predmet=izpit.predmet.predmet).last()

            stVeljavnihOpravljanj = 0
            stDejanskihOpravljanj = 0
            stPopravljanj = 0
            if stOpr is not None:
                stDejanskihOpravljanj = stOpr.stVseh
                stPopravljanj = stOpr.stPop
                stVeljavnihOpravljanj = stDejanskihOpravljanj - stPopravljanj
            polaganja.append(stDejanskihOpravljanj+1)
            polaganja_letos.append(stOpravljanjLetos)
            polaganja_pon.append(stPopravljanj)

        polaganja = polaganja[::-1]
        polaganja_pon = polaganja_pon[::-1]
        polaganja_letos = polaganja_letos[::-1]
        polaganja_pon_1 = polaganja_pon.copy()
        user_filter = Evidenca_izpit_filter(request.GET, queryset=prijavljeni_izpit)
        filter_form = user_filter.form
        paginator = Paginator(user_filter.qs, 10)
        page = request.GET.get('page')

        try:
            response_data = paginator.page(page)
        except PageNotAnInteger:
            response_data = paginator.page(1)
        except EmptyPage:
            response_data = paginator.page(paginator.num_pages)


        filtriran_datum = single_date_filter(izpit)
        form_prijavljeni = IzpitOceneTockeForm()
        return render(request, 'seznam_prijavljenih_izpit_izpis.html', {'nosilci':nosilci, 'form': filter_form, 'prijaljeni_izpit': response_data, 'form_prijavljeni': form_prijavljeni, 'predmet': predmet, 'izpit': izpit, 'filtriran_datum': filtriran_datum, 'polaganja': polaganja, 'polaganja_pon': polaganja_pon, 'polaganja_letos':polaganja_letos, 'ponavljanja_pon_1':polaganja_pon_1})



@login_required
@user_passes_test(is_osebje)
def seznam_prijavljenih(request, pk1, pk):
    """

    :param request:
    :param pk1: predmet_leto
    :param pk: izpit
    :return:
    """

    if request.method == 'GET':
        ''' POTREBNO DOPOLNITI DA SE OB KLICU GET NAFILAJO PODATKI FORMA - OCENA, TOCKE, KER TAKO ZAHTEVA ZGODBA - GREM SPAT'''
        izpit = Izpit.objects.get(pk=pk)
        predmet = Predmet_leto.objects.select_related('predmet', 'profesor', 'studijsko_leto').get(pk=pk1)
        nosilci = Nosilci.objects.filter(predmet_leto=predmet).all()
        """SPREMEMBE STATUS = TRUE FALSE..."""
        prijavljeni_izpit = Evidenca_izpit.objects.filter(izpit=izpit, status=True).all().order_by('student__last_name')
        student=[]
        referant = request.user.email
        prikaz = True
        if referant == "admin@studis.si" or referant == 'referent@studis.si':
            prikaz = False

        for x in prijavljeni_izpit:
            if x.status == True:
                student.append(x.student)
        students_ref = Evidenca_student.objects.filter(predmet=predmet).order_by('student__last_name')
        predmeti=[]
        for x in students_ref:
            if x.student in student:
                pass
            else:
                predmeti.append(x.student)
        user_filter = Evidenca_izpit_filter(request.GET, queryset=prijavljeni_izpit)
        filter_form = user_filter.form
        paginator = Paginator(user_filter.qs, 10)
        page = request.GET.get('page')

        try:
            response_data = paginator.page(page)
        except PageNotAnInteger:
            response_data = paginator.page(1)
        except EmptyPage:
            response_data = paginator.page(paginator.num_pages)


        filtriran_datum = single_date_filter(izpit)
        form_prijavljeni = IzpitOceneTockeForm()
        form_test = IzpitOceneTockeFormSet()

        return render(request, 'seznam_prijavljenih_izpit.html', {'nosilci':nosilci, 'form': filter_form,'prikaz':prikaz, 'prijaljeni_izpit': response_data, 'form_prijavljeni': form_prijavljeni, 'predmet': predmet, 'izpit': izpit, 'filtriran_datum': filtriran_datum,'neprijavljeni':predmeti,'form_test':form_test})
    else:
        formset = IzpitOceneTockeFormSet(request.POST, request.FILES)
        izpit = Izpit.objects.get(pk=pk)
        print(izpit.id)
        predmet = Predmet_leto.objects.filter(pk=pk1).last()
        nosilci = Nosilci.objects.filter(predmet_leto=predmet).all()
        """
            VNOS TOCK IN OCEN
        """
        for x in formset:
            student = None
            evidenca_izpit_vpisi = None
            evidenca_izpit_vpisi = None
            for y in x:
                if y.data is not None and y.name is not None:
                    #print(y.data, y.name)
                    if y.name == 'student':
                        print(y.data)
                        student = Student.objects.get(pk=y.data)
                        evidenca_student = Evidenca_student.objects.filter(predmet=predmet, student=student).get()
                        evidenca_izpit_vpisi = Evidenca_izpit.objects.filter(izpit=izpit, student=student).get()
                    if y.name == 'tocke' and y.data is not '':
                        evidenca_izpit_vpisi.tocke = int(float(y.data))
                    if y.name == 'ocena' and y.data is not '':
                        evidenca_izpit_vpisi.ocena = int(float(y.data))
                        evidenca_student.ocena = int(float(y.data))
                    if y.name == 'ocena_pisni' and y.data is not '':
                        evidenca_izpit_vpisi.ocena_pisni = int(float(y.data))
                    if y.name == 'status' and y.data is False and evidenca_izpit_vpisi is not None:
                        print(evidenca_izpit_vpisi)
                        evidenca_izpit_vpisi.status = False
                        evidenca_izpit_vpisi.datum_odjave = datetime.datetime.now()
                        evidenca_izpit_vpisi.odjavitelj = request.user.email
                    elif y.name == 'status' and y.data is True:
                        evidenca_izpit_vpisi.status = True
                    if evidenca_izpit_vpisi is not None:
                        evidenca_izpit_vpisi.save()
                    evidenca_student.save()
            #print("TEST:", x.data)
            #print(Student.objects.get(pk=x.data['evidenca_izpit_set-2-student']))
            #print(Student.objects.get(pk=x.data['evidenca_izpit_set-3-student']))

        return HttpResponseRedirect(reverse('seznam_prijavljenih', args=[pk1, pk, ]))



"""

    TO NAJ SE VEČ NE BI UPORABLJALO
    
"""

@login_required
@user_passes_test(is_osebje)
def seznam_prijavljenih_vpis(request, pk1, pk, pk2):
    """

    :param request:
    :param pk1: predmet_leto
    :param pk: izpit
    :param pk2: student
    :return:
    """

    if request.method == 'GET':
        return redirect('https://studis-tpo.herokuapp.com/osebje/predmeti/'+pk1+'/'+pk+'/seznamPrijavljenih/')

    if request.method == 'POST':
        test = IzpitOceneTockeFormSet()
        for x in test:
            print(x)
        izpit = Izpit.objects.get(pk=pk)
        prijavljeni_izpit = Evidenca_izpit.objects.filter(izpit=izpit).all().order_by('student__last_name')
        user_filter = Evidenca_izpit_filter(request.GET, queryset=prijavljeni_izpit)
        filter_form = user_filter.form
        paginator = Paginator(user_filter.qs, 10)
        page = request.GET.get('page')


        try:
            response_data = paginator.page(page)
        except PageNotAnInteger:
            response_data = paginator.page(1)
        except EmptyPage:
            response_data = paginator.page(paginator.num_pages)

        form_izpit = IzpitOceneTockeForm(request.POST)
        if form_izpit.is_valid():
            data = form_izpit.cleaned_data
            ''' POTREBO POPRAVITI DA POSODOBI OBSTOJECEGA NE PA DA DODA NOVEGA - GREM SPAT 
            '''
            print(data)
            izpit = Izpit.objects.get(pk=pk)
            student = Student.objects.get(pk=pk2)
            evidenca_izpit_vpisi = Evidenca_izpit.objects.filter(izpit=izpit, student=student).last()
            data = simplejson.dumps(data)
            json_data = simplejson.loads(data)
            print(json_data)
            if json_data["tocke"] is not None:
                evidenca_izpit_vpisi.tocke = int(json_data["tocke"])
            if json_data["ocena"] is not None:
                evidenca_izpit_vpisi.ocena = int(json_data["ocena"])
            if json_data["ocena_pisni"] is not None:
                evidenca_izpit_vpisi.ocena_pisni = int(json_data["ocena_pisni"])
            predmet = Predmet_leto.objects.filter(pk=pk1).last()
            evidenca_student = Evidenca_student.objects.filter(predmet=predmet, student=student).last()
            if json_data["ocena"] is not None:
                evidenca_student.ocena = int(json_data["ocena"])
            evidenca_student.save()
            if json_data['vrni_prijavo'] is '1':
                evidenca_izpit_vpisi.status = False
                evidenca_izpit_vpisi.datum_odjave = datetime.datetime.now()
                evidenca_izpit_vpisi.odjavitelj = request.user.email
            else:
                evidenca_izpit_vpisi.status = True
            evidenca_izpit_vpisi.save()
            evidenca_student.save()
            

            predmet = Predmet_leto.objects.select_related('predmet','profesor','studijsko_leto').get(pk=pk1)
            prijavljeni_izpit = Evidenca_izpit.objects.filter(izpit=izpit).all().order_by('student__last_name')
            filtriran_datum = single_date_filter(izpit)
            form_prijavljeni = IzpitOceneTockeForm()


            return render(request, 'seznam_prijavljenih_izpit.html', {'form': filter_form, 'prijaljeni_izpit': prijavljeni_izpit, 'form_prijavljeni': form_prijavljeni, 'predmet': predmet, 'izpit': izpit, 'filtriran_datum': filtriran_datum, })

@login_required
@user_passes_test(is_osebje)
def predmet_details(request, pk):
    if request.method == 'GET':
        predmet = Predmet_leto.objects.select_related('predmet','profesor','studijsko_leto').get(pk=pk)
        nosilci = Nosilci.objects.filter(predmet_leto=predmet).all()
        izpiti = Izpit.objects.select_related().filter(predmet=pk, locked=False)
        filtrirani_datumi = date_filter(izpiti)
        izpiti = zip(filtrirani_datumi, izpiti)

        studijsko_leto = predmet.studijsko_leto
        #potrjeni = Vpis.objects.filter(potrjen = True,leto_vpisa=studijsko_leto).values_list('student_id',flat=True)
        students_ref = Evidenca_student.objects.filter(predmet = predmet).values_list('student_id',flat=True)
        intersection = [id for id in students_ref]
        students = Student.objects.prefetch_related('vpis_set').filter(id__in=intersection)
        user_filter = Student_list_predmet_filter(request.GET,queryset=students)
        filter_form = user_filter.form
        form_rok = IzpitForm(pk=predmet.id)
        paginator = Paginator(user_filter.qs, 10)
        page = request.GET.get('page')
        try:
            response_data = paginator.page(page)
        except PageNotAnInteger:
            response_data = paginator.page(1)
        except EmptyPage:
            response_data = paginator.page(paginator.num_pages)
        return render(request, 'predmet_seznam_studentov.html', {'students': response_data, 'subject' : predmet, 'form': filter_form, 'izpit_form': form_rok, 'izpiti': izpiti, 'datum_izpita': filtrirani_datumi, 'nosilci': nosilci})

def single_date_filter(izpit):
    tmp_date = []
    tmp_date = izpit.datum.strftime("%B %d, %Y").split(" ")
    if tmp_date[0] == "May":
        tmp_date[0] = "Maj"
    elif tmp_date[0] == "Aug":
        tmp_date[0] = "Avg"
    elif tmp_date[0] == "Oct":
        tmp_date[0] = "Okt"
    dan = tmp_date[1].split(",")
    date = dan[0] + ' ' + tmp_date[0] + ', ' + tmp_date[2]
    return date

def date_filter(izpiti):
    filtrirani = []
    for izpit in izpiti:
        tmp_date = []
        tmp_date = izpit.datum.strftime("%B %d, %Y").split(" ")
        if tmp_date[0] == "May":
            tmp_date[0] = "Maj"
        elif tmp_date[0] == "Aug":
            tmp_date[0] = "Avg"
        elif tmp_date[0] == "June":
            tmp_date[0] = "Junij"
        elif tmp_date[0] == "July":
            tmp_date[0] = "Julij"
        elif tmp_date[0] == "Oct":
            tmp_date[0] = "Okt"

        dan = tmp_date[1].split(",")
        date = dan[0] + ' ' + tmp_date[0] + ', ' + tmp_date[2]
        filtrirani.append(date)

    return filtrirani


@login_required
@user_passes_test(is_osebje)
def potrdi_vpis(request):
    if request.method == 'GET':
        print("aloj")
        vpisi=Vpis.objects.all().order_by('student__last_name')

        vpis_filter = Vpisi_filter(request.GET,queryset=vpisi)
        filter_form = vpis_filter.form 

        paginator = Paginator(vpis_filter.qs, 10)
        page = request.GET.get('page')

        try:
            response_data = paginator.page(page)
        except PageNotAnInteger:
            response_data = paginator.page(1)
        except EmptyPage:
            response_data = paginator.page(paginator.num_pages)
        return render(request, 'potrdi_vpis.html',{'vpisi':response_data, 'form' : filter_form})

def funkcija(request, pk):
    print("aloha")
    vpis=Vpis.objects.get(pk= pk)
    print(vpis.letnik)
    #leto=Studijsko_leto.objects.last()
    #zeton.used=True
    #zeton.save()
    #vpis=Vpis(letnik=zeton.letnik, vrsta=zeton.vrsta, student=zeton.student, status='noidea',leto_vpisa=leto,
    #          tip=zeton.nacin, smer=zeton.smer).save()
    ime=vpis.student.first_name+" "+vpis.student.last_name
    letnik=str(vpis.letnik)
    message=str(ime+" uspesno vpisan v "+letnik+" letnik.")

    messages.success(request, message)
    vpis.potrjen=True
    vpis.save()

    vpisi = Vpis.objects.all().order_by('student__last_name')

    vpis_filter = Vpisi_filter(request.GET, queryset=vpisi)
    filter_form = vpis_filter.form

    paginator = Paginator(vpis_filter.qs, 10)
    page = request.GET.get('page')

    try:
        response_data = paginator.page(page)
    except PageNotAnInteger:
        response_data = paginator.page(1)
    except EmptyPage:
        response_data = paginator.page(paginator.num_pages)
    return render(request, 'potrdi_vpis.html', {'vpisi': response_data, 'form': filter_form})

def potrdilo_vpis(request, pk):
    template = get_template('potrdilo_vpis.html')
    vpis = Vpis.objects.get(pk=pk)
    datum=datetime.datetime.now().strftime("%d.%m.%Y")
    potrdilo=PotrdiloVpisa.objects.first()
    stevilka=int(potrdilo.stevilka)
    stevilka1=stevilka+1
    stevilka2=stevilka+2
    stevilka3=stevilka+3
    stevilka4=stevilka+4
    stevilka5 = stevilka + 5
    potrdilo.stevilka=potrdilo.stevilka+6
    potrdilo.save()
    context = {
            "vpisna": vpis.student.vpisna,
            "student": vpis.student.first_name+" "+vpis.student.last_name,
            "datum_rojstva": vpis.student.datum_rojstva.strftime("%d.%m.%Y"),
            "kraj_rojstva": vpis.student.kraj_rojstva,
            "drzava_rojsva": vpis.student.drzava.ime_drzave,
            "priimekime": vpis.student.last_name+","+vpis.student.first_name,
            "studijsko_leto": vpis.leto_vpisa,
            "vrsta_vpisa": vpis.vrsta.vrsta.format,
            "nacin": vpis.tip.nacin,
            "letnik": str(vpis.letnik)+". letnik",
            "program": "racunalnistvo in informatika",
            "smer": vpis.smer,
            "vrsta": vpis.vrsta,
            "ulica": vpis.student.ulica_naslov,
            "posta": vpis.student.posta_naslov,
            "datum": datum,
            "stevilka": stevilka,
            "stevilka1": stevilka1,
            "stevilka2": stevilka2,
            "stevilka3": stevilka3,
            "stevilka4": stevilka4,
            "stevilka5": stevilka5
    }
    html = template.render(context)
    pdf = render_to_pdf('potrdilo_vpis.html', context)
    if pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        filename = "Invoice_%s.pdf" % ("12341231")
        content = "inline; filename='%s'" % (filename)
        download = request.GET.get("download")
        if download:
            content = "attachment; filename='%s'" % (filename)
        response['Content-Disposition'] = content
        return response
    return HttpResponse("Not found")

def render_to_pdf(template_src, context_dict={}):
     template = get_template(template_src)
     html  = template.render(context_dict)
     result = BytesIO()
     pdf = pisa.pisaDocument(BytesIO(html.encode("windows-1250")), result)
     if not pdf.err:
         return HttpResponse(result.getvalue(), content_type='application/pdf')
     return None

@login_required
@user_passes_test(is_osebje)
def Kartoteka2(request,pk):
    student = Student.objects.get(pk=pk)
    studijsko_leto = request.GET.get('studijsko_leto')
    if studijsko_leto is '0' or studijsko_leto is None:
        vpisi = Vpis.objects.filter(student=student)
    else:
        vpisi = Vpis.objects.filter(student=student,leto_vpisa_id=studijsko_leto)

    polaganja = request.GET.get('polaganja')
    
    data = [[x] for x in vpisi]
    predmeti_evidenca = Evidenca_student.objects.filter(student=student)
    predmeti_id = [x.predmet.id for x in predmeti_evidenca]
    izpit_evidenca = Evidenca_izpit.objects.filter(student=student)
    for i,vpis in enumerate(vpisi):
        subjects = Predmet_leto.objects.filter(studijsko_leto=vpis.leto_vpisa,id__in=predmeti_id)
        izpiti = Izpit.objects.filter(predmet__in=subjects).order_by('datum')
        predmeti = [[x]for x in subjects]
        ocene = []
        maxECTS = 0
        doneECTS = 0
        for predmet in predmeti:
            izpiti_studenta = izpiti.filter(predmet_id=predmet[0].id)
            izpiti_evidenca = izpit_evidenca.filter(izpit__in=izpiti_studenta,ocena__gte=1).order_by('izpit__datum')
            izpitus = [[x] for x in izpiti_evidenca]
            evidenca_studenta = predmeti_evidenca.filter(predmet=predmet[0])
            predmet.append(izpiti_studenta) 
            predmet.append(izpitus)
            predmet.append(evidenca_studenta)
            maxECTS += predmet[0].predmet.kreditne_tocke
            if izpiti_evidenca.last() is not None and izpiti_evidenca.last().ocena is not None:
                ocena = izpiti_evidenca.last().ocena
                if ocena >= 5:
                    ocene.append(ocena)
                    doneECTS += predmet[0].predmet.kreditne_tocke
            else:
                ocene.append(0)
        avg = 0
        if len(ocene) is not 0:
            avg = sum(ocene) / len(ocene)
        ECTS = str(doneECTS) + "/" + str(maxECTS)
        extra = {'avg' : avg, 'ECTS' : ECTS}
        data[i].append(predmeti)
        data[i].append(extra)

    return render(request, 'indeks.html', {'student' : student, 'data' : data, 'polaganje' : polaganja})

@login_required
@user_passes_test(is_osebje)
def Kartoteka3(request,pk):
    student = Student.objects.get(pk=pk)
    studijsko_leto = request.GET.get('studijsko_leto')
    if studijsko_leto is '0' or studijsko_leto is None:
        vpisi = Vpis.objects.filter(student=student)
    else:
        vpisi = Vpis.objects.filter(student=student,leto_vpisa_id=studijsko_leto)

    polaganja = request.GET.get('polaganja')
    
    data = [[x] for x in vpisi]
    predmeti_evidenca = Evidenca_student.objects.filter(student=student)
    predmeti_id = [x.predmet.id for x in predmeti_evidenca]
    izpit_evidenca = Evidenca_izpit.objects.filter(student=student)
    for i,vpis in enumerate(vpisi):
        subjects_help = Predmet_leto.objects.filter(studijsko_leto=vpis.leto_vpisa,id__in=predmeti_id)
        subjects = [x.predmet for x in subjects_help]
        
        #izpiti = Izpit.objects.filter(predmet__in=subjects).order_by('datum')
        predmeti = [[x]for x in subjects]
        ocene = []
        maxECTS = 0
        doneECTS = 0
        for predmet in predmeti:
            a = Predmet_leto.objects.filter(predmet=predmet,studijsko_leto=vpis.leto_vpisa)
            p = [x.id for x in a] # current year

            for vpis_dup in vpisi:
                if vpis_dup == vpis:
                    continue
                elif vpis_dup.leto_vpisa.studijsko_leto > vpis.leto_vpisa.studijsko_leto:
                    c = Predmet_leto.objects.filter(predmet=subject,studijsko_leto=vpis_dup.leto_vpisa)
                    c_a = [p.append(x.id) for x in c]
                    extra_ps = len(c)
            ####
            izpiti_studenta = izpiti.filter(predmet_id=predmet[0].id)
            izpiti_evidenca = izpit_evidenca.filter(izpit__in=izpiti_studenta,ocena__gte=1).order_by('izpit__datum')
            izpitus = [[x] for x in izpiti_evidenca]
            evidenca_studenta = predmeti_evidenca.filter(predmet=predmet[0])
            predmet.append(izpiti_studenta) 
            predmet.append(izpitus)
            predmet.append(evidenca_studenta)
            maxECTS += predmet[0].predmet.kreditne_tocke
            if izpiti_evidenca.last() is not None and izpiti_evidenca.last().ocena is not None:
                ocena = izpiti_evidenca.last().ocena
                if ocena >= 5:
                    ocene.append(ocena)
                    doneECTS += predmet[0].predmet.kreditne_tocke
            else:
                ocene.append(0)
        avg = 0
        if len(ocene) is not 0:
            avg = sum(ocene) / len(ocene)
        ECTS = str(doneECTS) + "/" + str(maxECTS)
        extra = {'avg' : avg, 'ECTS' : ECTS}
        data[i].append(predmeti)
        data[i].append(extra)

    return render(request, 'indeks.html', {'student' : student, 'data' : data, 'polaganje' : polaganja})


@login_required
@user_passes_test(is_osebje)
def Kartoteka(request,pk):
    student = Student.objects.get(pk=pk)
    studijsko_leto = request.GET.get('studijsko_leto')
    polaganja = request.GET.get('polaganja')

    vpisi = Vpis.objects.filter(student=student)
    data = [[x] for x in vpisi] 
    predmeti_evidenca = Evidenca_student.objects.filter(student=student)
    predmeti_id = [x.predmet.id for x in predmeti_evidenca]
    for s,vpis in enumerate(vpisi):
        ocene = []
        maxECTS = 0
        doneECTS = 0

        entry = []
        subjects_help = Predmet_leto.objects.filter(studijsko_leto=vpis.leto_vpisa,id__in=predmeti_id)
        subjects = [x.predmet for x in subjects_help]

        for subject in subjects:
            xs = [subject]
            izpitus = []
            maxECTS += subject.kreditne_tocke
            for vpis_dup in vpisi:
                c = Predmet_leto.objects.filter(predmet=subject,studijsko_leto=vpis_dup.leto_vpisa)
                p_a = [x.id for x in c]
                b = Evidenca_student.objects.filter(student=student,predmet__in=p_a)
                p_l = [x.predmet for x in b]
                izpiti = Izpit.objects.filter(predmet__in=p_l).order_by('datum')
                izpit_student = Evidenca_izpit.objects.filter(student=student,izpit__in=izpiti,status=True)
                for i,izpit in enumerate(izpit_student):
                    container = []
                    container.append(izpit)
                    extra_letos = str(i+1)
                    print(extra_letos)
                    container.append(extra_letos)
                    izpitus.append(container)
            ####
            stevilo_opravljanj = SteviloOpravljanj.objects.get(student=student,predmet=subject)
            for i,izpit in enumerate(izpitus):
                if i+1 <= 3:
                    extra_op = '(' + str(i+1) + '-0)'
                    izpit.append(extra_op)
                    print(extra_op)
                elif i+1 > 3:
                    extra_op = '(' + str(i+1) + '-' + str(stevilo_opravljanj.stPop) + ')'
                    print(extra_op)
                    izpit.append(extra_op)
            last_izpit = None
            if len(izpitus) is not 0:
                last_izpit = izpitus[-1:][0][0]
            print(last_izpit)
            if last_izpit is not None:
                ocena = last_izpit.ocena
                if ocena is not None and ocena > 5:
                    ocene.append(ocena)
                    doneECTS += subject.kreditne_tocke
            xs.append(izpitus)
            entry.append(xs)
        avg = 0
        if len(ocene) is not 0:
            avg = sum(ocene) / len(ocene)
        ECTS = str(doneECTS) + "/" + str(maxECTS)
        extra = {'avg' : avg, 'ECTS' : ECTS}
        data[s].append(entry)   
        data[s].append(extra)

    if studijsko_leto is '0' or studijsko_leto is None:
        pass
    else:
        mark = 0
        if studijsko_leto == '1':
            for vpis in data:
                if vpis[0].leto_vpisa.studijsko_leto == '2014/2015':
                    data = [vpis]
                    mark = 1
        if studijsko_leto == '2':
            for vpis in data:
                if vpis[0].leto_vpisa.studijsko_leto == '2015/2016':
                    data = [vpis]
                    mark = 1
        if studijsko_leto == '3':
            for vpis in data:
                if vpis[0].leto_vpisa.studijsko_leto == '2016/2017':
                    data = [vpis]
                    mark = 1
        if studijsko_leto == '4':
            for vpis in data:
                if vpis[0].leto_vpisa.studijsko_leto == '2017/2018':
                    data = [vpis]
                    mark = 1
        if studijsko_leto == '5':
            for vpis in data:
                if vpis[0].leto_vpisa.studijsko_leto == '2018/2019':
                    data = [vpis]
                    mark = 1
        
        if mark == 0:
            data = []
    return render(request, 'indeks.html', {'student' : student, 'data' : data, 'polaganje' : polaganja})


def generate_new_pdf_student_kartoteka(request, pk):
    studijsko_leto = request.GET.get('studijsko_leto')
    polaganja = request.GET.get('polaganja')
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="KartotecniList.pdf"'
    buffer = BytesIO()
    report = MyPrintKartoteka(buffer, 'Letter', pk, studijsko_leto, polaganja)
    pdf = report.print_users()
    response.write(pdf)
    return response

def generate_new_csv_student_kartoteka(request, pk):
    #pridobi podatke
    student = Student.objects.get(pk=pk)
    studijsko_leto = request.GET.get('studijsko_leto')
    if studijsko_leto is '0' or studijsko_leto is None:
        vpisi = Vpis.objects.filter(student=student)
    else:
        vpisi = Vpis.objects.filter(student=student,leto_vpisa_id=studijsko_leto)

    polaganja = request.GET.get('polaganja')
    data = [[x] for x in vpisi]
    predmeti_evidenca = Evidenca_student.objects.filter(student=student)
    predmeti_id = [x.predmet.id for x in predmeti_evidenca]
    izpit_evidenca = Evidenca_izpit.objects.filter(student=student)
    for i,vpis in enumerate(vpisi):
        subjects = Predmet_leto.objects.filter(studijsko_leto=vpis.leto_vpisa,id__in=predmeti_id)
        izpiti = Izpit.objects.filter(predmet__in=subjects).order_by('datum')
        predmeti = [[x]for x in subjects]
        ocene = []
        maxECTS = 0
        doneECTS = 0
        for predmet in predmeti:
            izpiti_studenta = izpiti.filter(predmet_id=predmet[0].id)
            izpiti_evidenca = izpit_evidenca.filter(izpit__in=izpiti_studenta).order_by('izpit__datum')
            evidenca_studenta = predmeti_evidenca.filter(predmet=predmet[0])
            predmet.append(izpiti_studenta)
            predmet.append(izpiti_evidenca)
            predmet.append(evidenca_studenta)
            maxECTS += predmet[0].predmet.kreditne_tocke
            ocena = izpiti_evidenca.last().ocena
            if ocena > 5:
                ocene.append(ocena)
                doneECTS += predmet[0].predmet.kreditne_tocke
        avg = sum(ocene) / len(ocene)
        ECTS = str(doneECTS) + "/" + str(maxECTS)
        extra = {'avg' : avg, 'ECTS' : ECTS}
        data[i].append(predmeti)
        data[i].append(extra)

    response = HttpResponse(content_type='text/csv')
    filename = "KartotecniList.csv"
    response['Content-Disposition'] = 'attachment; filename="%s"' % (filename)
    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)

    date = datetime.date.today()
    writer.writerow(['Datum izpisa', date])
    writer.writerow([])
    writer.writerow(['Pregled opravljenih izpitov'])
    writer.writerow(['Vpisna številka','Ime','Priimek'])
    writer.writerow([])
    writer.writerow([])
    writer.writerow([])
    writer.writerow([])
    writer.writerow([student.vpisna, student.first_name, student.last_name])
    for entry in data:
        writer.writerow(['Študijsko leto', entry[0].leto_vpisa])
        writer.writerow(['Smer študija', entry[0].smer])
        writer.writerow(['Letnik', entry[0].letnik])
        writer.writerow(['Vrsta', entry[0].vrsta])
        writer.writerow(['Način', entry[0].tip])
        writer.writerow(['Skupina', 'LJUBLJANA'])
        writer.writerow([])
        writer.writerow([])
        writer.writerow(['#', 'Šifra', 'Naziv predmeta', 'ECTS', 'Izvajalec', 'Datum', 'Ocena', 'Št. polaganj'])
        j = 0
        for subject in entry[1]:
            if polaganja == '0':
                writer.writerow([j, subject[0].predmet.sifra, subject[0].predmet.naslov_predmeta, 
                subject[0].predmet.kreditne_tocke, subject[0].profesor.first_name + ' ' + subject[0].profesor.last_name,
                subject[1][0].datum, subject[2][0].ocena, 1])
                k = 0
                for izpit_pos in subject[1]:
                    if k > 0:
                        writer.writerow(['', '', '', '', '', izpit_pos.datum, subject[2][k].ocena, k+1])
                    k = k + 1
            else:
                writer.writerow([j, subject[0].predmet.sifra, subject[0].predmet.naslov_predmeta, 
                subject[0].predmet.kreditne_tocke, subject[0].profesor.first_name + ' ' + subject[0].profesor.last_name,
                subject[1].last().datum, subject[2].last().ocena, len(subject[1])])
            
            j = j + 1

        print(entry[2]['ECTS'])
        writer.writerow([])
        writer.writerow(['Skupno število kreditnih točk', entry[2]['ECTS']])
        writer.writerow(['Skupna poprečna ocena', entry[2]['avg']])
        writer.writerow([])
        writer.writerow([])
        writer.writerow([])
    return response

def seznam_prijavljenih_izpis_pdf(request, pk1, pk):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="SeznamPrijavljenih.pdf"'
    buffer = BytesIO()
    report = MyPrintPrijavljeniSeznam(buffer, 'Letter', pk1, pk)
    pdf = report.print_users()
    response.write(pdf)
    return response

def seznam_prijavljenih_izpis_csv(request, pk1, pk):

    """

    :param request:
    :param pk1: predmet_leto
    :param pk: izpit
    :return:
    """

    print(pk1)
    print(pk)

    #PODATKI
    izpit = Izpit.objects.get(pk=pk)
    predmet = Predmet_leto.objects.select_related('predmet', 'profesor', 'studijsko_leto').get(pk=pk1)
    prijavljeni_izpit = Evidenca_izpit.objects.filter(izpit=izpit).all()
    filtriran_datum = single_date_filter(izpit)

    #stevilo opravljanj
    polaganja = []
    polaganja_pon = []
    polaganja_letos = []
    stud_leto_prev = studijskoLeto(1)
    for x in prijavljeni_izpit:
        stOpravljanjLetos = Evidenca_izpit.objects.filter(izpit__predmet=izpit.predmet, student_id=x.student.id,
                                                          ocena__isnull=False).count()
        stOpr = SteviloOpravljanj.objects.filter(student_id=x.student.id,
                                                 predmet=izpit.predmet.predmet).last()

        stVeljavnihOpravljanj = 0
        stDejanskihOpravljanj = 0
        stPopravljanj = 0
        if stOpr is not None:
            stDejanskihOpravljanj = stOpr.stVseh
            stPopravljanj = stOpr.stPop
            stVeljavnihOpravljanj = stDejanskihOpravljanj - stPopravljanj
        polaganja.append(stDejanskihOpravljanj + 1)
        polaganja_letos.append(stOpravljanjLetos)
        polaganja_pon.append(stPopravljanj)

    polaganja = polaganja[::-1]
    polaganja_pon = polaganja_pon[::-1]
    polaganja_letos = polaganja_letos[::-1]
    polaganja_pon_1 = polaganja_pon.copy()

    response = HttpResponse(content_type='text/csv')
    filename = "KartotecniList.csv"
    response['Content-Disposition'] = 'attachment; filename="%s"' % (filename)
    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)

    date = datetime.date.today()
    writer.writerow(['Datum izpisa', date])
    writer.writerow([])
    writer.writerow(['Seznam prijavljenih študentov na izpit'])
    writer.writerow([])
    writer.writerow(['Predmet', predmet])
    writer.writerow(['Sifra predmeta', predmet.predmet.sifra])
    writer.writerow(['Študijsko leto', predmet.studijsko_leto])
    writer.writerow(['Izpraševalec', predmet.profesor.first_name + ' ' + predmet.profesor.last_name])
    writer.writerow(['Smer', predmet.predmet.smer])
    writer.writerow(['Datum izpita', filtriran_datum])
    writer.writerow(['Prostor izpita', izpit.prostor])
    writer.writerow(['Ura izpita', izpit.cas])
    writer.writerow([])
    writer.writerow(['#', 'Vpisna številka', 'Ime', 'Priimek', 'Študijsko leto', 'Zap. št. polaganja skupaj', 'Zap. št. polaganja letos', 'Točke', 'Končna ocena'])
    j = 0
    for evidenca in prijavljeni_izpit:
        tocke_tmp = '/'
        kocna_ocena_tmp = '/'
        if evidenca.tocke is not None:
            tocke_tmp = evidenca.tocke
        if evidenca.ocena is not None:
            kocna_ocena_tmp = evidenca.ocena
        if evidenca.status is True:
            if polaganja_pon_1.pop() == 0:
                writer.writerow([j, evidenca.student.vpisna, evidenca.student.first_name, evidenca.student.last_name, predmet.studijsko_leto, polaganja.pop(), polaganja_letos.pop(),tocke_tmp, kocna_ocena_tmp])
            else:
                writer.writerow([j, evidenca.student.vpisna, evidenca.student.first_name, evidenca.student.last_name,
                                 predmet.studijsko_leto, polaganja.pop() + '-' + polaganja_pon.pop(), polaganja_letos.pop(), tocke_tmp, kocna_ocena_tmp])
        else:
            if polaganja_pon_1.pop() == 0:
                writer.writerow([j, evidenca.student.vpisna, evidenca.student.first_name, evidenca.student.last_name, predmet.studijsko_leto, polaganja.pop(), polaganja_letos.pop(), tocke_tmp, 'VP'])
            else:
                writer.writerow([j, evidenca.student.vpisna, evidenca.student.first_name, evidenca.student.last_name, predmet.studijsko_leto, polaganja.pop() + '-' + polaganja_pon.pop(), polaganja_letos.pop(), tocke_tmp, 'VP'])

        j = j + 1


    return response

def odjavistudent(request, pk,pk1, pk2):

    izpit=Evidenca_izpit.objects.filter(pk=pk2).get()
    stOpravljanj=SteviloOpravljanj.objects.filter(student=izpit.student, predmet=izpit.izpit.predmet.predmet).last()
    print(izpit.izpit.predmet.predmet)
    oseba=User.objects.filter(pk=request.user.id).get()
    print(pk, pk1, pk2)
    print(stOpravljanj)
    izpit.status = False
    izpit.odjavitelj = str(oseba)
    datum = datetime.datetime.now().strftime("%d.%m.%Y %H:%M")
    izpit.datum_odjave = datum
    izpit.save()
    if stOpravljanj is not None:
        stOpravljanj.stVseh = stOpravljanj.stVseh - 1
        stOpravljanj.save()

    poskus_odjava = (datetime.datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d")
    datum_izpita = izpit.izpit.datum.strftime("%Y-%m-%d")
    if poskus_odjava < datum_izpita:
        pass
    else:
        message = str("Študentu je rok za prijavo potekel. Vseeno ste ga odjavili")
        messages.success(request, message)
    return redirect('seznam_prijavljenih', pk1=pk1, pk=pk)

def prijavistudent(request, pk,pk1, pk2):
    student=Student.objects.filter(pk=pk2).get()
    izpit=Izpit.objects.filter(pk=pk).last()
    cifra=Evidenca_izpit.objects.filter(izpit=izpit, student_id=student.id).count()

    datum_in_2_days = (datetime.datetime.now() + timedelta(days=2)).strftime("%Y-%m-%d")
    datum_izpita = izpit.datum.strftime("%Y-%m-%d")
    print(datum_in_2_days, datum_izpita)
    ocena1=Evidenca_student.objects.filter(student_id=student.id,predmet=izpit.predmet).get()
    ocena=1
    if ocena1 is not None:
        if ocena1.ocena is not None:
            ocena=ocena1.ocena

    xxx=Evidenca_izpit.objects.filter(izpit__predmet=izpit.predmet, student_id=student.id, ocena__isnull=True)
    print(student.id,izpit.id)
    bla=0
    for x in xxx:
        print(x.status)
        if x.status == True:
            bla=1

    print("hhhh",xxx)

    if bla != 0:
        message = str("Študent je prijavljen že na drugi rok, ki še nima vnešene ocene.")
        messages.success(request, message)

    stOpravljanjLetos = Evidenca_izpit.objects.filter(izpit__predmet=izpit.predmet, student_id=student.id,
                                                      ocena__isnull=False).count()
    stOpr = SteviloOpravljanj.objects.filter(student_id=student.id,
                                     predmet=izpit.predmet.predmet).last()

    stVeljavnihOpravljanj = 0
    stDejanskihOpravljanj = 0
    stPopravljanj = 0
    if stOpr is not None:
        stDejanskihOpravljanj = stOpr.stVseh
        stPopravljanj = stOpr.stPop
        stVeljavnihOpravljanj = stDejanskihOpravljanj - stPopravljanj

    vpisan=True
    vpis=Vpis.objects.filter(student=student).count()
    if vpis == 0:
        vpisan=False
    else:
        vpis=Vpis.objects.filter(student=student).last()
        leto=vpis.leto_vpisa
        letopredmet=Evidenca_izpit.objects.filter(izpit=izpit, student_id=student.id).first()
        blokada=izpit.predmet.studijsko_leto
        print(blokada,leto)
        if blokada == leto:
            pass
        else:
            vpisan=False

    if vpisan == False:
        message = str("Nevpisan študent")
        messages.success(request, message)

    if ocena1 is not None:
        if ocena > 5:
            message = str("Študent ima ta izpit že opravljen")
            messages.success(request, message)

    if stVeljavnihOpravljanj >= 6:
        message = str("Študent je opravljal izpit več kot šestkrat")
        messages.success(request, message)

    if stOpravljanjLetos >= 3:
        message = str("Študent je opravljal izpit več kot trikrat letos")
        messages.success(request, message)

    if stVeljavnihOpravljanj > 2 or vpisan == False:
        message = str("Študent bo mogel plačat izpit. Ocena mu bo vpisana, ko bo poravnal položnico.")
        messages.success(request, message)

    if stOpravljanjLetos != 0:
        imaOceno = Evidenca_izpit.objects.filter(izpit__predmet=izpit.predmet, student_id=student.id,
                                                 ocena__isnull=False).order_by('-izpit__datum').first()
        zadnji_datum = (imaOceno.izpit.datum + timedelta(days=14)).strftime("%Y-%m-%d")
        if (zadnji_datum > datum_izpita):
            message = str("Ni minilo še 14 dni od zadnjega opravljanja.")
            messages.success(request, message)

    if datum_in_2_days > datum_izpita:
        message = str("Prepozna prijava za izpit.")
        messages.success(request, message)

    message = str(
        "(" + str(stDejanskihOpravljanj + 1) + " - " + str(stPopravljanj) + ") = " + str(stVeljavnihOpravljanj + 1))
    messages.success(request, message)
    message = str(
        "Stevilo vseh opravljanj letos:" + str(stOpravljanjLetos+1))
    messages.success(request, message)


    '''if  cifra == 0:
        Evidenca_izpit(izpit=izpit, student=student).save()
    else:
        prijava =Evidenca_izpit.objects.filter(izpit=izpit, student_id=student.id).last()
        prijava.datum_odjave = None
        prijava.odjavitelj = None
        prijava.status = True
        prijava.save()

    dodaj = SteviloOpravljanj.objects.filter(student_id=student.id,
                                             predmet=izpit.predmet.predmet).last()
    if dodaj is not None:
        dodaj.stVseh = dodaj.stVseh + 1
        dodaj.save()'''

    return redirect('OpozoriloPrijava', pk1=pk1, pk=pk, pk2=pk2)

def OpozoriloPrijava(request, pk1,pk, pk2):
    return render(request, 'opozoriloPrijava.html', {'pk': pk, 'pk1': pk1, 'pk2': pk2})

def PotrjenaPrijava(request, pk,pk1, pk2):
    student = Student.objects.filter(pk=pk2).get()
    izpit = Izpit.objects.filter(pk=pk).last()
    print(pk)
    cifra = Evidenca_izpit.objects.filter(izpit=izpit, student_id=student.id).count()

    if  cifra == 0:
        Evidenca_izpit(izpit=izpit, student=student).save()
    else:
        prijava =Evidenca_izpit.objects.filter(izpit=izpit, student_id=student.id).last()
        prijava.datum_odjave = None
        print(izpit.id)
        prijava.odjavitelj = None
        prijava.status = True
        prijava.save()

    dodaj = SteviloOpravljanj.objects.filter(student_id=student.id,
                                             predmet=izpit.predmet.predmet).last()
    if dodaj is not None:
        dodaj.stVseh = dodaj.stVseh + 1
        dodaj.save()

    return redirect('seznam_prijavljenih', pk1=pk1, pk=pk)

def nazajPrijava(request, pk, pk1,pk2):
    return redirect('seznam_prijavljenih', pk1=pk1, pk=pk)

def SpremeniIzpit(request,pk,pk1):
    spremeni = True
    if request.method == 'POST':
        form = IzpitForm1(request.POST,pk=pk,izpit_pk=pk1)
        if form.is_valid():
            print("hej")
            data = form.cleaned_data
            print(data["datum"])
            Izpit.objects.filter(pk=pk1).update(izprasevalec=data["izprasevalec"],datum=data["datum"],cas=data["cas"],prostor=data["prostor"])
            print("hahahahahah")
            return redirect('predmet_details', pk)
    else:
        data = Izpit.objects.filter(pk=pk1).get()
        evidence=Evidenca_izpit.objects.filter(izpit=data)
        count1=0
        count2=0
        for x in evidence:
            if x.ocena is not None or x.ocena_pisni is not None or x.tocke is not None:
                count1+=1
            if x.status == True:
                count2+=1
            print(x.tocke, x.ocena_pisni, x.ocena, count1)

        if count1 != 0:
            message = str("Izpit ima že vpisane ocene. Sprememba nemogoča.")
            messages.success(request, message)
            spremeni=False
        else:
            if count2 != 0:
                message = str("Na izpit je prijavljeno "+str(count2)+" študentov.")
                messages.success(request, message)

        form = IzpitForm1(initial={'datum': data.datum, 'cas': data.cas, 'prostor': data.prostor,
                                 },pk=pk,izpit_pk=pk1)
    return render(request, 'spremeni_izpit.html', {'form': form, 'spremeni':spremeni})

def BrisiIzpit(request,pk,pk1):
    izpiti = Evidenca_izpit.objects.filter(izpit_id=pk1, status=True)
    stevilo=izpiti.count()
    sporocila=[]
    if stevilo != 0:
        message = str("Prijavljenih je "+str(stevilo)+" studentov.")
        sporocila.append(message)
        messages.success(request, message)
    else:
        message = str("Na ta izpit ni prijavljenih študentov")
        sporocila.append(message)
        messages.success(request, message)
    ocena=False
    for izpit in izpiti:
        if izpit.ocena is not None or izpit.tocke is not None:
            ocena=True

    if ocena == True:
        message = str("Izpit nemores zbrisati saj so ze vpisane ocene")
        sporocila.append(message)
        messages.error(request, message)

    return redirect('opozoriloIzpit', pk1=pk1, pk=pk)

def OpozoriloIzpit(request,pk,pk1):
    izpiti = Evidenca_izpit.objects.filter(izpit_id=pk1, status=True)
    ocena = True
    for izpit in izpiti:
        if izpit.ocena is not None or izpit.tocke is not None:
            ocena = False
    return render(request, 'opozorilo.html', {'pk':pk,'pk1':pk1,'true':ocena})

def nazaj(request, pk, pk1):
    return redirect('predmet_details', pk=pk)

def PotrjenBris(request,pk,pk1):
    izpiti = Evidenca_izpit.objects.filter(izpit_id=pk1, status=True)

    for izpit in izpiti:
        student=izpit.student
        predmet=izpit.izpit.predmet
        opravljanja = SteviloOpravljanj.objects.filter(student=student,predmet=predmet.predmet).last()
        opravljanja.stVseh=opravljanja.stVseh-1
        opravljanja.save()
        izpit.delete()
    izpitnirok=Izpit.objects.filter(pk=pk1).delete()


    return redirect('predmet_details', pk=pk)

def DirektenVpisOcene(request,pk,pk1):
    spremeni = True
    datum = None
    if request.method == 'POST':
        form = DirektenVpisOceneForm(request.POST, pk=pk)
        if form.is_valid():
            data = form.cleaned_data
            izpitx=Izpit.objects.filter(datum=data["datum"],predmet_id=pk).count()
            print(izpitx)
            if izpitx == 0:
                prof=Predmet_leto.objects.filter(pk=pk).get()
                izpit=Izpit(datum=data["datum"], cas="09:00", prostor=11,locked=True, izredni=False, predmet_id=pk, izprasevalec=prof.profesor)
                izpit.save()
                evidenca_izpita = Evidenca_izpit(izpit=izpit, student_id=pk1, ocena=data["ocena"], status=True).save()
            else:
                izpit=Izpit.objects.filter(datum=data["datum"],predmet_id=pk).last()
                evidenca_izpita = Evidenca_izpit.objects.filter(izpit=izpit, student_id=pk1).last()
                print(izpit.datum, izpit.predmet)
                print(evidenca_izpita)
                if evidenca_izpita is None:
                    evidenca1=Evidenca_izpit(izpit=izpit, student_id=pk1)
                    evidenca1.ocena = data["ocena"]
                    evidenca1.save()
                    '''prof = Predmet_leto.objects.filter(pk=pk).get()
                    izpit = Izpit(datum=data["datum"], cas="09:00", prostor=11, locked=True, izredni=False,
                                  predmet_id=pk, izprasevalec=prof.profesor)
                    izpit.save()
                    evidenca_izpita = Evidenca_izpit(izpit=izpit, student_id=pk1, ocena=data["ocena"],
                                                     status=False).save()'''
                else:
                    evidenca_izpita.ocena = data["ocena"]
                    evidenca_izpita.save()

            evidenca_student=Evidenca_student.objects.filter(student_id=pk1,predmet_id=pk).get()
            evidenca_student.ocena=data["ocena"]
            evidenca_student.save()
            predmet = Predmet_leto.objects.filter(id=pk).get()
            x= SteviloOpravljanj.objects.filter(student_id=pk1, predmet=predmet.predmet).last()
            x.stVseh=x.stVseh+1
            x.save()
            return redirect('predmet_details', pk)
    else:
        predmet=Predmet_leto.objects.filter(id=pk).get()
        data = SteviloOpravljanj.objects.filter(student_id=pk1,predmet=predmet.predmet).last()

        evidence = Evidenca_izpit.objects.filter(student_id=pk1, status=True, izpit__predmet=predmet,ocena__isnull=True).count()
        evidence1 = Evidenca_izpit.objects.filter(student_id=pk1, status=True, izpit__predmet=predmet, ocena__isnull=True).last()
        print(evidence)
        print(evidence1)
        if evidence != 0:
            spremeni=False
            datum = evidence1.izpit.datum
            message = str("Študent je že vpisan na izpit "+str(datum.strftime("%d.%m.%Y"))+". Lahko mu samo za ta možni datum vnesete oceno.")
            messages.error(request, message)
            stOpravljanjLetos = Evidenca_izpit.objects.filter(izpit__predmet=predmet, student_id=pk1,
                                                              ocena__isnull=False).count()
            form = DirektenVpisOceneForm(pk=pk,
                                         initial={'stPolaganja': data.stVseh + 1, 'stPolaganjaLetos': stOpravljanjLetos + 1,'datum':datum})
        if spremeni is True:
            form = DirektenVpisOceneForm(pk=pk, initial={'stPolaganja': data.stVseh+1, 'stPolaganjaLetos': data.stVseh+1})
    return render(request, 'DirektenVpisOcene.html', {'form': form, 'spremeni': spremeni, 'predmetid':pk, 'datum':datum})

def index_csv(request, pk):

    response = HttpResponse(content_type='text/csv')
    filename = "ElektronskiIndex.csv"
    response['Content-Disposition'] = 'attachment; filename="%s"' % (filename)
    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)

    #PRIDOBI PODATKE
    student = Student.objects.get(pk=pk)
    izpiti_orig = Evidenca_izpit.objects.filter(student=student,ocena__gt=5,status=True).order_by('izpit')
    
    rev = izpiti_orig[::-1]
    seen = {}
    new_list = []
    for izpit in rev:
        if izpit.izpit.predmet.predmet not in seen:
            new_list.append(izpit)
            seen[izpit.izpit.predmet.predmet] = True 
    izpiti_orig = new_list[::-1]

    sumECTS = 0
    for izpit in izpiti_orig:
        sumECTS += izpit.izpit.predmet.predmet.kreditne_tocke

    stOpravljanj = []
    for izpit in izpiti_orig:
        st = SteviloOpravljanj.objects.get(student=student,predmet=izpit.izpit.predmet.predmet)
        stOpravljanj.append(st)
    
    vpisi = Vpis.objects.filter(student=student)
    data = [[x] for x in vpisi]
    predmeti_evidenca = Evidenca_student.objects.filter(student=student)
    predmeti_id = [x.predmet.id for x in predmeti_evidenca]
    izpit_evidenca = Evidenca_izpit.objects.filter(student=student)
    ocene_global = []
    global_ECTS = []
    for i,vpis in enumerate(vpisi):
        subjects = Predmet_leto.objects.filter(studijsko_leto=vpis.leto_vpisa,id__in=predmeti_id)
        izpiti = Izpit.objects.filter(predmet__in=subjects).order_by('datum')
        predmeti = [[x]for x in subjects]
        ocene = []
        doneECTS = 0
        for predmet in predmeti:
            izpiti_studenta = izpiti.filter(predmet_id=predmet[0].id)
            izpiti_evidenca = izpit_evidenca.filter(izpit__in=izpiti_studenta,status=True).order_by('izpit__datum')
            if izpiti_evidenca.last() is not None and izpiti_evidenca.last().ocena is not None:
                ocena = izpiti_evidenca.last().ocena
                if ocena > 5:
                    ocene.append(ocena)
                    ocene_global.append(ocena)
                    doneECTS += predmet[0].predmet.kreditne_tocke
                    global_ECTS.append(predmet[0].predmet.kreditne_tocke)
        avg = 0
        if len(ocene) is not 0:
            avg = sum(ocene) / len(ocene)
        extra = {'avg' : avg, 'ECTS' : doneECTS}
        data[i].append(extra)
        data[i].append(ocene)

    avg = 0
    if len(ocene_global) is not 0:
        avg = sum(ocene_global) / len (ocene_global)
    go = {'len' : len(izpiti_orig),
                'ECTS': sumECTS,
                'avg' : avg}

    date = datetime.date.today()
    writer.writerow(['Datum izpisa', date])
    writer.writerow([])
    writer.writerow(['Elektronski indeks'])
    writer.writerow([])
    writer.writerow(['Ime in priimek', student.first_name + ' ' + student.last_name])
    writer.writerow(['Vpisna', student.vpisna])

    writer.writerow([])
    writer.writerow(['#','Šifra','Naziv predmeta','Ocenil','Datum','Opravljanje','ECTS','Ocena'])
    j = 1
    for entry in izpiti_orig:
        tmp_datum = []
        tmp_datum = str(entry.izpit.datum).split('-')
        writer.writerow([j, entry.izpit.predmet.predmet.sifra, entry.izpit.predmet.predmet.naslov_predmeta, entry.izpit.predmet.profesor.first_name + ' ' + entry.izpit.predmet.profesor.last_name, 
        tmp_datum[2] + '.' + tmp_datum[1] + '.'  + tmp_datum[0], stOpravljanj[j-1].stVseh, entry.izpit.predmet.predmet.kreditne_tocke, entry.ocena])
        j = j + 1
    

    writer.writerow([])
    writer.writerow([])
    writer.writerow(['Poprečne ocene po študijskih letih'])
    writer.writerow([])
    writer.writerow(['#', 'Študijsko leto', 'Število opravljenih izpitv', 'Kreditne tocke', 'Skupno povprečje'])
    j = 1
    for entry in data:
        writer.writerow([j, entry[0].leto_vpisa.studijsko_leto, len(entry[2]), entry[1]['avg'], entry[1]['ECTS']])
        j = j + 1

    writer.writerow([])
    writer.writerow([])
    writer.writerow(['Poprečne ocene po študijskih letih'])
    writer.writerow([])
    writer.writerow(['Število opravljenih izpitov', 'Kreditne točke', 'Skupno povprečje'])
    writer.writerow([len(go), go['ECTS'], go['avg']])

    return response

@login_required
@user_passes_test(is_osebje)
def Elektronski_Indeks(request,pk):
    student = Student.objects.get(pk=pk)
    izpiti_orig = Evidenca_izpit.objects.filter(student=student,ocena__gt=5,status=True).order_by('izpit')
    
    rev = izpiti_orig[::-1]
    seen = {}
    new_list = []
    for izpit in rev:
        if izpit.izpit.predmet.predmet not in seen:
            new_list.append(izpit)
            seen[izpit.izpit.predmet.predmet] = True 
    izpiti_orig = new_list[::-1]

    sumECTS = 0
    for izpit in izpiti_orig:
        sumECTS += izpit.izpit.predmet.predmet.kreditne_tocke

    stOpravljanj = []
    for izpit in izpiti_orig:
        st = SteviloOpravljanj.objects.get(student=student,predmet=izpit.izpit.predmet.predmet)
        stOpravljanj.append(st)
    vpisi = Vpis.objects.filter(student=student)
    data = [[x] for x in vpisi]
    predmeti_evidenca = Evidenca_student.objects.filter(student=student)
    predmeti_id = [x.predmet.id for x in predmeti_evidenca]
    izpit_evidenca = Evidenca_izpit.objects.filter(student=student)
    ocene_global = []
    global_ECTS = []
    print(vpisi)
    for i,vpis in enumerate(vpisi):
        subjects = Predmet_leto.objects.filter(studijsko_leto=vpis.leto_vpisa,id__in=predmeti_id)
        izpiti = Izpit.objects.filter(predmet__in=subjects).order_by('datum')
        predmeti = [[x]for x in subjects]
        ocene = []
        doneECTS = 0
        print("TEST1")
        for predmet in predmeti:
            print("TEST")
            izpiti_studenta = izpiti.filter(predmet_id=predmet[0].id)
            izpiti_evidenca = izpit_evidenca.filter(izpit__in=izpiti_studenta).order_by('izpit__datum')
            if izpiti_evidenca.last() is not None and izpiti_evidenca.last().ocena is not None:
                ocena = izpiti_evidenca.last().ocena
                status = izpit_evidenca.last().status
                print("status")
                if ocena > 5:
                    ocene.append(ocena)
                    ocene_global.append(ocena)
                    doneECTS += predmet[0].predmet.kreditne_tocke
                    global_ECTS.append(predmet[0].predmet.kreditne_tocke)
        avg = 0
        if len(ocene) is not 0:
            avg = sum(ocene) / len(ocene)
        extra = {'avg' : avg, 'ECTS' : doneECTS}
        data[i].append(extra)
        data[i].append(ocene)

    avg = 0
    if len(ocene_global) is not 0:
        avg = sum(ocene_global) / len (ocene_global)
    go = {'len' : len(izpiti_orig),
                'ECTS': sumECTS,
                'avg' : avg}

    return render(request, 'elektronski_indeks.html', {'student' : student, 'izpiti' : izpiti_orig, 'data' : data, 'opr' : stOpravljanj, 'globalOcena' : go}) 

def index_pdf(request, pk):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="IndeksStudenta.pdf"'
    buffer = BytesIO()
    report = MyPrintIndeks(buffer, 'Letter', pk)
    pdf = report.print_users()
    response.write(pdf)
    return response

def NarocenaPotrdila(request):
    narocila=NarociloPotrdil.objects.filter(status=True)
    for x in narocila:
        print (x.student)
    return render(request, 'NarocenaPotrdila.html', {'narocila': narocila})

def NatisniNarocenaPotrdila(request,pk):
    narocilo = None
    try:
        narocilo = NarociloPotrdil.objects.filter(status=True,student_id=pk).get()
        narocilo.status=False
        narocilo.save()
    except:
        error = "Ni naročil"

    template = get_template('NarocenaPotrdilaTiskaj.html')
    vpis = Vpis.objects.filter(student_id=pk).last()
    datum = datetime.datetime.now().strftime("%d.%m.%Y")

    stevilke_potrdil = []
    if narocilo is not None:
        potrdilo = PotrdiloVpisa.objects.first()
        stevilka = int(potrdilo.stevilka)
        for x in range(stevilka, stevilka+narocilo.stevilo):
            stevilke_potrdil.append(x)

        potrdilo.stevilka = potrdilo.stevilka + narocilo.stevilo
        potrdilo.save()
    if vpis is not None:
        context = {
            "vpisna": vpis.student.vpisna,
            "student": vpis.student.first_name + " " + vpis.student.last_name,
            "datum_rojstva": vpis.student.datum_rojstva.strftime("%d.%m.%Y"),
            "kraj_rojstva": vpis.student.kraj_rojstva,
            "drzava_rojsva": vpis.student.drzava.ime_drzave,
            "priimekime": vpis.student.last_name + "," + vpis.student.first_name,
            "studijsko_leto": vpis.leto_vpisa,
            "vrsta_vpisa": vpis.vrsta.vrsta.format,
            "nacin": vpis.tip.nacin,
            "letnik": str(vpis.letnik) + ". letnik",
            "program": "racunalnistvo in informatika",
            "smer": vpis.smer,
            "vrsta": vpis.vrsta,
            "ulica": vpis.student.ulica_naslov,
            "posta": vpis.student.posta_naslov,
            "datum": datum,
            "stevilka": stevilke_potrdil,
        }
        html = template.render(context)
        pdf = render_to_pdf('NarocenaPotrdilaTiskaj.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "Invoice_%s.pdf" % ("12341231")
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" % (filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")
    else:
        narocila = NarociloPotrdil.objects.filter(status=True)
        errors = []
        error = "Študent še nima nobenih vpisov"
        errors.append(error)
        return render(request, 'NarocenaPotrdila.html', {'narocila': narocila, 'error': errors})



    return redirect('NarocenaPotrdila')


def seznam_narocil(request):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    filename = "Seznam_narocil.csv"
    response['Content-Disposition'] = 'attachment; filename="%s"' % (filename)
    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)
    print("KLIK")
    writer = csv.writer(response)
    narocila = NarociloPotrdil.objects.filter(status=True).all()
    if narocila is not None:
        count = 1
        writer.writerow(['#', 'Vpisna številka', 'Ime', 'Priimek', 'Število potrdil', 'Datum'])
        for narocilo in narocila:
            writer.writerow([count, narocilo.student.vpisna, narocilo.student.first_name, narocilo.student.last_name, narocilo.stevilo, narocilo.datum_narocila.strftime("%d.%m.%Y")])
            count = count + 1
        return response