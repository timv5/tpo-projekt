from django import forms
import datetime
from Studis.validators import *
from Studis.error_messages import *
from Studis.models.subjects import Nosilci
from Studis.models.storitve import Vrsta_vpis, Nacin_studij, Smer, Oblika_studij, Izpit
import holidays
class IzpitForm(forms.Form):
    pk = None
    izprasevalec = forms.ModelChoiceField(queryset=Nosilci.objects.none(),empty_label = None)
    datum = forms.DateField(label="Datum izpita",initial=datetime.date.today)
    cas = forms.TimeField(widget=forms.TimeInput(format='%H:%M'))
    prostor = forms.CharField(max_length=99)
    izredni = forms.BooleanField(label='Izredni izpit',required= False)
    def __init__(self, *args, **kwargs):
        if kwargs:
            self.pk = kwargs.pop('pk')
        super(IzpitForm, self).__init__(*args, **kwargs)
        nosilci = Nosilci.objects.filter(predmet_leto=self.pk)
        profesorji = [x.nosilec.id for x in nosilci]
        self.fields['izprasevalec'].queryset = Profesor.objects.filter(id__in=profesorji)
        


    def clean(self):
        cleaned_data = super().clean()
        datum = cleaned_data['datum']
        slovenia_holidays = holidays.CountryHoliday('SI') 
        izpiti_predmeta_datumi = Izpit.objects.filter(predmet_id=self.pk).values_list('datum',flat=True)
        if datum.weekday() >= 5 or datum <= datetime.date.today() or datum in slovenia_holidays or datum in izpiti_predmeta_datumi:
            raise forms.ValidationError("Izpitni rok ne sme biti v preteklosti, v soboto, nedeljo ali na praznik. Prav tako se ne sme podvajati po datumu.")
        return cleaned_data

class IzpitForm1(forms.Form):
    pk = None
    izprasevalec = forms.ModelChoiceField(queryset=Nosilci.objects.none(),empty_label = None)
    datum = forms.DateField(label="Datum izpita",initial=datetime.date.today)
    cas = forms.TimeField(widget=forms.TimeInput(format='%H:%M'))
    prostor = forms.CharField(max_length=99)
    izredni = forms.BooleanField(label='Izredni izpit',required= False)
    def __init__(self, *args, **kwargs):
        if kwargs:
            self.pk = kwargs.pop('pk')
            self.izpit_pk = kwargs.pop('izpit_pk')
            print(self.izpit_pk)
        super(IzpitForm1, self).__init__(*args, **kwargs)
        nosilci = Nosilci.objects.filter(predmet_leto=self.pk)
        profesorji = [x.nosilec.id for x in nosilci]
        self.fields['izprasevalec'].queryset = Profesor.objects.filter(id__in=profesorji)
        


    def clean(self):
        cleaned_data = super().clean()
        datum = cleaned_data['datum']
        slovenia_holidays = holidays.CountryHoliday('SI') 
        izpiti_predmeta_datumi = Izpit.objects.filter(predmet_id=self.pk).values_list('datum',flat=True).exclude(id=self.izpit_pk)
        print(izpiti_predmeta_datumi)
        if datum.weekday() >= 5 or datum <= datetime.date.today() or datum in slovenia_holidays:
            raise forms.ValidationError("Izpitni rok ne sme biti v preteklosti, v soboto, nedeljo ali na praznik. Prav tako se ne sme podvajati po datumu.")
        return cleaned_data