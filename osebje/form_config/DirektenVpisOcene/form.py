from django import forms
import datetime

from Studis.models import Nosilci
from Studis.validators import *
from Studis.error_messages import *
from Studis.models.storitve import Vrsta_vpis, Nacin_studij, Smer, Oblika_studij, Izpit
import holidays

class DirektenVpisOceneForm(forms.Form):
    izprasevalec = forms.ModelChoiceField(queryset=Nosilci.objects.none(), empty_label=None)
    datum = forms.DateField(label="Datum izpita", initial=datetime.date.today)
    ocena = forms.IntegerField(label="Ocena")
    stPolaganja= forms.IntegerField(label="Zaporedna številka polaganja")
    stPolaganjaLetos =forms.IntegerField(label="Zaporedna številka polaganja v tem letu")

    def clean_datum(self):
        datum = self.cleaned_data.get('datum')

        slovenia_holidays = holidays.CountryHoliday('SI')
        #izpiti_predmeta_datumi = Izpit.objects.filter(predmet_id=self.pk).values_list('datum',flat=True)
        if datum.weekday() >= 5 or datum in slovenia_holidays:
            raise forms.ValidationError("Izpit ne sme biti v soboto, nedeljo ali na praznik.")
        if datum > datetime.date.today():
            raise forms.ValidationError("Izpitni rok ne sme biti v prihodnost.")
        return datum

    def clean_ocena(self):
        ocena = self.cleaned_data.get('ocena')
        if ocena < 1 or ocena > 10:
            raise forms.ValidationError("Ocena mora biti med 1 in 10")
        return ocena

    def __init__(self, *args, **kwargs):
        if kwargs:
            self.pk = kwargs.pop('pk')
        super(DirektenVpisOceneForm, self).__init__(*args, **kwargs)
        nosilci = Nosilci.objects.filter(predmet_leto=self.pk)
        profesorji = [x.nosilec.id for x in nosilci]
        self.fields['izprasevalec'].queryset = Profesor.objects.filter(id__in=profesorji)