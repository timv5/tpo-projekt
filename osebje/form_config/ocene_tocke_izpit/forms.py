from django import forms
import datetime

from django.forms import inlineformset_factory

from Studis.validators import *
from Studis.error_messages import *
from Studis.models.storitve import Evidenca_izpit, Izpit

class IzpitOceneTockeForm(forms.Form):
    CHOICES = (
        (1, ("Vrnil")),
        (2, ("Ni vrnil"))
    )
    mail = forms.CharField(label='mail', required=False)
    tocke = forms.IntegerField(label='tocke', max_value=100, min_value=0, required=False)
    ocena_pisni = forms.IntegerField(label='ocena_pisni', max_value=10, min_value=1, required=False)
    ocena = forms.IntegerField(label='ocena', max_value=10, min_value=1, required=False)

    vrni_prijavo = forms.ChoiceField(choices=CHOICES, initial=2)


IzpitOceneTockeFormSet = inlineformset_factory(Izpit, Evidenca_izpit, fields=('student', 'tocke', 'ocena_pisni', 'ocena', 'status',), extra=10)