import xlrd
from Studis.models import Drzava
def seed():
    file_location = "drzave.xlsx"
    workbook = xlrd.open_workbook(file_location)
    sheet=workbook.sheet_by_index(0)
    for drzava in Drzava.objects.all():
        drzava.delete()
    for col in range(sheet.nrows):
        if col > 3:
            #print(sheet.cell_value(col,4))
            dvomestna=sheet.cell_value(col,0)
            tromestna=sheet.cell_value(col,1)
            num=sheet.cell_value(col,2)
            if(isinstance(num, str)):
                num = num.split(".")[0]
            elif(isinstance(num,float)):
                num = round(num)
            ime=sheet.cell_value(col,4)
            drzava= Drzava(dvomestna_koda=dvomestna, tromestna_oznaka=tromestna,
                           num_oznaka=num,ime_drzave=ime).save()

    return drzava
