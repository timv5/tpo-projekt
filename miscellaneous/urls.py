from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path
from . import views as misc_views

urlpatterns = [

    path('seed1',misc_views.prvi_seed, name='seed1'),
    path('seed2', misc_views.drugi_seed, name='seed2'),
    path('seed3', misc_views.tretji_seed, name='seed3'),
    path('seed4', misc_views.cetrti_seed, name='seed4'),
    path('seed5', misc_views.peti_seed, name='seed5'),
    path('seed6', misc_views.sesti_seed, name='seed6'),
    path('seed7', misc_views.sedmi_seed, name='seed7'),

]

urlpatterns = format_suffix_patterns(urlpatterns)