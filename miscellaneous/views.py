from django.shortcuts import render
from faker import Faker
from Studis.models import *
from django.contrib.auth.models import Group
import random
from random import randint
import unicodedata
import datetime
from django.http import HttpResponseRedirect
from .seed_posta import seed as seed_posta
from .seed_drzava import seed as seed_drzava
from .seed_obcine import seed as seed_obcina
from Studis.poslovna_logika.poslovna_logika import studijskoLeto


# Create your views here.
def seed_db(request):
    if request.method == 'GET':
        pass
    return HttpResponseRedirect('/')


def seedDifferentStudents(n, posta, group, fake, obcina):
    studijsko_leto = Studijsko_leto.objects.get(studijsko_leto='2017/2018')
    evidenca_vpis_17 = Evidenca_Vpis.objects.filter(studijsko_leto=studijsko_leto)

    drzava = Drzava.objects.get(num_oznaka="705")
    predmet = Predmet_leto.objects.get(pk=134)
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59")
    izpit.save()
    vrste = Vrsta_vpis.objects.all()
    oblika = Oblika_studij.objects.all()
    nacin = Nacin_studij.objects.all()
    smer = Smer.objects.all()
    st_vpisov = 700
    for x in range(n):
        st_vpisov += 1
        emso = '1403995500141'
        vpisna = '63' + '14' + '{num:04d}'.format(num=st_vpisov)
        firstName = fake.first_name()
        lastName = fake.last_name()
        ulica = fake.street_address()
        phone = fake.phone_number().replace(" ", "")
        birth = fake.date(pattern="%Y-%m-%d")
        first_name_encoded = unicodedata.normalize('NFD', firstName.lower()).encode('ascii', 'ignore')
        last_name_encoded = unicodedata.normalize('NFD', lastName.lower()).encode('ascii', 'ignore')
        first_name_initial = first_name_encoded.decode()[0]
        last_name_initial = last_name_encoded.decode()[0]
        rnd_num = random.randrange(1000, 9999, 1)
        lastniemail = first_name_initial + last_name_initial + str(rnd_num) + '.doma@student.si'
        assigned_email = first_name_initial + last_name_initial + str(rnd_num) + '@student.uni-lj.si'
        new_student = Student(first_name=firstName, last_name=lastName, email=assigned_email, vpisna=vpisna,
                              osebni_email=lastniemail, drzava=drzava,
                              tel=phone, ulica_naslov=ulica, ulica_prejem=ulica, posta_naslov=posta,
                              posta_prejem=posta, drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                              kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina,
                              obcina_naslov=obcina,
                              datum_rojstva=birth)
        new_student.save()
        new_student.groups.add(group)
        new_student.set_password('password123')
        new_student.save()

        Coin(student=new_student, vrsta=vrste[0], oblika=oblika[0], nacin=nacin[0], smer=smer[0],
             letnik=3, pravica_izbire=True, used=True).save()

        Vpis(letnik=3, leto_vpisa=studijsko_leto, potrjen=True, student=new_student, smer=smer[0],
             tip=nacin[0], vrsta=vrste[0]).save()

        Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=3).save()
        SteviloOpravljanj(predmet=predmet.predmet, student=new_student, stVseh=0, stPop=0).save()
        Evidenca_izpit(student=new_student, izpit=izpit).save()


def create_referent(fake, group):
    for referent in Referent.objects.all():
        referent.delete()
    referent = Referent(sifra='99999007', datum_rojstva=fake.date(pattern="%Y-%m-%d"), emso='1111111111007',
                        drzavljanstvo='SLOVENIA', tel='999999007', first_name='referent', last_name='gazda',
                        email='referent@studis.si', posta=Posta.objects.all().first())
    referent.set_password('password123')
    referent.save()
    referent.groups.add(group)


def create_nacin_studij():
    for nacin in Nacin_studij.objects.all():
        nacin.delete()
    Nacin_studij(sifra="1", nacin='Redni').save()
    Nacin_studij(sifra="3", nacin='Izredni').save()


def create_oblika_studij():
    for oblika in Oblika_studij.objects.all():
        oblika.delete()
    Oblika_studij(sifra="1", oblika='Na lokaciji').save()
    Oblika_studij(sifra="2", oblika='Na daljavo').save()
    Oblika_studij(sifra="3", oblika='E-študij').save()


def create_studijsko_leto():
    for sl in Studijsko_leto.objects.all():
        sl.delete()
    Studijsko_leto(studijsko_leto='2014/2015').save()
    Studijsko_leto(studijsko_leto='2015/2016').save()
    Studijsko_leto(studijsko_leto='2016/2017').save()
    Studijsko_leto(studijsko_leto='2017/2018').save()
    Studijsko_leto(studijsko_leto='2018/2019').save()


def create_vrsta_vpis():
    for vrsta in Vrsta_vpis.objects.all():
        vrsta.delete()
    Vrsta_vpis(sifra="01", vrsta='Prvi vpis v letnik/dodatno leto').save()
    Vrsta_vpis(sifra="02", vrsta='Ponavljanje letnika').save()
    Vrsta_vpis(sifra="03", vrsta='Nadaljevanje letnika').save()
    Vrsta_vpis(sifra="04", vrsta='Podaljšanje statusa študenta').save()
    Vrsta_vpis(sifra="05", vrsta='Vpis po merilih za prehode v višji letnik').save()
    Vrsta_vpis(sifra="06", vrsta='Vpis v semester skupnega št.programa').save()
    Vrsta_vpis(sifra="07", vrsta='Vpis po merilih za prehode v isti letnik').save()
    Vrsta_vpis(sifra="98", vrsta='Vpis za zaključek').save()


def delete_subjects():
    for subject in Predmet.objects.all():
        subject.delete()

    for modul in Modul.objects.all():
        modul.delete()


def create_smer():
    for smer in Smer.objects.all():
        smer.delete()
    Smer(smer='UNI', ime='RAČUNAL. IN INFORM. UN-1.st', sifra="1000468").save()
    Smer(smer='VS', ime='RAČUNAL. IN INFORM. VS-1.st', sifra="1000470").save()


def create_evidenca_vpisa():
    for evpis in Evidenca_Vpis.objects.all():
        evpis.delete()
    Evidenca_Vpis(studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2014/2015')).save()
    Evidenca_Vpis(studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2015/2016')).save()
    Evidenca_Vpis(studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2016/2017')).save()
    Evidenca_Vpis(studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2017/2018')).save()
    Evidenca_Vpis(studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2018/2019')).save()


def create_student_group():
    students = Group(name='student')
    students.save()
    return students


def create_professors_group():
    professors = Group(name='profesor')
    professors.save()
    return professors


def create_referent_group():
    referent = Group(name='referent')
    referent.save()
    return referent


def create_super_user():
    for user in User.objects.all():
        user.delete()
    User.objects.create_superuser(password='password123', email='admin@studis.si')


def seedProfessors(n, posta, group, fake):
    for professor in Profesor.objects.all():
        professor.delete()

    for x in range(n):
        sifra = random.randrange(10000000, 99999999)
        birth = fake.date(pattern="%Y-%m-%d")
        phone = fake.phone_number().replace(" ", "")
        firstName = fake.first_name()
        lastName = fake.last_name()
        ulica = fake.street_address()
        emso = random.randrange(1000000000000, 9999999999999)
        first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
        last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
        assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
        new_professor = Profesor(sifra=sifra, first_name=firstName,
                                 last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone,
                                 drzavljanstvo="SLOVENIA",
                                 emso=emso, email=assigned_email, ulica=ulica)
        new_professor.save()
        new_professor.groups.add(group)
        new_professor.set_password('password123')
        new_professor.save()


def seedVpis():
    student = Student.objects.filter(email__contains='iz6968').first()
    student2 = Student.objects.filter(email__contains='kt0000').first()
    student3 = Student.objects.filter(email__contains='ei0000').first()
    student5 = Student.objects.filter(email__contains='ei0002').first()
    student4 = Student.objects.filter(email__contains='ei0001').first()
    smer = Smer.objects.first()
    nacin = Nacin_studij.objects.first()
    leto = Studijsko_leto.objects.all()
    vrsta = Vrsta_vpis.objects.all()

    Vpis(letnik=1, student=student, leto_vpisa=leto[0],
         smer=smer, tip=nacin, vrsta=vrsta.first()).save()
    Vpis(letnik=2, student=student, leto_vpisa=leto[1],
         smer=smer, tip=nacin, vrsta=vrsta[0]).save()
    Vpis(letnik=3, student=student, leto_vpisa=leto[2],
         smer=smer, tip=nacin, vrsta=vrsta[0]).save()

    smer = Smer.objects.all()
    smer = smer[1]
    Vpis(letnik=1, student=student2, leto_vpisa=leto[2],
         smer=smer, tip=nacin, vrsta=vrsta.first()).save()
    Vpis(letnik=1, student=student2, leto_vpisa=leto[3],
         smer=smer, tip=nacin, vrsta=vrsta[1]).save()

    smer = Smer.objects.all()
    smer = smer[0]
    Vpis(letnik=1, student=student3, leto_vpisa=leto[2],
         smer=smer, tip=nacin, vrsta=vrsta.first()).save()

    Vpis(letnik=1, student=student4, leto_vpisa=leto[0],
         smer=smer, tip=nacin, vrsta=vrsta.first()).save()
    Vpis(letnik=1, student=student4, leto_vpisa=leto[1],
         smer=smer, tip=nacin, vrsta=vrsta.first()).save()
    Vpis(letnik=1, student=student4, leto_vpisa=leto[2],
         smer=smer, tip=nacin, vrsta=vrsta.first()).save()

    Vpis(letnik=1, student=student5, leto_vpisa=leto[2],
         smer=smer, tip=nacin, vrsta=vrsta.first()).save()


def seedSimulirajPredmetnik():
    student = Student.objects.filter(email__contains='iz6968').first()
    smer = Smer.objects.first()
    leto = Studijsko_leto.objects.all()

    predmeti_prvi_letnik = Splosni_Predmet.objects.filter(letnik=1, smer=smer).values_list('id', flat=True)
    predmet_leto_predmet_id = Predmet_leto.objects.filter(studijsko_leto=leto[0]).values_list('predmet_id', flat=True)
    predmet_leto_id = Predmet_leto.objects.filter(studijsko_leto=leto[0]).values_list('id', flat=True)
    zippy = list(zip(predmet_leto_id, predmet_leto_predmet_id))
    p = [x[0] for x in zippy if x[1] in predmeti_prvi_letnik]

    for predmet in Predmet_leto.objects.filter(id__in=p):
        Evidenca_student(student=student, predmet=predmet, ocena=randint(5, 10), letnik_predmeta=1).save()
        izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59")
        izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59")
        izpit.save()
        izpit2.save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
        Evidenca_izpit(student=student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
        SteviloOpravljanj(student=student, predmet=predmet.predmet, stVseh=2, stPop=0).save()

    predmeti_drugi_letnik = Splosni_Predmet.objects.filter(letnik=2, smer=smer).values_list('id', flat=True)
    izbirni1 = Strokovni_izbirni.objects.filter(sifra="63220").values_list('id', flat=True)
    izbirni2 = Strokovni_izbirni.objects.filter(sifra="63219").values_list('id', flat=True)
    predmeti_drugi_letnik = list(predmeti_drugi_letnik) + list(izbirni1) + list(izbirni2)
    predmet_leto_predmet_id = Predmet_leto.objects.filter(studijsko_leto=leto[1]).values_list('predmet_id', flat=True)
    predmet_leto_id = Predmet_leto.objects.filter(studijsko_leto=leto[1]).values_list('id', flat=True)
    zippy = list(zip(predmet_leto_id, predmet_leto_predmet_id))
    p = [x[0] for x in zippy if x[1] in predmeti_drugi_letnik]
    for predmet in Predmet_leto.objects.filter(id__in=p):
        Evidenca_student(student=student, predmet=predmet, ocena=randint(5, 10), letnik_predmeta=2).save()
        izpit = Izpit(predmet=predmet, datum='2015-05-11', izredni=False, prostor='P10', cas="08:59")
        izpit.save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=randint(6, 10), tocke=randint(50, 100)).save()
        SteviloOpravljanj(student=student, predmet=predmet.predmet, stVseh=1, stPop=0).save()

    predmeti_tretji_letnik = Splosni_Predmet.objects.filter(letnik=3, smer=smer).values_list('id', flat=True)
    prvi_modul = Modulski_Predmet.objects.filter(smer=smer, modul__ime_modula="Razvoj programske opreme").values_list(
        'id', flat=True)
    drugi_modul = Modulski_Predmet.objects.filter(smer=smer, modul__ime_modula="Informacijski sistemi").values_list(
        'id', flat=True)
    izbirni = Modulski_Predmet.objects.filter(naslov_predmeta="Prevajalniki", smer=smer).values_list('id', flat=True)
    predmeti_tretji_letnik = list(predmeti_tretji_letnik) + list(prvi_modul) + list(drugi_modul) + list(izbirni)
    predmet_leto_predmet_id = Predmet_leto.objects.filter(studijsko_leto=leto[2]).values_list('predmet_id', flat=True)
    predmet_leto_id = Predmet_leto.objects.filter(studijsko_leto=leto[2]).values_list('id', flat=True)
    zippy = list(zip(predmet_leto_id, predmet_leto_predmet_id))
    p = [x[0] for x in zippy if x[1] in predmeti_tretji_letnik]
    for predmet in Predmet_leto.objects.filter(id__in=p):
        Evidenca_student(student=student, predmet=predmet, ocena=randint(6, 10), letnik_predmeta=3).save()
        izpit = Izpit(predmet=predmet, datum='2016-05-11', izredni=False, prostor='P10', cas="08:59")
        izpit.save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=randint(6, 10), tocke=randint(50, 100)).save()
        SteviloOpravljanj(student=student, predmet=predmet.predmet, stVseh=1, stPop=0).save()

    # SIMULIRAJ ZA KT0000
    student = Student.objects.filter(email__contains='kt0000').first()
    smer = Smer.objects.all()
    smer = smer[1]
    leto = Studijsko_leto.objects.all()

    predmeti_vsi = Splosni_Predmet.objects.filter(letnik=1, smer=smer).values_list('id', flat=True)

    predmeti_prvi_letnik = predmeti_vsi[:5]
    predmet_leto_predmet_id = Predmet_leto.objects.filter(studijsko_leto=leto[2]).values_list('predmet_id', flat=True)
    predmet_leto_id = Predmet_leto.objects.filter(studijsko_leto=leto[2]).values_list('id', flat=True)
    zippy = list(zip(predmet_leto_id, predmet_leto_predmet_id))
    p = [x[0] for x in zippy if x[1] in predmeti_prvi_letnik]
    date1 = datetime.date(2017, 6, 16)
    date2 = datetime.date(2017, 6, 29)
    date3 = datetime.date(2017, 9, 10)
    for predmet in Predmet_leto.objects.filter(id__in=p):
        Evidenca_student(student=student, predmet=predmet, ocena=5, letnik_predmeta=1).save()
        izpit = Izpit(predmet=predmet, datum=date1, izredni=False, prostor='P1', cas="09:59")
        izpit2 = Izpit(predmet=predmet, datum=date2, izredni=False, prostor='P1', cas="10:00")
        izpit3 = Izpit(predmet=predmet, datum=date3, izredni=False, prostor='P10', cas="08:59")
        date1 = date1 + datetime.timedelta(days=-1)
        date2 = date2 + datetime.timedelta(days=-1)
        date3 = date3 + datetime.timedelta(days=-1)
        izpit.save()
        izpit2.save()
        izpit3.save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
        Evidenca_izpit(student=student, izpit=izpit2, ocena=5, tocke=randint(0, 49)).save()
        Evidenca_izpit(student=student, izpit=izpit3, ocena=5, tocke=randint(0, 49)).save()
        SteviloOpravljanj(student=student, predmet=predmet.predmet, stVseh=4, stPop=3).save()

    date1 = datetime.date(2017, 6, 25)
    predmeti_drugi_letnik = predmeti_vsi[5:]
    p = [x[0] for x in zippy if x[1] in predmeti_drugi_letnik]
    for predmet in Predmet_leto.objects.filter(id__in=p):
        Evidenca_student(student=student, predmet=predmet, ocena=randint(6, 10), letnik_predmeta=1).save()
        izpit = Izpit(predmet=predmet, datum=date1, izredni=False, prostor='P1', cas="09:59")
        date1 = date1 + datetime.timedelta(days=-1)
        izpit.save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=randint(6, 10), tocke=randint(50, 100)).save()
        SteviloOpravljanj(student=student, predmet=predmet.predmet, stVseh=1, stPop=0).save()

    predmet_leto_predmet_id = Predmet_leto.objects.filter(studijsko_leto=leto[3]).values_list('predmet_id', flat=True)
    predmet_leto_id = Predmet_leto.objects.filter(studijsko_leto=leto[3]).values_list('id', flat=True)
    zippy = list(zip(predmet_leto_id, predmet_leto_predmet_id))
    p = [x[0] for x in zippy if x[1] in predmeti_prvi_letnik]
    date1 = datetime.date(2018, 6, 25)
    for predmet in Predmet_leto.objects.filter(id__in=p):
        Evidenca_student(student=student, predmet=predmet, ocena=randint(6, 10), letnik_predmeta=1).save()
        izpit = Izpit(predmet=predmet, datum=date1, izredni=False, prostor='P10', cas="08:59")
        num = random.randint(-5, 5)
        date1 = date1 + datetime.timedelta(days=num)
        izpit.save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=randint(6, 10), tocke=randint(50, 100)).save()

    # SIMULIRAJ ZA ČINDEKS
    student = Student.objects.filter(email__contains='ei0000').first()
    smer = Smer.objects.all()
    smer = smer[0]
    leto = Studijsko_leto.objects.all()

    predmeti_vsi = Splosni_Predmet.objects.filter(letnik=1, smer=smer).values_list('id', flat=True)

    predmeti_prvi_letnik = predmeti_vsi
    predmet_leto_predmet_id = Predmet_leto.objects.filter(studijsko_leto=leto[2]).values_list('predmet_id', flat=True)
    predmet_leto_id = Predmet_leto.objects.filter(studijsko_leto=leto[2]).values_list('id', flat=True)
    zippy = list(zip(predmet_leto_id, predmet_leto_predmet_id))
    p = [x[0] for x in zippy if x[1] in predmeti_prvi_letnik]
    date1 = datetime.date(2017, 7, 20)
    date2 = datetime.date(2017, 7, 29)
    date3 = datetime.date(2017, 9, 15)
    for predmet in Predmet_leto.objects.filter(id__in=p):
        Evidenca_student(student=student, predmet=predmet, ocena=randint(6, 10), letnik_predmeta=1).save()
        izpit = Izpit(predmet=predmet, datum=date1, izredni=False, prostor='P1', cas="09:59")
        izpit.save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=randint(6, 10), tocke=randint(0, 49)).save()
        SteviloOpravljanj(student=student, predmet=predmet.predmet, stVseh=1, stPop=0).save()

    # SIMULIRAJ ZA CINDEKS
    student = Student.objects.filter(email__contains='ei0001').first()
    smer = Smer.objects.all()
    smer = smer[0]
    leto = Studijsko_leto.objects.all()

    predmeti_vsi = Splosni_Predmet.objects.filter(letnik=1, smer=smer).values_list('id', flat=True)

    predmeti_prvi_letnik = predmeti_vsi[:5]
    predmet_leto_predmet_id = Predmet_leto.objects.filter(studijsko_leto=leto[0]).values_list('predmet_id', flat=True)
    predmet_leto_id = Predmet_leto.objects.filter(studijsko_leto=leto[0]).values_list('id', flat=True)
    zippy = list(zip(predmet_leto_id, predmet_leto_predmet_id))
    p = [x[0] for x in zippy if x[1] in predmeti_prvi_letnik]
    date1 = datetime.date(2015, 6, 16)
    date2 = datetime.date(2015, 6, 29)
    date3 = datetime.date(2015, 9, 10)
    for predmet in Predmet_leto.objects.filter(id__in=p):
        Evidenca_student(student=student, predmet=predmet, ocena=5, letnik_predmeta=1).save()
        izpit = Izpit(predmet=predmet, datum=date1, izredni=False, prostor='P1', cas="09:59")
        izpit2 = Izpit(predmet=predmet, datum=date2, izredni=False, prostor='P1', cas="10:00")
        izpit3 = Izpit(predmet=predmet, datum=date3, izredni=False, prostor='P10', cas="08:59")
        date1 = date1 + datetime.timedelta(days=-1)
        date2 = date2 + datetime.timedelta(days=-1)
        date3 = date3 + datetime.timedelta(days=-1)
        izpit.save()
        izpit2.save()
        izpit3.save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
        Evidenca_izpit(student=student, izpit=izpit2, ocena=5, tocke=randint(0, 49)).save()
        Evidenca_izpit(student=student, izpit=izpit3, ocena=5, tocke=randint(0, 49)).save()
        SteviloOpravljanj(student=student, predmet=predmet.predmet, stVseh=4, stPop=3).save()

    date1 = datetime.date(2015, 6, 25)
    predmeti_drugi_letnik4 = predmeti_vsi[5:]
    p = [x[0] for x in zippy if x[1] in predmeti_drugi_letnik]
    for predmet in Predmet_leto.objects.filter(id__in=p):
        Evidenca_student(student=student, predmet=predmet, ocena=randint(6, 10), letnik_predmeta=1).save()
        izpit = Izpit(predmet=predmet, datum=date1, izredni=False, prostor='P1', cas="09:59")
        date1 = date1 + datetime.timedelta(days=-1)
        izpit.save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=randint(6, 10), tocke=randint(50, 100)).save()
        SteviloOpravljanj(student=student, predmet=predmet.predmet, stVseh=1, stPop=0).save()

    predmet_leto_predmet_id = Predmet_leto.objects.filter(studijsko_leto=leto[1]).values_list('predmet_id', flat=True)
    predmet_leto_id = Predmet_leto.objects.filter(studijsko_leto=leto[1]).values_list('id', flat=True)
    zippy = list(zip(predmet_leto_id, predmet_leto_predmet_id))
    p = [x[0] for x in zippy if x[1] in predmeti_prvi_letnik]
    date1 = datetime.date(2016, 6, 25)
    for predmet in Predmet_leto.objects.filter(id__in=p):
        Evidenca_student(student=student, predmet=predmet, ocena=5, letnik_predmeta=1).save()
        izpit = Izpit(predmet=predmet, datum=date1, izredni=False, prostor='P10', cas="08:59")
        num = random.randint(-5, 5)
        date1 = date1 + datetime.timedelta(days=num)
        izpit.save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()

    predmet_leto_predmet_id = Predmet_leto.objects.filter(studijsko_leto=leto[2]).values_list('predmet_id', flat=True)
    predmet_leto_id = Predmet_leto.objects.filter(studijsko_leto=leto[2]).values_list('id', flat=True)
    zippy = list(zip(predmet_leto_id, predmet_leto_predmet_id))
    p = [x[0] for x in zippy if x[1] in predmeti_prvi_letnik]
    date1 = datetime.date(2017, 6, 25)
    for predmet in Predmet_leto.objects.filter(id__in=p):
        Evidenca_student(student=student, predmet=predmet, ocena=randint(6, 10), letnik_predmeta=1).save()
        izpit = Izpit(predmet=predmet, datum=date1, izredni=False, prostor='P10', cas="08:59")
        num = random.randint(-5, 5)
        date1 = date1 + datetime.timedelta(days=num)
        izpit.save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=randint(6, 10), tocke=randint(50, 100)).save()

    # SIMULIRAJ ZA DINDEKS
    student = Student.objects.filter(email__contains='ei0002').first()
    smer = Smer.objects.all()
    smer = smer[0]
    leto = Studijsko_leto.objects.all()

    predmeti_vsi = Splosni_Predmet.objects.filter(letnik=1, smer=smer).values_list('id', flat=True)

    predmeti_prvi_letnik = predmeti_vsi
    predmet_leto_predmet_id = Predmet_leto.objects.filter(studijsko_leto=leto[2]).values_list('predmet_id', flat=True)
    predmet_leto_id = Predmet_leto.objects.filter(studijsko_leto=leto[2]).values_list('id', flat=True)
    zippy = list(zip(predmet_leto_id, predmet_leto_predmet_id))
    p = [x[0] for x in zippy if x[1] in predmeti_prvi_letnik]
    date1 = datetime.date(2017, 6, 16)
    date2 = datetime.date(2017, 6, 29)
    date3 = datetime.date(2017, 9, 10)
    for predmet in Predmet_leto.objects.filter(id__in=p):
        Evidenca_student(student=student, predmet=predmet, ocena=5, letnik_predmeta=1).save()
        izpit = Izpit(predmet=predmet, datum=date1, izredni=False, prostor='P1', cas="09:59")
        izpit2 = Izpit(predmet=predmet, datum=date2, izredni=False, prostor='P1', cas="10:00")
        izpit3 = Izpit(predmet=predmet, datum=date3, izredni=False, prostor='P10', cas="08:59")
        date1 = date1 + datetime.timedelta(days=-1)
        date2 = date2 + datetime.timedelta(days=-1)
        date3 = date3 + datetime.timedelta(days=-1)
        izpit.save()
        izpit2.save()
        izpit3.save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
        Evidenca_izpit(student=student, izpit=izpit2, ocena=5, tocke=randint(0, 49)).save()
        Evidenca_izpit(student=student, izpit=izpit3, ocena=5, tocke=randint(0, 49)).save()
        SteviloOpravljanj(student=student, predmet=predmet.predmet, stVseh=3, stPop=0).save()


def seedStudents(n, posta, drzava, group, fake, obcina):
    for student in Student.objects.all():
        student.delete()

    drzava = Drzava.objects.get(num_oznaka="705")
    evidenca_vpis_14 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2014/2015')).first()
    evidenca_vpis_15 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2015/2016')).first()
    evidenca_vpis_16 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2016/2017')).first()
    evidenca_vpis_17 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2017/2018')).first()
    jst = Student(first_name='Igor', last_name='Žibert', email='iz6968@student.uni-lj.si',
                  vpisna='63140300', emso='1107055000187', tel='051335754',
                  datum_rojstva='1995-07-11', posta_naslov=posta, posta_prejem=posta, drzava=drzava,
                  ulica_naslov="Ulica bratov Rozman 4", ulica_prejem="Ulica bratov Rozman 4",
                  drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                  kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina, obcina_naslov=obcina)
    jst.save()
    jst.groups.add(group)
    jst.set_password('password123')
    jst.save()

    testindeks = Student(first_name='Test', last_name='Kartotetnilist', email='kt0000@student.uni-lj.si',
                         vpisna='63160999', emso='1107055000187', tel='051335754',
                         datum_rojstva='1995-07-11', posta_naslov=posta, posta_prejem=posta, drzava=drzava,
                         ulica_naslov="Ulica bratov Rozman 6", ulica_prejem="Ulica bratov Rozman 6",
                         drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                         kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina, obcina_naslov=obcina,
                         osebni_email='kartotetdnilist@studis.si')
    testindeks.save()
    testindeks.groups.add(group)
    testindeks.set_password('password123')
    testindeks.save()

    testIndeks1 = Student(first_name='Test', last_name='Čindeks', email='ei0000@student.uni-lj.si',
                          vpisna='63160998', emso='1107055000181', tel='051335754',
                          datum_rojstva='1995-07-11', posta_naslov=posta, posta_prejem=posta, drzava=drzava,
                          ulica_naslov="Ulica bratov Rozman 6", ulica_prejem="Ulica bratov Rozman 6",
                          drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                          kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina, obcina_naslov=obcina,
                          osebni_email='ellist1@studis.si')
    testIndeks1.save()
    testIndeks1.groups.add(group)
    testIndeks1.set_password('password123')
    testIndeks1.save()

    testIndeks1 = Student(first_name='Test', last_name='Cindeks', email='ei0001@student.uni-lj.si',
                          vpisna='63160997', emso='1107055000180', tel='051335754',
                          datum_rojstva='1995-07-11', posta_naslov=posta, posta_prejem=posta, drzava=drzava,
                          ulica_naslov="Ulica bratov Rozman 6", ulica_prejem="Ulica bratov Rozman 6",
                          drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                          kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina, obcina_naslov=obcina,
                          osebni_email='ellist2@studis.si')
    testIndeks1.save()
    testIndeks1.groups.add(group)
    testIndeks1.set_password('password123')
    testIndeks1.save()

    testIndeks1 = Student(first_name='Test', last_name='Dindeks', email='ei0002@student.uni-lj.si',
                          vpisna='63160996', emso='1107055000184', tel='051335754',
                          datum_rojstva='1995-07-11', posta_naslov=posta, posta_prejem=posta, drzava=drzava,
                          ulica_naslov="Ulica bratov Rozman 6", ulica_prejem="Ulica bratov Rozman 6",
                          drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                          kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina, obcina_naslov=obcina,
                          osebni_email='ellist3@studis.si')
    testIndeks1.save()
    testIndeks1.groups.add(group)
    testIndeks1.set_password('password123')
    testIndeks1.save()

    for x in range(n):
        if (x <= 5):
            now = '14'
            st_vpisov = evidenca_vpis_14.st_vpisov
            evidenca_vpis_14.st_vpisov += 1
            evidenca_vpis_14.save()
        elif (x > 5 and x <= 10):
            now = '15'
            st_vpisov = evidenca_vpis_15.st_vpisov
            evidenca_vpis_15.st_vpisov += 1
            evidenca_vpis_15.save()
        elif (x > 10 and x <= 15):
            now = '16'
            st_vpisov = evidenca_vpis_16.st_vpisov
            evidenca_vpis_16.st_vpisov += 1
            evidenca_vpis_16.save()
        if (x > 15):
            now = '17'
            st_vpisov = evidenca_vpis_17.st_vpisov
            evidenca_vpis_17.st_vpisov += 1
            evidenca_vpis_17.save()
        emso = '1403995500141'
        vpisna = '63' + now + '{num:04d}'.format(num=st_vpisov)
        firstName = fake.first_name()
        lastName = fake.last_name()
        ulica = fake.street_address()
        phone = fake.phone_number().replace(" ", "")
        birth = fake.date(pattern="%Y-%m-%d")
        first_name_encoded = unicodedata.normalize('NFD', firstName.lower()).encode('ascii', 'ignore')
        last_name_encoded = unicodedata.normalize('NFD', lastName.lower()).encode('ascii', 'ignore')
        first_name_initial = first_name_encoded.decode()[0]
        last_name_initial = last_name_encoded.decode()[0]
        rnd_num = random.randrange(1000, 9999, 1)
        lastniemail = first_name_initial + last_name_initial + str(rnd_num) + '.doma@student.si'
        assigned_email = first_name_initial + last_name_initial + str(rnd_num) + '@student.uni-lj.si'
        new_student = Student(first_name=firstName, last_name=lastName, email=assigned_email, vpisna=vpisna,
                              osebni_email=lastniemail, drzava=drzava,
                              tel=phone, ulica_naslov=ulica, ulica_prejem=ulica, posta_naslov=posta,
                              posta_prejem=posta, drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                              kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina,
                              obcina_naslov=obcina,
                              datum_rojstva=birth)
        new_student.save()
        new_student.groups.add(group)
        new_student.set_password('password123')
        new_student.save()


def seed_prvi_letnik_profesor_uni(fake):
    # Pridobi prvo posto, vseeno kero
    posta = Posta.objects.first()

    # Pridobi profesor group
    group = Group.objects.filter(name='profesor').first()

    # Viljan Mahnič
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Viljan'
    lastName = 'Mahnič'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Nežka Mramor Kosta
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Nežka'
    lastName = 'Kosta'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Gašper Fijavž
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Gašper'
    lastName = 'Fijavž'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Nikolaj Zimic
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Nikolaj'
    lastName = 'Zimic'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Borut Paul Kerševan
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Borut'
    lastName = 'Kerševan'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Boštjan Slivnik
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Boštjan'
    lastName = 'Slivnik'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Bojan Orel
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Bojan'
    lastName = 'Orel'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Branko Šter
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Branko'
    lastName = 'Šter'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Zoran Bosnić
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Zoran'
    lastName = 'Bosnić'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Dejan Lavbič
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Dejan'
    lastName = 'Lavbič'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()


def seed_prvi_letnik_predmet_uni(smer):
    studijska_leta = Studijsko_leto.objects.all()

    profesor = Profesor.objects.filter(last_name='Mahnič').first()
    predmet = Splosni_Predmet(sifra='63277', naslov_predmeta='Programiranje 1', kreditne_tocke=6, smer=smer,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, studijsko_leto=studijsko_leto, profesor=profesor).save()

    profesor = Profesor.objects.filter(last_name='Kosta').first()
    predmet = Splosni_Predmet(sifra='63202', naslov_predmeta='Osnove matematicne analize', kreditne_tocke=6, smer=smer,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, studijsko_leto=studijsko_leto, profesor=profesor).save()

    profesor = Profesor.objects.filter(last_name='Fijavž').first()
    predmet = Splosni_Predmet(sifra='63203', naslov_predmeta='Diskretne strukture', kreditne_tocke=6, smer=smer,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, studijsko_leto=studijsko_leto, profesor=profesor).save()

    profesor = Profesor.objects.filter(last_name='Zimic').first()
    predmet = Splosni_Predmet(sifra='63204', naslov_predmeta='Osnove digitalnih vezij', kreditne_tocke=6, smer=smer,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, studijsko_leto=studijsko_leto, profesor=profesor).save()
    profesor = Profesor.objects.filter(last_name='Kerševan').first()
    predmet = Splosni_Predmet(sifra='63205', naslov_predmeta='Fizika', kreditne_tocke=6, smer=smer,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, studijsko_leto=studijsko_leto, profesor=profesor).save()
    profesor = Profesor.objects.filter(last_name='Slivnik').first()
    predmet = Splosni_Predmet(sifra='63278', naslov_predmeta='Programiranje 2', kreditne_tocke=6, smer=smer,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, studijsko_leto=studijsko_leto, profesor=profesor).save()
    profesor = Profesor.objects.filter(last_name='Orel').first()
    predmet = Splosni_Predmet(sifra='63207', naslov_predmeta='Linearna algebra', kreditne_tocke=6, smer=smer,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, studijsko_leto=studijsko_leto, profesor=profesor).save()
    profesor = Profesor.objects.filter(last_name='Šter').first()
    predmet = Splosni_Predmet(sifra='63212', naslov_predmeta='Arhitekture računalniških sistemov', kreditne_tocke=6,
                              smer=smer,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, studijsko_leto=studijsko_leto, profesor=profesor).save()
    profesor = Profesor.objects.filter(last_name='Bosnić').first()
    predmet = Splosni_Predmet(sifra='63209', naslov_predmeta='Računalniške komunikacije', kreditne_tocke=6, smer=smer,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, studijsko_leto=studijsko_leto, profesor=profesor).save()
    profesor = Profesor.objects.filter(last_name='Lavbič').first()
    predmet = Splosni_Predmet(sifra='63215', naslov_predmeta='Osnove informacijskih sistemov', kreditne_tocke=6,
                              smer=smer,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, studijsko_leto=studijsko_leto, profesor=profesor).save()


def seed_drugi_letnik_profesor_uni(fake):
    # Pridobi prvo posto, vseeno kero
    posta = Posta.objects.first()

    # Pridobi profesor group
    group = Group.objects.filter(name='profesor').first()

    # Rok Žitko
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Rok'
    lastName = 'Žitko'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Andrej Bauer
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Andrej'
    lastName = 'Bauer'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Igor Kononenko
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Igor'
    lastName = 'Kononenko'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Marko Bajec
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Marko'
    lastName = 'Bajec'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Aleksandar Jurišić
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Aleksandar'
    lastName = 'Jurišić'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Borut Robič
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Borut'
    lastName = 'Robič'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Uroš Lotrič
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Uroš'
    lastName = 'Lotrič'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Patricio Bulić
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Patricio'
    lastName = 'Bulić'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Nina Bostič Bishop
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Nina'
    lastName = 'Bostič Bishop'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Gostujoči profesorji z drugih univerz
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Gostujoči profesorji'
    lastName = 'z drugih univerz'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()


def seed_drugi_letnik_predmet_uni(smer):
    studijska_leta = Studijsko_leto.objects.all()

    profesor = Profesor.objects.filter(last_name='z drugih univerz').first()
    predmet = Splosno_izbirni(sifra='63225', naslov_predmeta='Izbrana poglavja iz računalništva in informatike',
                              smer=smer, kreditne_tocke=6)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Bostič Bishop').first()
    predmet = Splosno_izbirni(sifra='63222', naslov_predmeta='Angleški jezik – nivo A', smer=smer,
                              kreditne_tocke=3)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Bostič Bishop').first()
    predmet = Splosno_izbirni(sifra='63223', naslov_predmeta='Angleški jezik – nivo B', smer=smer,
                              kreditne_tocke=3)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Bostič Bishop').first()
    predmet = Splosno_izbirni(sifra='63224', naslov_predmeta='Angleški jezik – nivo C', smer=smer,
                              kreditne_tocke=3)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Kosta').first()
    predmet = Strokovni_izbirni(sifra='63219', naslov_predmeta='Matematično modeliranje', smer=smer,
                                kreditne_tocke=6)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
    profesor = Profesor.objects.filter(last_name='Žitko').first()
    predmet = Strokovni_izbirni(sifra='63221', naslov_predmeta='Računalniške tehnologije', smer=smer,
                                kreditne_tocke=6)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
    profesor = Profesor.objects.filter(last_name='Bauer').first()
    predmet = Strokovni_izbirni(sifra='63220', naslov_predmeta='Principi programskih jezikov', smer=smer,
                                kreditne_tocke=6)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
    profesor = Profesor.objects.filter(last_name='Kononenko').first()
    predmet = Splosni_Predmet(sifra='63279', naslov_predmeta='Algoritmi in podatkovne strukture 1', smer=smer,
                              kreditne_tocke=6,
                              letnik=2)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Bajec').first()
    predmet = Splosni_Predmet(sifra='63208', naslov_predmeta='Osnove podatkovnih baz', smer=smer,
                              kreditne_tocke=6,
                              letnik=2)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Jurišić').first()
    predmet = Splosni_Predmet(sifra='63213', naslov_predmeta='Verjetnost in statistika', smer=smer,
                              kreditne_tocke=6,
                              letnik=2)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Robič').first()
    predmet = Splosni_Predmet(sifra='63283', naslov_predmeta='Izračunljivost in računska zahtevnost', smer=smer,
                              kreditne_tocke=6,
                              letnik=2)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Robič').first()
    predmet = Splosni_Predmet(sifra='63280', naslov_predmeta='Algoritmi in podatkovne strukture 2', smer=smer,
                              kreditne_tocke=6,
                              letnik=2)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Lotrič').first()
    predmet = Splosni_Predmet(sifra='63216', naslov_predmeta='Teorija informacij in sistemov', smer=smer,
                              kreditne_tocke=6,
                              letnik=2)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Bulić').first()
    predmet = Splosni_Predmet(sifra='63218', naslov_predmeta='Organizacija računalniških sistemov', smer=smer,
                              kreditne_tocke=6,
                              letnik=2)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Robič').first()
    predmet = Splosni_Predmet(sifra='63217', naslov_predmeta='Operacijski sistemi', smer=smer,
                              kreditne_tocke=6,
                              letnik=2)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()


def seed_tretji_letnik_profesor_uni(fake):
    # Pridobi prvo posto, vseeno kero
    posta = Posta.objects.first()

    # Pridobi profesor group
    group = Group.objects.filter(name='profesor').first()

    # Denis Trček
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Denis'
    lastName = 'Trček'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Tomaž Hovelja
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Tomaž'
    lastName = 'Hovelja'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Blaž Zupan
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Blaž'
    lastName = 'Zupan'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Matjaž Kukar
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Matjaž'
    lastName = 'Kukar'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Rok Rupnik
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Rok'
    lastName = 'Rupnik'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Branko Matjaž Jurič
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Branko'
    lastName = 'Jurič'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Mojca Ciglarič
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Mojca'
    lastName = 'Ciglarič'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Tomaž Dobravec
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Tomaž'
    lastName = 'Dobravec'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Marko Robnik Šikonja
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Marko'
    lastName = 'Šikonja'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Danijel Skočaj
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Danijel'
    lastName = 'Skočaj'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Matej Kristan
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Matej'
    lastName = 'Kristan'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Narvika Bovcon
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Narvika'
    lastName = 'Bovcon'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Matija Marolt
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Matija'
    lastName = 'Marolt'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Drnovšek Mateja
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Drnovšek'
    lastName = 'Mateja'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Franc Solina
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Franc'
    lastName = 'Solina'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Miha Mraz
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Miha'
    lastName = 'Mraz'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()


def seed_tretji_letnik_predmet_uni(smer):
    studijska_leta = Studijsko_leto.objects.all()

    # Modul Informacijski sistemi
    modul_IS = Modul(sifra='00000001', ime_modula='Informacijski sistemi')
    modul_IS.save()
    profesor = Profesor.objects.filter(last_name='Trček').first()
    predmet = Modulski_Predmet(sifra='63249', naslov_predmeta='Elektronsko poslovanje', smer=smer,
                               kreditne_tocke=6, modul=modul_IS)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
    profesor = Profesor.objects.filter(last_name='Hovelja').first()
    predmet = Modulski_Predmet(sifra='63250', naslov_predmeta='Organizacija in management', smer=smer,
                               kreditne_tocke=6, modul=modul_IS)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
    profesor = Profesor.objects.filter(last_name='Zupan').first()
    predmet = Modulski_Predmet(sifra='63251', naslov_predmeta='Uvod v odkrivanje znanj iz podatkov', smer=smer,
                               kreditne_tocke=6, modul=modul_IS)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
        # Modul Obvladovanje informatike

    modul_OI = Modul(sifra='00000002', ime_modula='Obvladovanje informatike')
    modul_OI.save()

    profesor = Profesor.objects.filter(last_name='Kukar').first()
    predmet = Modulski_Predmet(sifra='63226', naslov_predmeta='Tehnologija upravljanja podatkov', smer=smer,
                               kreditne_tocke=6, modul=modul_OI)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
    profesor = Profesor.objects.filter(last_name='Bajec').first()
    predmet = Modulski_Predmet(sifra='63252', naslov_predmeta='Razvoj informacijskih sistemov', smer=smer,
                               kreditne_tocke=6, modul=modul_OI)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
    profesor = Profesor.objects.filter(last_name='Rupnik').first()
    predmet = Modulski_Predmet(sifra='63253', naslov_predmeta='Planiranje in upravljanje informatike', smer=smer,
                               kreditne_tocke=6, modul=modul_OI)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
        # Modul Razvoj programske opreme

    modul_RPO = Modul(sifra='00000003', ime_modula='Razvoj programske opreme')
    modul_RPO.save()

    profesor = Profesor.objects.filter(last_name='Jurič').first()
    predmet = Modulski_Predmet(sifra='63254', naslov_predmeta='Postopki razvoja programske opreme', smer=smer,
                               kreditne_tocke=6, modul=modul_RPO)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
    profesor = Profesor.objects.filter(last_name='Lavbič').first()
    predmet = Modulski_Predmet(sifra='63255', naslov_predmeta='Spletno programiranje', smer=smer,
                               kreditne_tocke=6, modul=modul_RPO)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Mahnič').first()
    predmet = Modulski_Predmet(sifra='63256', naslov_predmeta='Tehnologija programske opreme', smer=smer,
                               kreditne_tocke=6, modul=modul_RPO)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
        # Modul Računalniška omrežja

    modul_RO = Modul(sifra='00000004', ime_modula='Računalniška omrežja')
    modul_RO.save()

    profesor = Profesor.objects.filter(last_name='Ciglarič').first()
    predmet = Modulski_Predmet(sifra='63258', naslov_predmeta='Komunikacijski protokoli', smer=smer,
                               kreditne_tocke=6, modul=modul_RO)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
    profesor = Profesor.objects.filter(last_name='Zimic').first()
    predmet = Modulski_Predmet(sifra='63259', naslov_predmeta='Brezžična in mobilna omrežja', smer=smer,
                               kreditne_tocke=6, modul=modul_RO)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Mraz').first()
    predmet = Modulski_Predmet(sifra='63257', naslov_predmeta='Modeliranje računalniških omrežij', smer=smer,
                               kreditne_tocke=6, modul=modul_RO)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
        # Modul Računalniški sistemi

    modul_RI = Modul(sifra='00000005', ime_modula='Računalniški sistemi')
    modul_RI.save()

    profesor = Profesor.objects.filter(last_name='Mraz').first()
    predmet = Modulski_Predmet(sifra='63262', naslov_predmeta='Zanesljivost in zmogljivost računalniških sistemov',
                               smer=smer,
                               kreditne_tocke=6, modul=modul_RI)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
    profesor = Profesor.objects.filter(last_name='Bulić').first()
    predmet = Modulski_Predmet(sifra='63260', naslov_predmeta='Digitalno načrtovanje', smer=smer,
                               kreditne_tocke=6, modul=modul_RI)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Lotrič').first()
    predmet = Modulski_Predmet(sifra='63261', naslov_predmeta='Porazdeljeni sistemi', smer=smer,
                               kreditne_tocke=6, modul=modul_RI)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
        # Modul Algoritmi in sistemski programi

    modul_AIP = Modul(sifra='00000006', ime_modula='Algoritmi in sistemski programi')
    modul_AIP.save()

    profesor = Profesor.objects.filter(last_name='Slivnik').first()
    predmet = Modulski_Predmet(sifra='63265', naslov_predmeta='Prevajalniki', smer=smer,
                               kreditne_tocke=6, modul=modul_AIP)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
    profesor = Profesor.objects.filter(last_name='Dobravec').first()
    predmet = Modulski_Predmet(sifra='63264', naslov_predmeta='Sistemska programska oprema', smer=smer,
                               kreditne_tocke=6, modul=modul_AIP)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Šikonja').first()
    predmet = Modulski_Predmet(sifra='63263', naslov_predmeta='Analiza algoritmov in hevristično reševanje problemov',
                               smer=smer,
                               kreditne_tocke=6, modul=modul_AIP)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
        # Modul Umetna inteligenca

    modul_UI = Modul(sifra='00000007', ime_modula='Umetna inteligenca')
    modul_UI.save()

    profesor = Profesor.objects.filter(last_name='Skočaj').first()
    predmet = Modulski_Predmet(sifra='63268', naslov_predmeta='Razvoj inteligentnih sistemov', smer=smer,
                               kreditne_tocke=6, modul=modul_UI)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
    profesor = Profesor.objects.filter(last_name='Kononenko').first()
    predmet = Modulski_Predmet(sifra='63266', naslov_predmeta='Inteligentni sistemi', smer=smer,
                               kreditne_tocke=6, modul=modul_UI)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Kristan').first()
    predmet = Modulski_Predmet(sifra='63267', naslov_predmeta='Umetno zaznavanje', smer=smer,
                               kreditne_tocke=6, modul=modul_UI)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
        # Modul Medijske tehnologije

    modul_MT = Modul(sifra='00000008', ime_modula='Medijske tehnologije')
    modul_MT.save()

    profesor = Profesor.objects.filter(last_name='Bovcon').first()
    predmet = Modulski_Predmet(sifra='63271', naslov_predmeta='Osnove oblikovanja', smer=smer,
                               kreditne_tocke=6, modul=modul_MT)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
    profesor = Profesor.objects.filter(last_name='Kristan').first()
    predmet = Modulski_Predmet(sifra='63270', naslov_predmeta='Multimedijski sistemi', smer=smer,
                               kreditne_tocke=6, modul=modul_MT)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Marolt').first()
    predmet = Modulski_Predmet(sifra='63269', naslov_predmeta='Računalniška grafika in tehnologija iger', smer=smer,
                               kreditne_tocke=6, modul=modul_MT)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()
        # Splosni predmeti

    profesor = Profesor.objects.filter(last_name='Bosnić').first()
    predmet = Splosni_Predmet(sifra='63214', naslov_predmeta='Osnove umetne inteligence', kreditne_tocke=6, smer=smer,
                              letnik=3)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Mateja').first()
    predmet = Splosni_Predmet(sifra='63248', naslov_predmeta='Ekonomika in podjetništvo', kreditne_tocke=6, smer=smer,
                              letnik=3)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Solina').first()
    predmet = Splosni_Predmet(sifra='63281', naslov_predmeta='Diplomski seminar', kreditne_tocke=6, smer=smer,
                              letnik=3)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()


def seed_prvi_letnik_profesor_vs(fake):
    # Pridobi prvo posto, vseeno kero
    posta = Posta.objects.first()

    # Pridobi profesor group
    group = Group.objects.filter(name='profesor').first()

    # Peter Peer <3
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Peter'
    lastName = 'Peer'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Roman Drnovšek
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Roman'
    lastName = 'Drnovšek'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Janez Demšar
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Janez'
    lastName = 'Demšar'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Sandi Klavžar
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Sandi'
    lastName = 'Klavžar'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()

    # Igor Škraba
    sifra = random.randrange(10000000, 99999999)
    birth = fake.date(pattern="%Y-%m-%d")
    phone = fake.phone_number().replace(" ", "")
    firstName = 'Igor'
    lastName = 'Škraba'
    ulica = fake.street_address()
    emso = random.randrange(1000000000000, 9999999999999)
    first_name_encoded = unicodedata.normalize('NFD', firstName).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName).encode('ascii', 'ignore')
    assigned_email = first_name_encoded.decode().lower() + '.' + last_name_encoded.decode().lower() + '@fri.uni-lj.si'
    new_professor = Profesor(sifra=sifra, first_name=firstName,
                             last_name=lastName, datum_rojstva=birth, posta=posta, tel=phone, drzavljanstvo="SLOVENIA",
                             emso=emso, email=assigned_email, ulica=ulica)
    new_professor.save()
    new_professor.groups.add(group)
    new_professor.set_password('password123')
    new_professor.save()


def seed_prvi_letnik_predmet_vs(smer):
    studijska_leta = Studijsko_leto.objects.all()

    profesor = Profesor.objects.filter(last_name='Demšar').first()
    predmet = Splosni_Predmet(sifra='63702', naslov_predmeta='Programiranje 1', kreditne_tocke=6, smer=smer,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Klavžar').first()
    predmet = Splosni_Predmet(sifra='63705', naslov_predmeta='Diskretne strukture', kreditne_tocke=6, smer=smer,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(first_name='Roman').first()
    predmet = Splosni_Predmet(sifra='63704', naslov_predmeta='Matematika', smer=smer,
                              kreditne_tocke=6,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Skočaj').first()
    predmet = Splosni_Predmet(sifra='63701', naslov_predmeta='Uvod v računalništvo', smer=smer,
                              kreditne_tocke=6,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Škraba').first()
    predmet = Splosni_Predmet(sifra='63703', naslov_predmeta='Računalniška arhitektura', smer=smer,
                              kreditne_tocke=6,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Dobravec').first()
    predmet = Splosni_Predmet(sifra='63706', naslov_predmeta='Programiranje 2', smer=smer,
                              kreditne_tocke=6,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Kukar').first()
    predmet = Splosni_Predmet(sifra='63707', naslov_predmeta='Podatkovne baze', smer=smer,
                              kreditne_tocke=6,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Jurišić').first()
    predmet = Splosni_Predmet(sifra='63710', naslov_predmeta='Osnove verjetnosti in statistike', smer=smer,
                              kreditne_tocke=6,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Peer').first()
    predmet = Splosni_Predmet(sifra='63709', naslov_predmeta='Operacijski sistemi', smer=smer,
                              kreditne_tocke=6,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()

    profesor = Profesor.objects.filter(last_name='Ciglarič').first()
    predmet = Splosni_Predmet(sifra='63708', naslov_predmeta='Računalniške komunikacije', smer=smer,
                              kreditne_tocke=6,
                              letnik=1)
    predmet.save()
    for studijsko_leto in studijska_leta:
        Predmet_leto(predmet=predmet, profesor=profesor, studijsko_leto=studijsko_leto).save()


def seed_pecnik(posta, group, fake, obcina):
    studijsko_leto = Studijsko_leto.objects.get(studijsko_leto='2016/2017')
    evidenca_vpis_17 = Evidenca_Vpis.objects.filter(studijsko_leto=studijsko_leto)

    drzava = Drzava.objects.get(num_oznaka="705")
    vrste = Vrsta_vpis.objects.all()
    oblika = Oblika_studij.objects.all()
    nacin = Nacin_studij.objects.all()
    smer = Smer.objects.all()
    st_vpisov = 1004
    st_vpisov += 1
    emso = '1403995500141'
    vpisna = '63' + '14' + '{num:04d}'.format(num=st_vpisov)
    firstName = fake.first_name()
    lastName = fake.last_name()
    ulica = fake.street_address()
    phone = fake.phone_number().replace(" ", "")
    birth = fake.date(pattern="%Y-%m-%d")
    posta = posta
    obcina = obcina
    group = group
    first_name_encoded = unicodedata.normalize('NFD', firstName.lower()).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName.lower()).encode('ascii', 'ignore')
    first_name_initial = first_name_encoded.decode()[0]
    last_name_initial = last_name_encoded.decode()[0]
    rnd_num = random.randrange(1000, 9999, 1)
    lastniemail = first_name_initial + last_name_initial + str(rnd_num) + '.doma@student.si'
    assigned_email = first_name_initial + last_name_initial + str(rnd_num) + '@student.uni-lj.si'
    new_student = Student(first_name=firstName, last_name=lastName, email=assigned_email, vpisna=vpisna,
                          osebni_email=lastniemail, drzava=drzava,
                          tel=phone, ulica_naslov=ulica, ulica_prejem=ulica, posta_naslov=posta,
                          posta_prejem=posta, drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                          kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina,
                          obcina_naslov=obcina,
                          datum_rojstva=birth)
    new_student.save()
    new_student.groups.add(group)
    new_student.set_password('password123')
    new_student.save()

    Coin(student=new_student, vrsta=vrste[0], oblika=oblika[0], nacin=nacin[0], smer=smer[0],
         letnik=1, pravica_izbire=True, used=True).save()

    Vpis(letnik=1, leto_vpisa=studijsko_leto, potrjen=True, student=new_student, smer=smer[0],
         tip=nacin[0], vrsta=vrste[0]).save()
    predmet = Predmet_leto.objects.get(pk=3)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=8)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=13)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=18)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=23)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=28)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=33)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=38)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=43)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=48)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit3 = Izpit(predmet=predmet, datum='2014-6-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    izpit3.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit3, ocena=5, tocke=5).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=3, stPop=0).save()

    Coin(student=new_student, vrsta=vrste[0], oblika=oblika[0], nacin=nacin[0], smer=smer[0],
         letnik=2, pravica_izbire=True, used=True).save()
    studijsko_leto = Studijsko_leto.objects.get(studijsko_leto='2017/2018')
    Vpis(letnik=2, leto_vpisa=studijsko_leto, potrjen=True, student=new_student, smer=smer[0],
         tip=nacin[0], vrsta=vrste[0]).save()
    predmet = Predmet_leto.objects.get(pk=49)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    predmet = Predmet_leto.objects.get(pk=89)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=2).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=0, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=94)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=2).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=0, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=99)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=2).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=0, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=104)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=2).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=0, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=109)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=2).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=0, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=114)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=2).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=0, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=119)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=2).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=0, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=124)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=2).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=1, stPop=0).save()
    izpit = Izpit(predmet=predmet, datum='2018-06-01', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    Evidenca_izpit(student=new_student, izpit=izpit).save()

    ##################
    studijsko_leto = Studijsko_leto.objects.get(studijsko_leto='2016/2017')
    evidenca_vpis_17 = Evidenca_Vpis.objects.filter(studijsko_leto=studijsko_leto)

    drzava = Drzava.objects.get(num_oznaka="705")
    vrste = Vrsta_vpis.objects.all()
    oblika = Oblika_studij.objects.all()
    nacin = Nacin_studij.objects.all()
    smer = Smer.objects.all()
    st_vpisov = 1005
    st_vpisov += 1
    emso = '1403995500141'
    vpisna = '63' + '14' + '{num:04d}'.format(num=st_vpisov)
    firstName = fake.first_name()
    lastName = fake.last_name()
    ulica = fake.street_address()
    phone = fake.phone_number().replace(" ", "")
    birth = fake.date(pattern="%Y-%m-%d")
    first_name_encoded = unicodedata.normalize('NFD', firstName.lower()).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName.lower()).encode('ascii', 'ignore')
    first_name_initial = first_name_encoded.decode()[0]
    last_name_initial = last_name_encoded.decode()[0]
    rnd_num = random.randrange(1000, 9999, 1)
    lastniemail = first_name_initial + last_name_initial + str(rnd_num) + '.doma@student.si'
    assigned_email = first_name_initial + last_name_initial + str(rnd_num) + '@student.uni-lj.si'
    new_student = Student(first_name=firstName, last_name=lastName, email=assigned_email, vpisna=vpisna,
                          osebni_email=lastniemail, drzava=drzava,
                          tel=phone, ulica_naslov=ulica, ulica_prejem=ulica, posta_naslov=posta,
                          posta_prejem=posta, drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                          kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina,
                          obcina_naslov=obcina,
                          datum_rojstva=birth)
    new_student.save()
    new_student.groups.add(group)
    new_student.set_password('password123')
    new_student.save()

    Coin(student=new_student, vrsta=vrste[0], oblika=oblika[0], nacin=nacin[0], smer=smer[0],
         letnik=1, pravica_izbire=True, used=True).save()

    Vpis(letnik=1, leto_vpisa=studijsko_leto, potrjen=True, student=new_student, smer=smer[0],
         tip=nacin[0], vrsta=vrste[0]).save()
    predmet = Predmet_leto.objects.get(pk=3)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=8)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=13)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=18)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=23)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=28)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=33)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=38)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=43)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit3 = Izpit(predmet=predmet, datum='2014-6-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    izpit3.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=5, tocke=5).save()
    Evidenca_izpit(student=new_student, izpit=izpit3, ocena=5, tocke=5).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=3, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=48)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit3 = Izpit(predmet=predmet, datum='2014-6-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    izpit3.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit3, ocena=5, tocke=5).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=3, stPop=0).save()

    Coin(student=new_student, vrsta=vrste[1], oblika=oblika[0], nacin=nacin[1], smer=smer[0],
         letnik=1, pravica_izbire=True, used=True).save()
    studijsko_leto = Studijsko_leto.objects.get(studijsko_leto='2017/2018')
    Vpis(letnik=1, leto_vpisa=studijsko_leto, potrjen=True, student=new_student, smer=smer[0],
         tip=nacin[1], vrsta=vrste[1]).save()
    predmet = Predmet_leto.objects.get(pk=49)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=3, stPop=3).save()
    predmet = Predmet_leto.objects.get(pk=44)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=3, stPop=3).save()
    predmet = Predmet_leto.objects.get(pk=89)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=2).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=0, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=94)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=2).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=0, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=99)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=2).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=0, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=104)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=2).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=0, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=109)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=2).save()
    izpit = Izpit(predmet=predmet, datum='2018-06-01', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=1, stPop=0).save()
    Evidenca_izpit(student=new_student, izpit=izpit).save()
    #############################################
    studijsko_leto = Studijsko_leto.objects.get(studijsko_leto='2016/2017')
    evidenca_vpis_17 = Evidenca_Vpis.objects.filter(studijsko_leto=studijsko_leto)

    drzava = Drzava.objects.get(num_oznaka="705")
    vrste = Vrsta_vpis.objects.all()
    oblika = Oblika_studij.objects.all()
    nacin = Nacin_studij.objects.all()
    smer = Smer.objects.all()
    st_vpisov = 1006
    st_vpisov += 1
    emso = '1403995500141'
    vpisna = '63' + '14' + '{num:04d}'.format(num=st_vpisov)
    firstName = fake.first_name()
    lastName = fake.last_name()
    ulica = fake.street_address()
    phone = fake.phone_number().replace(" ", "")
    birth = fake.date(pattern="%Y-%m-%d")
    first_name_encoded = unicodedata.normalize('NFD', firstName.lower()).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName.lower()).encode('ascii', 'ignore')
    first_name_initial = first_name_encoded.decode()[0]
    last_name_initial = last_name_encoded.decode()[0]
    rnd_num = random.randrange(1000, 9999, 1)
    lastniemail = first_name_initial + last_name_initial + str(rnd_num) + '.doma@student.si'
    assigned_email = first_name_initial + last_name_initial + str(rnd_num) + '@student.uni-lj.si'
    new_student = Student(first_name=firstName, last_name=lastName, email=assigned_email, vpisna=vpisna,
                          osebni_email=lastniemail, drzava=drzava,
                          tel=phone, ulica_naslov=ulica, ulica_prejem=ulica, posta_naslov=posta,
                          posta_prejem=posta, drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                          kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina,
                          obcina_naslov=obcina,
                          datum_rojstva=birth)
    new_student.save()
    new_student.groups.add(group)
    new_student.set_password('password123')
    new_student.save()

    Coin(student=new_student, vrsta=vrste[0], oblika=oblika[0], nacin=nacin[0], smer=smer[0],
         letnik=1, pravica_izbire=True, used=True).save()

    Vpis(letnik=1, leto_vpisa=studijsko_leto, potrjen=True, student=new_student, smer=smer[0],
         tip=nacin[0], vrsta=vrste[0]).save()
    predmet = Predmet_leto.objects.get(pk=3)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=8)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=13)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=18)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=23)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=28)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=33)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=38)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=43)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=48)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit3 = Izpit(predmet=predmet, datum='2014-6-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    izpit3.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit3, ocena=5, tocke=5).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=3, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=49)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    ###############
    drzava = Drzava.objects.get(num_oznaka="705")
    st_vpisov = 1007
    st_vpisov += 1
    emso = '1403995500141'
    vpisna = '63' + '14' + '{num:04d}'.format(num=st_vpisov)
    firstName = fake.first_name()
    lastName = fake.last_name()
    ulica = fake.street_address()
    phone = fake.phone_number().replace(" ", "")
    birth = fake.date(pattern="%Y-%m-%d")
    first_name_encoded = unicodedata.normalize('NFD', firstName.lower()).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName.lower()).encode('ascii', 'ignore')
    first_name_initial = first_name_encoded.decode()[0]
    last_name_initial = last_name_encoded.decode()[0]
    rnd_num = random.randrange(1000, 9999, 1)
    lastniemail = first_name_initial + last_name_initial + str(rnd_num) + '.doma@student.si'
    assigned_email = first_name_initial + last_name_initial + str(rnd_num) + '@student.uni-lj.si'
    new_student = Student(first_name=firstName, last_name=lastName, email=assigned_email, vpisna=vpisna,
                          osebni_email=lastniemail, drzava=drzava,
                          tel=phone, ulica_naslov=ulica, ulica_prejem=ulica, posta_naslov=posta,
                          posta_prejem=posta, drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                          kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina,
                          obcina_naslov=obcina,
                          datum_rojstva=birth)
    new_student.save()
    new_student.groups.add(group)
    new_student.set_password('password123')
    new_student.save()
    predmet = Predmet_leto.objects.get(pk=49)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=1, stPop=0).save()
    izpit = Izpit(predmet_id=49, datum='2018-06-01', izredni=False, prostor='P10', cas="08:59",
                  izprasevalec=predmet.profesor)
    izpit.save()
    Evidenca_izpit(student=new_student, izpit=izpit).save()
    ####ponavljalec k more placat
    studijsko_leto = Studijsko_leto.objects.get(studijsko_leto='2015/2016')
    evidenca_vpis_17 = Evidenca_Vpis.objects.filter(studijsko_leto=studijsko_leto)

    drzava = Drzava.objects.get(num_oznaka="705")
    vrste = Vrsta_vpis.objects.all()
    oblika = Oblika_studij.objects.all()
    nacin = Nacin_studij.objects.all()
    smer = Smer.objects.all()
    st_vpisov = 1008
    st_vpisov += 1
    emso = '1403995500141'
    vpisna = '63' + '14' + '{num:04d}'.format(num=st_vpisov)
    firstName = fake.first_name()
    lastName = fake.last_name()
    ulica = fake.street_address()
    phone = fake.phone_number().replace(" ", "")
    birth = fake.date(pattern="%Y-%m-%d")
    first_name_encoded = unicodedata.normalize('NFD', firstName.lower()).encode('ascii', 'ignore')
    last_name_encoded = unicodedata.normalize('NFD', lastName.lower()).encode('ascii', 'ignore')
    first_name_initial = first_name_encoded.decode()[0]
    last_name_initial = last_name_encoded.decode()[0]
    rnd_num = random.randrange(1000, 9999, 1)
    lastniemail = first_name_initial + last_name_initial + str(rnd_num) + '.doma@student.si'
    assigned_email = first_name_initial + last_name_initial + str(rnd_num) + '@student.uni-lj.si'
    new_student = Student(first_name=firstName, last_name=lastName, email=assigned_email, vpisna=vpisna,
                          osebni_email=lastniemail, drzava=drzava,
                          tel=phone, ulica_naslov=ulica, ulica_prejem=ulica, posta_naslov=posta,
                          posta_prejem=posta, drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                          kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina,
                          obcina_naslov=obcina,
                          datum_rojstva=birth)
    new_student.save()
    new_student.groups.add(group)
    new_student.set_password('password123')
    new_student.save()

    Coin(student=new_student, vrsta=vrste[0], oblika=oblika[0], nacin=nacin[0], smer=smer[0],
         letnik=1, pravica_izbire=True, used=True).save()

    Vpis(letnik=1, leto_vpisa=studijsko_leto, potrjen=True, student=new_student, smer=smer[0],
         tip=nacin[0], vrsta=vrste[0]).save()
    predmet = Predmet_leto.objects.get(pk=2)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=7)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=12)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=17)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=22)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=27)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=32)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=37)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=randint(6, 10), tocke=randint(50, 100)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=2, stPop=0).save()
    predmet = Predmet_leto.objects.get(pk=42)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit3 = Izpit(predmet=predmet, datum='2014-6-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    izpit3.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=5, tocke=5).save()
    Evidenca_izpit(student=new_student, izpit=izpit3, ocena=5, tocke=5).save()
    predmet = Predmet_leto.objects.get(pk=47)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit3 = Izpit(predmet=predmet, datum='2014-6-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    izpit3.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit3, ocena=5, tocke=5).save()
    ####popravlja
    Coin(student=new_student, vrsta=vrste[1], oblika=oblika[0], nacin=nacin[0], smer=smer[0],
         letnik=1, pravica_izbire=True, used=True).save()
    studijsko_leto = Studijsko_leto.objects.get(studijsko_leto='2016/2017')
    Vpis(letnik=1, leto_vpisa=studijsko_leto, potrjen=True, student=new_student, smer=smer[0],
         tip=nacin[0], vrsta=vrste[1]).save()
    predmet = Predmet_leto.objects.get(pk=48)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=5, tocke=randint(0, 49)).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=5, stPop=3).save()
    predmet = Predmet_leto.objects.get(pk=43)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    izpit = Izpit(predmet=predmet, datum='2014-05-11', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit2 = Izpit(predmet=predmet, datum='2014-5-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit3 = Izpit(predmet=predmet, datum='2014-6-29', izredni=False, prostor='P10', cas="08:59", izprasevalec=predmet.profesor)
    izpit.save()
    izpit2.save()
    izpit3.save()
    Evidenca_izpit(student=new_student, izpit=izpit, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit2, ocena=5, tocke=randint(0, 49)).save()
    Evidenca_izpit(student=new_student, izpit=izpit3, ocena=5, tocke=5).save()
    SteviloOpravljanj(student=new_student, predmet=predmet.predmet, stVseh=6, stPop=3).save()

    predmet = Predmet_leto.objects.get(pk=44)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()
    predmet = Predmet_leto.objects.get(pk=49)
    Evidenca_student(predmet=predmet, student=new_student, letnik_predmeta=1).save()




    izpit = Izpit(predmet_id=49, datum='2018-06-11', izredni=False, prostor='P10', cas="08:59",
                  izprasevalec=predmet.profesor)
    izpit.save()

    izpit = Izpit(predmet_id=49, datum='2018-06-28', izredni=False, prostor='P10', cas="08:59",
                  izprasevalec=predmet.profesor)
    izpit.save()



def prvi_seed(request):
    fake = Faker('sl_SI')
    for potrdilo in PotrdiloVpisa.objects.all():
        potrdilo.delete()
    PotrdiloVpisa(stevilka=700000).save()

    for spol in Spol.objects.all():
        spol.delete()
    spol1 = Spol(spol='Moški').save()
    spol2 = Spol(spol='Ženski').save()

    for group in Group.objects.all():
        group.delete()
    professor_group = create_professors_group()
    student_group = create_student_group()
    referent_group = create_referent_group()

    drzava = seed_drzava()
    create_super_user()
    create_smer()  # crate UNI,VS
    create_studijsko_leto()
    create_evidenca_vpisa()  # create evidenca leta 2018
    create_nacin_studij()
    create_vrsta_vpis()
    create_oblika_studij()
    posta = seed_posta()
    seed_obcina()
    create_referent(fake=fake, group=referent_group)
    return HttpResponseRedirect('/')


def drugi_seed(request):
    fake = Faker('sl_SI')
    delete_subjects()

    smer_uni = Smer.objects.filter(smer='UNI').first()
    smer_vs = Smer.objects.filter(smer='VS').first()

    seed_prvi_letnik_profesor_uni(fake=fake)
    seed_prvi_letnik_predmet_uni(smer=smer_uni)

    seed_drugi_letnik_profesor_uni(fake=fake)
    seed_drugi_letnik_predmet_uni(smer=smer_uni)

    seed_tretji_letnik_profesor_uni(fake=fake)
    seed_tretji_letnik_predmet_uni(smer=smer_uni)

    seed_prvi_letnik_profesor_vs(fake=fake)
    seed_prvi_letnik_predmet_vs(smer=smer_vs)
    return HttpResponseRedirect('/')


def tretji_seed(request):
    fake = Faker('sl_SI')
    student_group = Group.objects.filter(name='student').first()
    drzava = Drzava.objects.filter(ime_drzave='Slovenija').first()
    posta = Posta.objects.filter(postna_stevilka=1000, drzava=drzava, kraj='Ljubljana').first()
    obcina = Obcina.objects.filter(drzava=drzava, sifra=61, ime="Ljubljana").first()
    seedStudents(3, posta, drzava, student_group, fake, obcina)
    seedDifferentStudents(20, posta, student_group, fake, obcina)
    seedVpis()
    seedSimulirajPredmetnik()
    seed_pecnik(posta, student_group, fake, obcina)

    return HttpResponseRedirect('/')


def cetrti_seed(request):

    for nosilec in Nosilci.objects.all():
        nosilec.delete()

    predmet_leto = Predmet_leto.objects.all()

    for x in predmet_leto:
        nosilec = Nosilci(predmet_leto=x, nosilec=x.profesor)
        nosilec.save()

    oim = Predmet_leto.objects.filter(predmet__sifra='63250', studijsko_leto=studijskoLeto()).first()
    prof1 = Profesor.objects.filter(last_name='Mateja').get()
    print(prof1)
    nosilec1 = Nosilci(predmet_leto=oim, nosilec=prof1)
    print(nosilec1.nosilec)
    nosilec1.save()

    return HttpResponseRedirect('/')


def peti_seed(request):
    fake = Faker('sl_SI')
    student_group = Group.objects.filter(name='student').first()
    drzava = Drzava.objects.filter(ime_drzave='Slovenija').first()
    posta = Posta.objects.filter(postna_stevilka=1000, drzava=drzava, kraj='Ljubljana').first()
    obcina = Obcina.objects.filter(drzava=drzava, sifra=61, ime="Ljubljana").first()
    drzava = Drzava.objects.get(num_oznaka="705")
    evidenca_vpis_14 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2014/2015')).first()
    evidenca_vpis_15 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2015/2016')).first()
    evidenca_vpis_16 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2016/2017')).first()
    evidenca_vpis_17 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2017/2018')).first()

    students_prvi(6, posta, student_group, fake, drzava, obcina)


    return HttpResponseRedirect('/')


def sesti_seed(request):
    fake = Faker('sl_SI')
    student_group = Group.objects.filter(name='student').first()
    drzava = Drzava.objects.filter(ime_drzave='Slovenija').first()
    posta = Posta.objects.filter(postna_stevilka=1000, drzava=drzava, kraj='Ljubljana').first()
    obcina = Obcina.objects.filter(drzava=drzava, sifra=61, ime="Ljubljana").first()
    drzava = Drzava.objects.get(num_oznaka="705")
    evidenca_vpis_14 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2014/2015')).first()
    evidenca_vpis_15 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2015/2016')).first()
    evidenca_vpis_16 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2016/2017')).first()
    evidenca_vpis_17 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2017/2018')).first()

    students_drugi(6, posta, student_group, fake, drzava, obcina)


    return HttpResponseRedirect('/')


def sedmi_seed(request):
    fake = Faker('sl_SI')
    student_group = Group.objects.filter(name='student').first()
    drzava = Drzava.objects.filter(ime_drzave='Slovenija').first()
    posta = Posta.objects.filter(postna_stevilka=1000, drzava=drzava, kraj='Ljubljana').first()
    obcina = Obcina.objects.filter(drzava=drzava, sifra=61, ime="Ljubljana").first()
    drzava = Drzava.objects.get(num_oznaka="705")
    evidenca_vpis_14 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2014/2015')).first()
    evidenca_vpis_15 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2015/2016')).first()
    evidenca_vpis_16 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2016/2017')).first()
    evidenca_vpis_17 = Evidenca_Vpis.objects.filter(
        studijsko_leto=Studijsko_leto.objects.get(studijsko_leto='2017/2018')).first()

    students_tretji(6, posta, student_group, fake, drzava, obcina)


    return HttpResponseRedirect('/')



def splosni_predmet_seed(letnik, student, leto, smer):

    predmeti_letnik = Splosni_Predmet.objects.filter(letnik=letnik, smer=smer).all()
    predmeti = Predmet_leto.objects.filter(studijsko_leto=leto, predmet__in=predmeti_letnik)

    for predmet in predmeti:
        izpit = Izpit(predmet=predmet, datum='2017-05-11', izredni=False, prostor='P10', cas="08:59")
        izpit.save()
        SteviloOpravljanj(predmet=predmet.predmet, student=student, stVseh=0, stPop=0).save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=randint(6, 10)).save()
        Evidenca_student(student=student, ocena=randint(6, 10), predmet=predmet, letnik_predmeta=letnik).save()

def dodaj_splosni_predmet_seed(letnik, student, leto, smer):

    predmeti_letnik = Splosni_Predmet.objects.filter(letnik=letnik, smer=smer).all()
    predmeti = Predmet_leto.objects.filter(studijsko_leto=leto, predmet__in=predmeti_letnik)

    for predmet in predmeti:
        SteviloOpravljanj(predmet=predmet.predmet, student=student, stVseh=0, stPop=0).save()
        Evidenca_student(student=student, predmet=predmet, letnik_predmeta=letnik).save()

def strokovni_predmet_seed(letnik, student, leto, smer):

    predmeti_letnik = Strokovni_izbirni.objects.filter(smer=smer, sifra='63219').all()
    predmeti = Predmet_leto.objects.filter(studijsko_leto=leto, predmet__in=predmeti_letnik)

    for predmet in predmeti:
        izpit = Izpit(predmet=predmet, datum='2017-05-11', izredni=False, prostor='P10', cas="08:59")
        izpit.save()
        SteviloOpravljanj(predmet=predmet.predmet, student=student, stVseh=0, stPop=0).save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=randint(6, 10)).save()
        Evidenca_student(student=student, ocena=randint(6, 10), predmet=predmet, letnik_predmeta=letnik).save()

def dodaj_strokovni_predmet_seed(letnik, student, leto, smer):

    predmeti_letnik = Strokovni_izbirni.objects.filter(smer=smer, sifra='63219').all()
    predmeti = Predmet_leto.objects.filter(studijsko_leto=leto, predmet__in=predmeti_letnik)

    for predmet in predmeti:
        SteviloOpravljanj(predmet=predmet.predmet, student=student, stVseh=0, stPop=0).save()
        Evidenca_student(student=student, predmet=predmet, letnik_predmeta=letnik).save()

def splosni_izbirni_seed(letnik, student, leto, smer):
    predmeti_letnik = Splosno_izbirni.objects.filter(smer=smer, sifra='63222').all()
    predmeti = Predmet_leto.objects.filter(studijsko_leto=leto, predmet__in=predmeti_letnik)
    predmeti_letnik2 = Splosno_izbirni.objects.filter(smer=smer, sifra='63223').all()
    predmeti2 = Predmet_leto.objects.filter(studijsko_leto=leto, predmet__in=predmeti_letnik2)

    for predmet in predmeti:
        izpit = Izpit(predmet=predmet, datum='2017-05-11', izredni=False, prostor='P10', cas="08:59")
        izpit.save()
        SteviloOpravljanj(predmet=predmet.predmet, student=student, stVseh=0, stPop=0).save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=randint(6, 10)).save()
        Evidenca_student(student=student, ocena=randint(6, 10), predmet=predmet, letnik_predmeta=letnik).save()

    for predmet in predmeti2:
        izpit = Izpit(predmet=predmet, datum='2017-05-11', izredni=False, prostor='P10', cas="08:59")
        izpit.save()
        SteviloOpravljanj(predmet=predmet.predmet, student=student, stVseh=0, stPop=0).save()
        Evidenca_izpit(student=student, izpit=izpit, ocena=randint(6, 10)).save()
        Evidenca_student(student=student, ocena=randint(6, 10), predmet=predmet, letnik_predmeta=letnik).save()

def dodaj_splosni_izbirni_seed(letnik, student, leto, smer):
    predmeti_letnik = Splosno_izbirni.objects.filter(smer=smer, sifra='63222').all()
    predmeti = Predmet_leto.objects.filter(studijsko_leto=leto, predmet__in=predmeti_letnik)
    predmeti_letnik2 = Splosno_izbirni.objects.filter(smer=smer, sifra='63223').all()
    predmeti2 = Predmet_leto.objects.filter(studijsko_leto=leto, predmet__in=predmeti_letnik2)

    for predmet in predmeti:
        SteviloOpravljanj(predmet=predmet.predmet, student=student, stVseh=0, stPop=0).save()
        Evidenca_student(student=student, predmet=predmet, letnik_predmeta=letnik).save()

    for predmet in predmeti2:
        SteviloOpravljanj(predmet=predmet.predmet, student=student, stVseh=0, stPop=0).save()
        Evidenca_student(student=student, predmet=predmet, letnik_predmeta=letnik).save()

def dodaj_modul_seed(letnik, student, leto, smer):
    prvi_modul = Modulski_Predmet.objects.filter(smer=smer, modul__ime_modula="Razvoj programske opreme").values_list(
        'id', flat=True)
    drugi_modul = Modulski_Predmet.objects.filter(smer=smer, modul__ime_modula="Informacijski sistemi").values_list(
        'id', flat=True)
    izbirni = Modulski_Predmet.objects.filter(naslov_predmeta="Prevajalniki", smer=smer).values_list('id', flat=True)
    predmeti_tretji_letnik = list(prvi_modul) + list(drugi_modul) + list(izbirni)

    predmeti_letnik = predmeti_tretji_letnik
    predmeti = Predmet_leto.objects.filter(studijsko_leto=leto, predmet_id__in=predmeti_letnik)

    for predmet in predmeti:
        SteviloOpravljanj(predmet=predmet.predmet, student=student, stVseh=0, stPop=0).save()
        Evidenca_student(student=student, predmet=predmet, letnik_predmeta=letnik).save()

def students_prvi(n, posta, group, fake, drzava, obcina):
    studijsko_leto = Studijsko_leto.objects.get(studijsko_leto='2017/2018')
    evidenca_vpis_16 = Evidenca_Vpis.objects.filter(
        studijsko_leto=studijsko_leto).first()
    studijsko_leto2 = Studijsko_leto.objects.get(studijsko_leto='2016/2017')
    evidenca_vpis_17 = Evidenca_Vpis.objects.filter(
        studijsko_leto=studijsko_leto2).first()

    for x in range(n):

        now = '18'
        st_vpisov = evidenca_vpis_16.st_vpisov
        evidenca_vpis_16.st_vpisov += 1
        evidenca_vpis_16.save()

        emso = '0101006500006'
        vpisna = '63' + now + '{num:04d}'.format(num=st_vpisov)
        firstName = fake.first_name()
        lastName = "Skok"
        ulica = fake.street_address()
        phone = fake.phone_number().replace(" ", "")
        birth = fake.date(pattern="%Y-%m-%d")
        first_name_encoded = unicodedata.normalize('NFD', firstName.lower()).encode('ascii', 'ignore')
        last_name_encoded = unicodedata.normalize('NFD', lastName.lower()).encode('ascii', 'ignore')
        first_name_initial = first_name_encoded.decode()[0]
        last_name_initial = last_name_encoded.decode()[0]
        rnd_num = random.randrange(1000, 9999, 1)
        lastniemail = first_name_initial + last_name_initial + str(rnd_num) + '.doma@student.si'
        assigned_email = first_name_initial + last_name_initial + str(rnd_num) + '@student.uni-lj.si'
        new_student = Student(first_name=firstName, last_name=lastName, email=assigned_email, vpisna=vpisna,
                              osebni_email=lastniemail, drzava=drzava,
                              tel=phone, ulica_naslov=ulica, ulica_prejem=ulica, posta_naslov=posta,
                              posta_prejem=posta, drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                              kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina,
                              obcina_naslov=obcina,
                              datum_rojstva=birth)
        new_student.save()
        new_student.groups.add(group)
        new_student.set_password('password123')
        new_student.save()

        smer = Smer.objects.first()
        nacin = Nacin_studij.objects.first()
        vrsta = Vrsta_vpis.objects.all()
        oblika = Oblika_studij.objects.all()

        Coin(student=new_student, vrsta=vrsta[0], oblika=oblika[0], nacin=nacin, smer=smer,
             letnik=1, pravica_izbire=True, used=True).save()

        Vpis(letnik=1, student=new_student, leto_vpisa=studijsko_leto,
             smer=smer, tip=nacin, vrsta=vrsta[0], potrjen=True).save()
        dodaj_splosni_predmet_seed(1, new_student, studijsko_leto, smer)
    for x in range(n):

        now = '17'
        st_vpisov = evidenca_vpis_17.st_vpisov
        evidenca_vpis_17.st_vpisov += 1
        evidenca_vpis_17.save()

        emso = '0101006500006'
        vpisna = '63' + now + '{num:04d}'.format(num=st_vpisov)
        firstName = fake.first_name()
        lastName = "Lazar"
        ulica = fake.street_address()
        phone = fake.phone_number().replace(" ", "")
        birth = fake.date(pattern="%Y-%m-%d")
        first_name_encoded = unicodedata.normalize('NFD', firstName.lower()).encode('ascii', 'ignore')
        last_name_encoded = unicodedata.normalize('NFD', lastName.lower()).encode('ascii', 'ignore')
        first_name_initial = first_name_encoded.decode()[0]
        last_name_initial = last_name_encoded.decode()[0]
        rnd_num = random.randrange(1000, 9999, 1)
        lastniemail = first_name_initial + last_name_initial + str(rnd_num) + '.doma@student.si'
        assigned_email = first_name_initial + last_name_initial + str(rnd_num) + '@student.uni-lj.si'
        new_student = Student(first_name=firstName, last_name=lastName, email=assigned_email, vpisna=vpisna,
                              osebni_email=lastniemail, drzava=drzava,
                              tel=phone, ulica_naslov=ulica, ulica_prejem=ulica, posta_naslov=posta,
                              posta_prejem=posta, drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                              kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina,
                              obcina_naslov=obcina,
                              datum_rojstva=birth)
        new_student.save()
        new_student.groups.add(group)
        new_student.set_password('password123')
        new_student.save()

        smer = Smer.objects.first()
        nacin = Nacin_studij.objects.first()
        vrsta = Vrsta_vpis.objects.all()
        oblika = Oblika_studij.objects.all()

        Coin(student=new_student, vrsta=vrsta[0], oblika=oblika[0], nacin=nacin, smer=smer,
             letnik=1, pravica_izbire=True, used=True).save()

        Vpis(letnik=1, student=new_student, leto_vpisa=studijsko_leto2,
             smer=smer, tip=nacin, vrsta=vrsta[0], potrjen=True).save()
        splosni_predmet_seed(1, new_student, studijsko_leto2, smer)

def students_drugi(n, posta, group, fake, drzava, obcina):
    studijsko_leto = Studijsko_leto.objects.get(studijsko_leto='2017/2018')
    evidenca_vpis_16 = Evidenca_Vpis.objects.filter(
        studijsko_leto=studijsko_leto).first()
    studijsko_leto1 = Studijsko_leto.objects.get(studijsko_leto='2016/2017')
    evidenca_vpis_17 = Evidenca_Vpis.objects.filter(
        studijsko_leto=studijsko_leto1).first()

    for x in range(n):

        now = '16'
        st_vpisov = evidenca_vpis_16.st_vpisov
        evidenca_vpis_16.st_vpisov += 1
        evidenca_vpis_16.save()

        now3 = '17'
        st_vpisov = evidenca_vpis_17.st_vpisov
        evidenca_vpis_17.st_vpisov += 1
        evidenca_vpis_17.save()

        emso = '0101006500006'
        vpisna = '63' + now + '{num:04d}'.format(num=st_vpisov)
        firstName = fake.first_name()
        lastName = "Jager"
        ulica = fake.street_address()
        phone = fake.phone_number().replace(" ", "")
        birth = fake.date(pattern="%Y-%m-%d")
        first_name_encoded = unicodedata.normalize('NFD', firstName.lower()).encode('ascii', 'ignore')
        last_name_encoded = unicodedata.normalize('NFD', lastName.lower()).encode('ascii', 'ignore')
        first_name_initial = first_name_encoded.decode()[0]
        last_name_initial = last_name_encoded.decode()[0]
        rnd_num = random.randrange(1000, 9999, 1)
        lastniemail = first_name_initial + last_name_initial + str(rnd_num) + '.doma@student.si'
        assigned_email = first_name_initial + last_name_initial + str(rnd_num) + '@student.uni-lj.si'
        new_student = Student(first_name=firstName, last_name=lastName, email=assigned_email, vpisna=vpisna,
                              osebni_email=lastniemail, drzava=drzava,
                              tel=phone, ulica_naslov=ulica, ulica_prejem=ulica, posta_naslov=posta,
                              posta_prejem=posta, drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                              kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina,
                              obcina_naslov=obcina,
                              datum_rojstva=birth)
        new_student.save()
        new_student.groups.add(group)
        new_student.set_password('password123')
        new_student.save()

        smer = Smer.objects.first()
        nacin = Nacin_studij.objects.first()
        vrsta = Vrsta_vpis.objects.all()
        oblika = Oblika_studij.objects.all()
        '''dodaj prvi letnik'''
        Coin(student=new_student, vrsta=vrsta[0], oblika=oblika[0], nacin=nacin, smer=smer,
             letnik=1, pravica_izbire=True, used=True).save()

        Vpis(letnik=1, student=new_student, leto_vpisa=studijsko_leto1,
             smer=smer, tip=nacin, vrsta=vrsta[0], potrjen=True).save()
        splosni_predmet_seed(1, new_student, studijsko_leto1, smer)
        '''dodaj drugi letnik'''
        Coin(student=new_student, vrsta=vrsta[0], oblika=oblika[0], nacin=nacin, smer=smer,
             letnik=2, pravica_izbire=True, used=True).save()

        Vpis(letnik=2, student=new_student, leto_vpisa=studijsko_leto,
             smer=smer, tip=nacin, vrsta=vrsta[0], potrjen=True).save()


        dodaj_splosni_predmet_seed(2, new_student, studijsko_leto, smer)
        dodaj_strokovni_predmet_seed(2, new_student, studijsko_leto, smer)
        dodaj_splosni_izbirni_seed(2, new_student, studijsko_leto, smer)

def students_tretji(n, posta, group, fake, drzava, obcina):
    studijsko_leto = Studijsko_leto.objects.get(studijsko_leto='2016/2017')
    evidenca_vpis_16 = Evidenca_Vpis.objects.filter(
        studijsko_leto=studijsko_leto).first()
    studijsko_leto1 = Studijsko_leto.objects.get(studijsko_leto='2015/2016')
    evidenca_vpis_15 = Evidenca_Vpis.objects.filter(
        studijsko_leto=studijsko_leto1).first()
    studijsko_leto2 = Studijsko_leto.objects.get(studijsko_leto='2017/2018')
    evidenca_vpis_17 = Evidenca_Vpis.objects.filter(
        studijsko_leto=studijsko_leto2).first()

    for x in range(n):

        now = '16'
        st_vpisov = evidenca_vpis_16.st_vpisov
        evidenca_vpis_16.st_vpisov += 1
        evidenca_vpis_16.save()

        now2 = '15'
        st_vpisov = evidenca_vpis_15.st_vpisov
        evidenca_vpis_15.st_vpisov += 1
        evidenca_vpis_15.save()

        now3 = '17'
        st_vpisov = evidenca_vpis_17.st_vpisov
        evidenca_vpis_17.st_vpisov += 1
        evidenca_vpis_17.save()

        emso = '0101006500006'
        vpisna = '63' + now2 + '{num:04d}'.format(num=st_vpisov)
        firstName = fake.first_name()
        lastName = "Kern"
        ulica = fake.street_address()
        phone = fake.phone_number().replace(" ", "")
        birth = fake.date(pattern="%Y-%m-%d")
        first_name_encoded = unicodedata.normalize('NFD', firstName.lower()).encode('ascii', 'ignore')
        last_name_encoded = unicodedata.normalize('NFD', lastName.lower()).encode('ascii', 'ignore')
        first_name_initial = first_name_encoded.decode()[0]
        last_name_initial = last_name_encoded.decode()[0]
        rnd_num = random.randrange(1000, 9999, 1)
        lastniemail = first_name_initial + last_name_initial + str(rnd_num) + '.doma@student.si'
        assigned_email = first_name_initial + last_name_initial + str(rnd_num) + '@student.uni-lj.si'
        new_student = Student(first_name=firstName, last_name=lastName, email=assigned_email, vpisna=vpisna,
                              osebni_email=lastniemail, drzava=drzava,
                              tel=phone, ulica_naslov=ulica, ulica_prejem=ulica, posta_naslov=posta,
                              posta_prejem=posta, drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                              kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina,
                              obcina_naslov=obcina,
                              datum_rojstva=birth)
        new_student.save()
        new_student.groups.add(group)
        new_student.set_password('password123')
        new_student.save()

        smer = Smer.objects.first()
        nacin = Nacin_studij.objects.first()
        vrsta = Vrsta_vpis.objects.all()
        oblika = Oblika_studij.objects.all()
        '''dodaj prvi letnik'''
        Coin(student=new_student, vrsta=vrsta[0], oblika=oblika[0], nacin=nacin, smer=smer,
             letnik=1, pravica_izbire=True, used=True).save()

        Vpis(letnik=1, student=new_student, leto_vpisa=studijsko_leto1,
             smer=smer, tip=nacin, vrsta=vrsta[0], potrjen=True).save()
        splosni_predmet_seed(1, new_student, studijsko_leto1, smer)
        '''dodaj drugi letnik'''
        Coin(student=new_student, vrsta=vrsta[0], oblika=oblika[0], nacin=nacin, smer=smer,
             letnik=2, pravica_izbire=True, used=True).save()

        Vpis(letnik=2, student=new_student, leto_vpisa=studijsko_leto,
             smer=smer, tip=nacin, vrsta=vrsta[0], potrjen=True).save()
        splosni_predmet_seed(2, new_student, studijsko_leto, smer)
        strokovni_predmet_seed(2, new_student, studijsko_leto, smer)
        splosni_izbirni_seed(2, new_student, studijsko_leto, smer)
        '''dodaj tretji letnik'''
        Coin(student=new_student, vrsta=vrsta[0], oblika=oblika[0], nacin=nacin, smer=smer,
             letnik=3, pravica_izbire=True, used=True).save()

        Vpis(letnik=3, student=new_student, leto_vpisa=studijsko_leto2,
             smer=smer, tip=nacin, vrsta=vrsta[0], potrjen=True).save()
        dodaj_splosni_predmet_seed(3, new_student, studijsko_leto2, smer)
        dodaj_modul_seed(3, new_student, studijsko_leto2, smer)

    for x in range(n):

        now = '16'
        st_vpisov = evidenca_vpis_16.st_vpisov
        evidenca_vpis_16.st_vpisov += 1
        evidenca_vpis_16.save()

        now2 = '15'
        st_vpisov = evidenca_vpis_15.st_vpisov
        evidenca_vpis_15.st_vpisov += 1
        evidenca_vpis_15.save()

        now3 = '17'
        st_vpisov = evidenca_vpis_17.st_vpisov
        evidenca_vpis_17.st_vpisov += 1
        evidenca_vpis_17.save()

        emso = '0101006500006'
        vpisna = '63' + now2 + '{num:04d}'.format(num=st_vpisov)
        firstName = fake.first_name()
        lastName = "Perčič"
        ulica = fake.street_address()
        phone = fake.phone_number().replace(" ", "")
        birth = fake.date(pattern="%Y-%m-%d")
        first_name_encoded = unicodedata.normalize('NFD', firstName.lower()).encode('ascii', 'ignore')
        last_name_encoded = unicodedata.normalize('NFD', lastName.lower()).encode('ascii', 'ignore')
        first_name_initial = first_name_encoded.decode()[0]
        last_name_initial = last_name_encoded.decode()[0]
        rnd_num = random.randrange(1000, 9999, 1)
        lastniemail = first_name_initial + last_name_initial + str(rnd_num) + '.doma@student.si'
        assigned_email = first_name_initial + last_name_initial + str(rnd_num) + '@student.uni-lj.si'
        new_student = Student(first_name=firstName, last_name=lastName, email=assigned_email, vpisna=vpisna,
                              osebni_email=lastniemail, drzava=drzava,
                              tel=phone, ulica_naslov=ulica, ulica_prejem=ulica, posta_naslov=posta,
                              posta_prejem=posta, drzavljanstvo=drzava, drzava_naslov=drzava, drzava_prejem=drzava,
                              kraj_rojstva="Ljubljana", obcina_rojstva=obcina, obcina_prejem=obcina,
                              obcina_naslov=obcina,
                              datum_rojstva=birth)
        new_student.save()
        new_student.groups.add(group)
        new_student.set_password('password123')
        new_student.save()

        smer = Smer.objects.first()
        nacin = Nacin_studij.objects.first()
        vrsta = Vrsta_vpis.objects.all()
        oblika = Oblika_studij.objects.all()
        '''dodaj prvi letnik'''
        Coin(student=new_student, vrsta=vrsta[0], oblika=oblika[0], nacin=nacin, smer=smer,
             letnik=1, pravica_izbire=True, used=True).save()

        Vpis(letnik=1, student=new_student, leto_vpisa=studijsko_leto1,
             smer=smer, tip=nacin, vrsta=vrsta[0], potrjen=True).save()
        splosni_predmet_seed(1, new_student, studijsko_leto1, smer)
        '''dodaj drugi letnik'''
        Coin(student=new_student, vrsta=vrsta[0], oblika=oblika[0], nacin=nacin, smer=smer,
             letnik=2, pravica_izbire=True, used=True).save()

        Vpis(letnik=2, student=new_student, leto_vpisa=studijsko_leto,
             smer=smer, tip=nacin, vrsta=vrsta[0], potrjen=True).save()
        splosni_predmet_seed(2, new_student, studijsko_leto, smer)
        strokovni_predmet_seed(2, new_student, studijsko_leto, smer)
        splosni_izbirni_seed(2, new_student, studijsko_leto, smer)