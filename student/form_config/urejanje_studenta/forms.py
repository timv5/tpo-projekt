from django import forms
import datetime
from Studis.models import Posta, Drzava, Spol

class VpisForm(forms.Form):
    first_name = forms.CharField(label='Ime', max_length=100)
    last_name = forms.CharField(label='Priimek', max_length=100)
    emso = forms.CharField(label='Emšo', max_length=13)
    datum_rojstva = forms.CharField(initial=datetime.datetime.now().strftime("%d.%m.%Y"), label='datum_rojstva')
    drzava = forms.ModelChoiceField(label="Država", queryset=Drzava.objects.all())
    spol=forms.ModelChoiceField(label='Spol', queryset=Spol.objects.all())
    tel = forms.CharField(label='Telefonska številka', max_length=15)
    ulica_prejem = forms.CharField(label='Ulica', max_length=50)
    posta_prejem = forms.ModelChoiceField(queryset=Posta.objects.all() ,widget=forms.Select(attrs={'class':'custom-select'}))
    #def clean_datum_rojstva(self):
        #datum_rojstva= self.cleaned_data.get('datum_rojstva')
        #dt = datetime.datetime.strptime(datum_rojstva, "%d.%m.%Y")
        #print(dt.strftime("%Y-%m-%d"))
        #return dt.strftime("%Y-%m-%d")