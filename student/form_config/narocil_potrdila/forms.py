from django import forms

MY_CHOICES = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
    ('6', '6'),
)

class NarociloForms(forms.Form):
    stevilo = forms.ChoiceField(choices=MY_CHOICES)