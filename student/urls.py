from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path
from . import views as student_views

urlpatterns = [
    #url(r'^(?P<pk>[0-9]+)/$', student_views.StudentMulti, name="student_detail"),
    url(r'^predmeti/$', student_views.SubjectList, name="subject_list"),
    url(r'^predmeti/prijavi/(?P<pk>[0-9]+)/$', student_views.PrijaviIzpit, name="PrijaviIzpit"),
    url(r'^predmeti/odjavi/(?P<pk>[0-9]+)/$', student_views.OdjaviIzpit, name="OdjaviIzpit"),
    url(r'account/$', student_views.account_details, name="acc_detail"),
    url(r'account/pdf/(?P<pk>[0-9]+)/$', student_views.generate_new_pdf_student, name="acc_detail_pdf"),
    url(r'account/csv/(?P<pk>[0-9]+)/$', student_views.generate_csv_student, name="acc_detail_csv"),
    url(r'account/kartoteka/$',student_views.kartoteka_detail, name="kartoteka_detail"), 
    url(r'account/indeks/$',student_views.indeks_detail, name="indeks_detail"),
    url(r'naroci/potrdilo/$',student_views.naroci_potrdilo, name="naroci_potrdilo")
]

urlpatterns = format_suffix_patterns(urlpatterns)