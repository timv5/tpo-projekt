import requests
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from rest_framework.views import APIView
from rest_framework.response import Response
from Studis.models.users import Student
from Studis.models.storitve import Coin, NarociloPotrdil
from Studis.models.subjects import Predmet, Predmet_leto
from Studis.models.storitve import Evidenca_student, Izpit, Coin, Evidenca_izpit, Vpis, Studijsko_leto, SteviloOpravljanj
from Studis.models import User
from api.serializers import StudentSerializer
from django.shortcuts import redirect

from student.form_config.narocil_potrdila.forms import NarociloForms
from .form_config.urejanje_studenta.forms import VpisForm as VpisFormaUrejanjeStudenta
from Studis.models import User
from django.contrib.auth.decorators import login_required, user_passes_test
from decorators import *
from django.template.loader import get_template
from django.http import HttpResponse
import csv
import datetime
from datetime import date, timedelta
from osebje.views import date_filter
from .exporter.student_printing import MyPrint as MyPrintStudent
from io import BytesIO
from django.contrib import messages
from django.db.models import Max
from django.shortcuts import render_to_response
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

@login_required
@user_passes_test(is_student)
def account_details(request):
    user = request.user
    if request.method == 'GET':
        student = Student.objects.select_related('posta_prejem','posta_naslov').get(pk=user.id)
        vpisi = student.vpis_set.all().select_related()
        Obstaja=False
        try:
            zeton = Coin.objects.filter(student=request.user.pk).last()
            Obstaja=True
            if zeton.used:
                Obstaja=False
        except:
            Obstaja=False
        return render(request, 'student_details.html', {'student': student, 'vpisi' : vpisi, 'je': Obstaja})

@login_required
@user_passes_test(is_student)
def SubjectList(request):
    predmeti = []
    for evidenca in Evidenca_student.objects.prefetch_related("predmet__predmet").filter(student=request.user).values("predmet__predmet__naslov_predmeta", "predmet__predmet__sifra").distinct():
        predmeti.append([evidenca["predmet__predmet__naslov_predmeta"], evidenca["predmet__predmet__sifra"]])
    student=request.user

    student1 = Student.objects.select_related('posta_prejem', 'posta_naslov').get(pk=student.id)
    vpisi = student1.vpis_set.all().select_related().last()
    #print(vpisi.leto_vpisa)
    vpisani_predmeti = []
    if vpisi is not None:
        vpisani_predmeti = Evidenca_student.objects.filter(predmet__studijsko_leto=vpisi.leto_vpisa,student=student)
    #print(vpisani_predmeti)
    vpisani_predmeti1=[o.predmet for o in vpisani_predmeti]
    """names_to_exclude_pon1 = [o.predmet for o in ponovno1]
    #print(names_to_exclude_pon1)
    names_to_exclude_pon2 = [o.predmet for o in ponovno2]
    #print(names_to_exclude_pon2)"""
    leto = Studijsko_leto.objects.all()
    #print(leto[2])
    #print(leto[3])

    if vpisi is None:
        vpisani_predmeti = Evidenca_student.objects.filter(student=student)
    else:
        vpisani_predmeti = Evidenca_student.objects.filter(student=student)
        max=0
        for x in vpisani_predmeti:
            if max < x.predmet.studijsko_leto.pk:
                max=x.predmet.studijsko_leto.pk
        vpisani_predmeti = Evidenca_student.objects.filter(predmet__studijsko_leto_id=max,student=student)


    vpisani_predmeti1=[o.predmet for o in vpisani_predmeti]

    leto = Studijsko_leto.objects.all()
    izpiti=[]
    for predmet in vpisani_predmeti1:
        tmp = Predmet_leto.objects.select_related('predmet', 'profesor', 'studijsko_leto').get(pk=predmet.pk)
        izpittmp = Izpit.objects.select_related().filter(predmet=tmp.pk, locked=False).filter(evidenca_izpit__ocena__isnull=True)
        if izpittmp.count() > 0:
            izpiti+=izpittmp
    izpiti1=[]
    datum_danes = datetime.date.today()
    for x in izpiti:
        if x not in izpiti1 and x.datum >= datum_danes:
            izpiti1.append(x)
    #print(izpiti1)
    #print(izpiti)
    izpiti1.sort(key=lambda x: x.datum)


    datumi=date_filter(izpiti)
    datumi=datumi[::-1]
    #datumi=date_filter(izpiti1)
    #datumi=datumi[::-1]
    #print(datumi)

    prijavljen=Evidenca_izpit.objects.filter(student=student).filter(ocena__isnull=True)
    prijavljeni=[]
    blockOstaliRok=[]
    for x in prijavljen:
        if x.izpit.predmet not in blockOstaliRok and x.status==True:
            blockOstaliRok.append(x.izpit.predmet)
        if x.status == True:
            prijavljeni.append(x.izpit)
    return render(request,"mysubjects.html", {'predmeti':vpisani_predmeti, 'izpiti':izpiti1, 'prijavljen':prijavljeni, 'blokiraj':blockOstaliRok, 'evidenca':vpisani_predmeti})# ,'datumi':datumi})

#uporabljen
def generate_new_pdf_student(request, pk):
    student = Student.objects.select_related('posta_prejem', 'posta_naslov').get(pk=pk)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="%s.pdf"' %(student.vpisna)
    buffer = BytesIO()
    report = MyPrintStudent(buffer, 'Letter', pk)
    pdf = report.print_users()
    response.write(pdf)
    return response

#uporabljen
def generate_csv_student(request, pk):
    student = Student.objects.select_related('posta_prejem', 'posta_naslov').get(pk=pk)
    vpisi = student.vpis_set.all().select_related()
    response = HttpResponse(content_type='text/csv')
    filename = "Student_%s.csv" % (student.vpisna)
    response['Content-Disposition'] = 'attachment; filename="%s"' % (filename)

    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)
    date = datetime.date.today()
    writer.writerow(['Datum izpisa', date])
    writer.writerow([])
    writer.writerow([])
    writer.writerow([])
    writer.writerow(['Osebni podatki študenta'])
    writer.writerow(['Ime', student.first_name])
    writer.writerow(['Priimek', student.last_name])
    writer.writerow(['Vpisna številka', student.vpisna])
    writer.writerow(['Univerzitetni e-mail', student.email])
    writer.writerow(['Telefonska številka', student.tel])
    writer.writerow(['Osebni e-mail', student.osebni_email])
    writer.writerow([''])
    writer.writerow([''])

    writer.writerow(['Naslov stalnega prebivališča'])
    writer.writerow(['Stalno prebivališče - Država', student.posta_naslov.drzava])
    writer.writerow(['Stalno prebivališče - Ulica', student.ulica_naslov])
    writer.writerow(['Stalno prebivališče - Pošta', student.posta_naslov.postna_stevilka])
    writer.writerow(['Stalno prebivališče - Kraj', student.posta_naslov.kraj])
    writer.writerow([''])
    writer.writerow([''])

    writer.writerow(['Naslov za dostavo pošte'])
    writer.writerow(['Prejem - Država', student.posta_prejem.drzava])
    writer.writerow(['Prejem - Ulica', student.ulica_prejem])
    writer.writerow(['Prejem - Pošta', student.posta_prejem.postna_stevilka])
    writer.writerow(['Prejem - Kraj', student.posta_prejem.kraj])
    writer.writerow([''])
    writer.writerow([''])

    if vpisi:
        writer.writerow(['Podatki o vpisih'])
        for vpis in vpisi:
            writer.writerow(['Študijsko leto',vpis.leto_vpisa.studijsko_leto])
            writer.writerow(['Letnik',vpis.leto_vpisa.studijsko_leto])
            writer.writerow(['Študijski program',vpis.leto_vpisa.studijsko_leto])
            writer.writerow(['Vrsta vpisa',vpis.leto_vpisa.studijsko_leto])
            writer.writerow(['Način študija',vpis.leto_vpisa.studijsko_leto])
            writer.writerow([''])

    return response

def PrijaviIzpit(request,pk):
    student1=request.user.id
    student=Student.objects.filter(pk=student1)
    izpit=Izpit.objects.filter(pk=pk).get()
    coin=Coin.objects.filter(student=student1).last()

    #print(coin.vrsta.id)
    stOpravljanjLetos = Evidenca_izpit.objects.filter(izpit__predmet=izpit.predmet,student=student[0],ocena__isnull=False).count()
    stVsehOpravljanj =Evidenca_izpit.objects.filter(izpit__predmet__predmet=izpit.predmet.predmet,student=student[0],ocena__isnull=False).count()
    print("stopravljanj letos")
    #print(stOpravljanjLetos)
    #print("stopravljanj vseh")
    #print(stVsehOpravljanj)
    if coin.vrsta.id == 2:
        stVsehOpravljanj=stOpravljanjLetos
    xxx = Student.objects.select_related('posta_prejem', 'posta_naslov').get(pk=request.user.id)
    vpisi = xxx.vpis_set.all().select_related().last()
    izredni=vpisi.tip.sifra
    #print(izredni)
    zadnjaocena= Evidenca_izpit.objects.filter(izpit__predmet=izpit.predmet,student=student[0],ocena__gt=5).count()
    #print(zadnjaocena)
    stOpravljanjLetos=Evidenca_izpit.objects.filter(izpit__predmet=izpit.predmet,student=student[0],ocena__isnull=False).count()
    stDejanskihOpravljanj=SteviloOpravljanj.objects.filter(student=student[0],predmet=izpit.predmet.predmet).last().stVseh
    stPopravljanj=SteviloOpravljanj.objects.filter(student=student[0],predmet=izpit.predmet.predmet).last().stPop
    stVeljavnihOpravljanj=stDejanskihOpravljanj-stPopravljanj

    xxx = Student.objects.select_related('posta_prejem', 'posta_naslov').get(pk=request.user.id)
    vpisi = xxx.vpis_set.all().select_related().last()
    if vpisi is None:
        leto_vpisa='0'
    else:
        leto_vpisa=vpisi.leto_vpisa
    leto_predmeta=izpit.predmet.studijsko_leto

    izredni=1
    if leto_predmeta != leto_vpisa:
        izredni=3
        message = str("Nevpisan študent.")
        messages.success(request, message)

    #omejitev prijave 2 dni prej
    datum_in_2_days = (datetime.datetime.now() + timedelta(days=2)).strftime("%Y-%m-%d")
    datum_izpita=izpit.datum.strftime("%Y-%m-%d")
    if zadnjaocena == 0:
        imaOceno = Evidenca_izpit.objects.filter(izpit__predmet=izpit.predmet, student=student[0], ocena__isnull=False).order_by('-izpit__datum').first() #zadni izpit z oceno
        if( imaOceno is not None): #ce ni prvo opravljanje
            # zadnje opravljanje pred 14dnevi
            zadnji_datum=(imaOceno.izpit.datum + timedelta(days=14)).strftime("%Y-%m-%d")
            if stVeljavnihOpravljanj < 6:
                if stOpravljanjLetos < 3:
                    if datum_in_2_days > datum_izpita:
                        message = str("Prepozna prijava.")
                        messages.success(request, message)
                    else:
                        if(zadnji_datum > datum_izpita):
                            message = str("Mora minit 14 dni. ("+str(stDejanskihOpravljanj)+" - "+str(stPopravljanj)+") = "+str(stVeljavnihOpravljanj))
                            messages.success(request, message)
                        else:
                            if stVeljavnihOpravljanj > 2 or izredni == 3:
                                message = str("Plačilni izpit. Ocena vam bo vpisana, ko boste poravnali položnico.")
                                messages.success(request, message)
                            if Evidenca_izpit.objects.filter(izpit=izpit,student=student[0]).count() == 0:
                                Evidenca_izpit(izpit=izpit,student=student[0]).save()
                                dodaj = SteviloOpravljanj.objects.filter(student=student[0],
                                                                                    predmet=izpit.predmet.predmet).last()
                                dodaj.stVseh=dodaj.stVseh+1
                                dodaj.save()
                                message = str("(" + str(stDejanskihOpravljanj+1)+" - "+str(stPopravljanj)+") = "+str(stVeljavnihOpravljanj+1))
                                messages.success(request, message)
                            else:
                                prijava=Evidenca_izpit.objects.filter(izpit=izpit,student=student[0]).get()
                                prijava.datum_odjave=None
                                prijava.odjavitelj=None
                                prijava.status=True
                                prijava.save()
                                dodaj = SteviloOpravljanj.objects.filter(student=student[0],
                                                                         predmet=izpit.predmet.predmet).last()
                                dodaj.stVseh = dodaj.stVseh + 1
                                dodaj.save()
                                message = str(
                                    "(" + str(stDejanskihOpravljanj+1) + " - " + str(stPopravljanj) + ") = " + str(
                                        stVeljavnihOpravljanj+1))
                                messages.success(request, message)
                                message = str("Uspešna prijava.")
                                messages.success(request, message)
                else:
                    message = str("Preveč opravljanj. Presegli ste 3 opravljanja letos")
                    messages.success(request, message)
            else:
                message = str("Preveč opravljanj. Presegli ste vseh 6 opravljanj")
                messages.success(request, message)
        else:
            if stVeljavnihOpravljanj < 6:
                if stOpravljanjLetos < 3:
                    if datum_in_2_days > datum_izpita:
                        message = str("Prepozna prijava.")
                        messages.success(request, message)
                    else:
                        if stVeljavnihOpravljanj > 2 or izredni == 3:
                            message = str("Plačilni izpit. Ocena vam bo vpisana, ko boste poravnali položnico.")
                            messages.success(request, message)
                        if Evidenca_izpit.objects.filter(izpit=izpit,student=student[0]).count() == 0:
                            Evidenca_izpit(izpit=izpit,student=student[0]).save()
                            dodaj = SteviloOpravljanj.objects.filter(student=student[0],
                                                                     predmet=izpit.predmet.predmet).last()
                            dodaj.stVseh = dodaj.stVseh + 1
                            dodaj.save()
                            message = str(
                                "(" + str(stDejanskihOpravljanj+1) + " - " + str(stPopravljanj) + ") = " + str(
                                    stVeljavnihOpravljanj+1))
                            messages.success(request, message)
                        else:
                            prijava=Evidenca_izpit.objects.filter(izpit=izpit,student=student[0]).get()
                            prijava.datum_odjave=None
                            prijava.odjavitelj=None
                            prijava.status=True
                            prijava.save()
                            dodaj = SteviloOpravljanj.objects.filter(student=student[0],
                                                                     predmet=izpit.predmet.predmet).last()
                            dodaj.stVseh = dodaj.stVseh + 1
                            dodaj.save()
                            message = str(
                                "(" + str(stDejanskihOpravljanj+1) + " - " + str(stPopravljanj) + ") = " + str(
                                    stVeljavnihOpravljanj+1))
                            messages.success(request, message)
                            message = str("Uspešna prijava.")
                            messages.success(request, message)
                else:
                    message = str("Preveč opravljanj. Presegli ste 3 opravljanja letos")
                    messages.success(request, message)
            else:
                message = str("Preveč opravljanj. Presegli ste vseh 6 opravljanj")
                messages.success(request, message)
    else:
        message = str("Predmet imate že opravljen. Za ponovno opravljanje kontaktirajte študentsko pisarno.")
        messages.success(request, message)
    return redirect('subject_list')

def OdjaviIzpit(request,pk):
    student1 = request.user.id
    student = Student.objects.filter(pk=student1)
    #print(student[0])
    izpit = Izpit.objects.filter(pk=pk).get()
    odjava=Evidenca_izpit.objects.filter(izpit=izpit,student=student[0]).get()
    odjava.status=False
    odjava.odjavitelj=str(student[0])
    datum=datetime.datetime.now().strftime("%d.%m.%Y %H:%M")
    #print(datum)
    poskus_odjava= (datetime.datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d")
    datum_izpita=odjava.izpit.datum.strftime("%Y-%m-%d")
    #print(poskus_odjava)
    #print(datum_izpita)
    if poskus_odjava < datum_izpita:
        odjava.datum_odjave=datum
        odjava.save()
        stVsehOpravljanj = SteviloOpravljanj.objects.filter(student=student[0], predmet=izpit.predmet.predmet).last()
        stVsehOpravljanj.stVseh=stVsehOpravljanj.stVseh-1
        stVsehOpravljanj.save() 
    else:
        message = str("Prepozna odjava.")
        messages.success(request, message)
    return redirect('subject_list')

@login_required
@user_passes_test(is_student)
def kartoteka_detail(request):
    user = request.user
    student = Student.objects.get(pk=user.id)
    studijsko_leto = request.GET.get('studijsko_leto')
    polaganja = request.GET.get('polaganja')

    vpisi = Vpis.objects.filter(student=student)
    data = [[x] for x in vpisi] 
    predmeti_evidenca = Evidenca_student.objects.filter(student=student)
    predmeti_id = [x.predmet.id for x in predmeti_evidenca]
    for s,vpis in enumerate(vpisi):
        ocene = []
        maxECTS = 0
        doneECTS = 0

        entry = []
        subjects_help = Predmet_leto.objects.filter(studijsko_leto=vpis.leto_vpisa,id__in=predmeti_id)
        subjects = [x.predmet for x in subjects_help]

        for subject in subjects:
            xs = [subject]
            izpitus = []
            maxECTS += subject.kreditne_tocke
            for vpis_dup in vpisi:
                c = Predmet_leto.objects.filter(predmet=subject,studijsko_leto=vpis_dup.leto_vpisa)
                p_a = [x.id for x in c]
                b = Evidenca_student.objects.filter(student=student,predmet__in=p_a)
                p_l = [x.predmet for x in b]
                izpiti = Izpit.objects.filter(predmet__in=p_l).order_by('datum')
                izpit_student = Evidenca_izpit.objects.filter(student=student,izpit__in=izpiti,status=True)
                for i,izpit in enumerate(izpit_student):
                    container = []
                    container.append(izpit)
                    extra_letos = str(i+1)
                    print(extra_letos)
                    container.append(extra_letos)
                    izpitus.append(container)
            ####
            stevilo_opravljanj = SteviloOpravljanj.objects.get(student=student,predmet=subject)
            for i,izpit in enumerate(izpitus):
                if i+1 <= 3:
                    extra_op = '(' + str(i+1) + '-0)'
                    izpit.append(extra_op)
                    print(extra_op)
                elif i+1 > 3:
                    extra_op = '(' + str(i+1) + '-' + str(stevilo_opravljanj.stPop) + ')'
                    print(extra_op)
                    izpit.append(extra_op)
            
            if len(izpitus) > 0:
                last_izpit = izpitus[-1:][0][0]
                print(last_izpit)
                if last_izpit.ocena is not None:
                    ocena = last_izpit.ocena
                    if ocena > 5:
                        ocene.append(ocena)
                        doneECTS += subject.kreditne_tocke
                xs.append(izpitus)
            entry.append(xs)
        avg = 0
        if len(ocene) is not 0:
            avg = sum(ocene) / len(ocene)
        ECTS = str(doneECTS) + "/" + str(maxECTS)
        extra = {'avg' : avg, 'ECTS' : ECTS}
        data[s].append(entry)   
        data[s].append(extra)

    if studijsko_leto is '0' or studijsko_leto is None:
        pass
    else:
        mark = 0
        if studijsko_leto == '1':
            for vpis in data:
                if vpis[0].leto_vpisa.studijsko_leto == '2014/2015':
                    data = [vpis]
                    mark = 1
        if studijsko_leto == '2':
            for vpis in data:
                if vpis[0].leto_vpisa.studijsko_leto == '2015/2016':
                    data = [vpis]
                    mark = 1
        if studijsko_leto == '3':
            for vpis in data:
                if vpis[0].leto_vpisa.studijsko_leto == '2016/2017':
                    data = [vpis]
                    mark = 1
        if studijsko_leto == '4':
            for vpis in data:
                if vpis[0].leto_vpisa.studijsko_leto == '2017/2018':
                    data = [vpis]
                    mark = 1
        if studijsko_leto == '5':
            for vpis in data:
                if vpis[0].leto_vpisa.studijsko_leto == '2018/2019':
                    data = [vpis]
                    mark = 1
        
        if mark == 0:
            data = []
    return render(request, 'indeks.html', {'student' : student, 'data' : data, 'polaganje' : polaganja})

def indeks_detail(request):
    user = request.user
    student = Student.objects.get(pk=user.id)
    izpiti_orig = Evidenca_izpit.objects.filter(student=student,ocena__gt=5).order_by('izpit')
    
    rev = izpiti_orig[::-1]
    seen = {}
    new_list = []
    for izpit in rev:
        if izpit.izpit.predmet.predmet not in seen:
            new_list.append(izpit)
            seen[izpit.izpit.predmet.predmet] = True 
    izpiti_orig = new_list[::-1]

    sumECTS = 0
    for izpit in izpiti_orig:
        sumECTS += izpit.izpit.predmet.predmet.kreditne_tocke

    stOpravljanj = []
    for izpit in izpiti_orig:
        st = SteviloOpravljanj.objects.get(student=student,predmet=izpit.izpit.predmet.predmet)
        stOpravljanj.append(st)
    
    vpisi = Vpis.objects.filter(student=student)
    data = [[x] for x in vpisi]
    predmeti_evidenca = Evidenca_student.objects.filter(student=student)
    predmeti_id = [x.predmet.id for x in predmeti_evidenca]
    izpit_evidenca = Evidenca_izpit.objects.filter(student=student)
    ocene_global = []
    global_ECTS = []
    for i,vpis in enumerate(vpisi):
        subjects = Predmet_leto.objects.filter(studijsko_leto=vpis.leto_vpisa,id__in=predmeti_id)
        izpiti = Izpit.objects.filter(predmet__in=subjects).order_by('datum')
        predmeti = [[x]for x in subjects]
        ocene = []
        doneECTS = 0
        for predmet in predmeti:
            izpiti_studenta = izpiti.filter(predmet_id=predmet[0].id)
            izpiti_evidenca = izpit_evidenca.filter(izpit__in=izpiti_studenta).order_by('izpit__datum')
            if izpiti_evidenca.last() is not None and izpiti_evidenca.last().ocena is not None:
                ocena = izpiti_evidenca.last().ocena
                if ocena > 5:
                    ocene.append(ocena)
                    ocene_global.append(ocena)
                    doneECTS += predmet[0].predmet.kreditne_tocke
                    global_ECTS.append(predmet[0].predmet.kreditne_tocke)
        avg = 0
        if len(ocene) is not 0:
            avg = sum(ocene) / len(ocene)
        extra = {'avg' : avg, 'ECTS' : doneECTS}
        data[i].append(extra)
        data[i].append(ocene)

    avg = 0
    if len(ocene_global) is not 0:
        avg = sum(ocene_global) / len (ocene_global)
    go = {'len' : len(izpiti_orig),
                'ECTS': sumECTS,
                'avg' : avg}

    return render(request, 'elektronski_indeks.html', {'student' : student, 'izpiti' : izpiti_orig, 'data' : data, 'opr' : stOpravljanj, 'globalOcena' : go})


@login_required
@user_passes_test(is_student)
def naroci_potrdilo(request):

    if request.method == 'GET':
        form = NarociloForms()
        vpis = Vpis.objects.filter(student_id=request.user.pk).last()
        vrsta = None
        if vpis is not None:
            vrsta = vpis.vrsta
        student = Student.objects.get(pk=request.user.pk)
        narocila = NarociloPotrdil.objects.filter(student=student, status=True).count()
        omogoci = False
        if narocila == 0:
            omogoci = True
        return render(request, 'naroci_potrdilo.html', {'form':form, 'vrsta':vrsta, 'omogoci': omogoci})
    else:
        form = NarociloForms(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            st = int(data["stevilo"])
            student = Student.objects.get(pk=request.user.pk)
            narocilo = NarociloPotrdil(student=student, stevilo=st).save()
        return redirect(reverse('naroci_potrdilo'))