from reportlab.lib.pagesizes import letter, A4
from Studis.models import Student, Coin, Predmet_leto, Predmet, Evidenca_student
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, ListFlowable, BaseDocTemplate, PageTemplate, Frame, Spacer, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle, ListStyle
from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_RIGHT
from reportlab.lib.units import inch
from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.lib import colors
from reportlab.lib.colors import red, black, yellow
import datetime
from reportlab.pdfbase.pdfmetrics import stringWidth


class NumberedCanvas(canvas.Canvas):
    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self._saved_page_states = []

    def showPage(self):
        self._saved_page_states.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        """add page info to each page (page x of y)"""
        num_pages = len(self._saved_page_states)
        for state in self._saved_page_states:
            self.__dict__.update(state)
            self.draw_page_number(num_pages)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_page_number(self, page_count):
        # Change the position of this to wherever you want the page number to be
        self.drawRightString(115 * mm, 0.2 * mm + (0.4 * inch),
                             "%d od %d" % (self._pageNumber, page_count))


class MyPrint:
    def __init__(self, buffer, pagesize, pk):
        self.pk = pk
        self.buffer = buffer
        if pagesize == 'A4':
            self.pagesize = A4
        elif pagesize == 'Letter':
            self.pagesize = letter
        self.width, self.height = self.pagesize

    @staticmethod
    def _header_footer(canvas, doc):
        # Save the state of our canvas so we can draw on it
        canvas.saveState()
        styles = getSampleStyleSheet()

        normaln = styles['Normal']
        normaln.fontName = 'Arial'

        # Header
        header = Paragraph('Fakulteta za računalništvo in informatiko ', normaln)
        w, h = header.wrap(doc.width, doc.topMargin)
        header.drawOn(canvas, inch * 3, doc.height + doc.topMargin - h)

        date = datetime.date.today().strftime("%B %d, %Y")
        # Footer
        footer = Paragraph('Datum: ' + date, normaln)
        w, h = footer.wrap(doc.width, doc.bottomMargin)
        footer.drawOn(canvas, inch / 4, h)

        # Release the canvas
        canvas.restoreState()

    def print_users(self):
        buffer = self.buffer
        doc = SimpleDocTemplate(buffer,
                                rightMargin=inch / 4,
                                leftMargin=inch,
                                topMargin=inch / 2,
                                bottomMargin=inch / 4,
                                pagesize=self.pagesize)

        # Our container for 'Flowable' objects
        elements = []
        # A large collection of style sheets pre-made for us
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='centered', alignment=TA_CENTER))
        naslov1 = styles['Heading1']
        naslov1.alignment = TA_CENTER
        naslov1.fontName = 'Arial'

        naslov2 = styles['Heading2']
        naslov2.alignment = TA_LEFT
        naslov2.fontName = 'Arial'

        normaln = styles['Normal']
        normaln.fontName = 'Arial'

        student = Student.objects.select_related('posta_prejem', 'posta_naslov').get(pk=self.pk)
        vpisi = student.vpis_set.all().select_related()

        elements.append(Spacer(1, 0.15 * inch))
        elements.append(Paragraph('Podatki o študentu - ' + student.vpisna, naslov1))
        elements.append(Spacer(1, 0.125*inch))

        elements.append(Paragraph('Splošni podatki',naslov2))
        elements.append(Spacer(1, 0.1 * inch))
        elements.append(Paragraph('Ime: ' + student.first_name, normaln))
        elements.append(Paragraph('Priimek: ' + student.last_name, normaln))
        elements.append(Paragraph('Vpisna številka: ' + student.vpisna, normaln))
        elements.append(Paragraph('Univerzitetni e-mail: ' + student.email, normaln))
        elements.append(Paragraph('Telefonska številka: ' + student.tel, normaln))
        elements.append(Paragraph('Osebni e-mail: ' + student.osebni_email, normaln))

        elements.append(Spacer(1, 0.125 * inch))
        elements.append(Paragraph('Naslov stalnega prebivališča', naslov2))
        elements.append(Spacer(1, 0.1 * inch))
        elements.append(Paragraph('Država: ' + str(student.posta_naslov.drzava), normaln))
        elements.append(Paragraph('Ulica: ' + student.ulica_naslov, normaln))
        elements.append(Paragraph('Pošta: ' + student.posta_naslov.postna_stevilka, normaln))
        elements.append(Paragraph('Kraj: ' + student.posta_naslov.kraj, normaln))

        elements.append(Spacer(1, 0.125 * inch))
        elements.append(Paragraph('Naslov za dostavo pošte', naslov2))
        elements.append(Spacer(1, 0.1 * inch))
        elements.append(Paragraph('Država: ' + str(student.posta_prejem.drzava), normaln))
        elements.append(Paragraph('Ulica: ' + student.ulica_prejem, normaln))
        elements.append(Paragraph('Pošta: ' + student.posta_prejem.postna_stevilka, normaln))
        elements.append(Paragraph('Kraj: ' + student.posta_prejem.kraj, normaln))

        elements.append(Spacer(1, 0.125 * inch))
        elements.append(Paragraph('Podatki o vpisih', naslov2))
        elements.append(Spacer(1, 0.1 * inch))

        for i, vpis in enumerate(vpisi):
            elements.append(Paragraph('Študijsko leto: ' + vpis.leto_vpisa.studijsko_leto, normaln))
            elements.append(Paragraph('Letnik: ' + str(vpis.letnik), normaln))
            elements.append(Paragraph('Šudijski program: ' + vpis.smer.sifra + ' - ' + vpis.smer.ime, normaln))
            elements.append(Paragraph('Vrsta vpisa: ' + vpis.vrsta.sifra + ' - ' + vpis.vrsta.vrsta, normaln))
            elements.append(Paragraph('Način študija: ' + vpis.tip.sifra + ' - ' + vpis.tip.nacin, normaln))
            elements.append(Spacer(1, 0.1*inch))

        doc.build(elements, onFirstPage=self._header_footer, onLaterPages=self._header_footer,
                  canvasmaker=NumberedCanvas)
        # Get the value of the BytesIO buffer and write it to the response.
        pdf = buffer.getvalue()
        buffer.close()
        return pdf
