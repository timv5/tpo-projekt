# Generated by Django 2.0.3 on 2018-05-30 16:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Studis', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Nosilci',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nosilec', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Studis.Profesor')),
                ('predmet_leto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Studis.Predmet_leto')),
            ],
            options={
                'ordering': ('predmet_leto',),
            },
        ),
    ]
