from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

class Coin(models.Model):
    student = models.ForeignKey('Student', on_delete=models.CASCADE)
    vrsta = models.ForeignKey('Vrsta_vpis', on_delete=models.CASCADE, blank = False)
    oblika = models.ForeignKey('Oblika_studij', on_delete=models.CASCADE, blank = False)
    nacin = models.ForeignKey('Nacin_studij', on_delete=models.CASCADE, blank = False)
    smer = models.ForeignKey('Smer', on_delete=models.CASCADE, blank = False)
    letnik = models.PositiveIntegerField(blank = False)
    pravica_izbire = models.BooleanField(blank = False)
    used = models.BooleanField(default = False)
    # kolicina = models.PositiveIntegerField()
    # datum = models.DateField()

class Evidenca_Vpis(models.Model):
    studijsko_leto = models.ForeignKey('Studijsko_leto', on_delete=models.CASCADE)
    st_vpisov = models.PositiveIntegerField(default=1)

class Studijsko_leto(models.Model):
    studijsko_leto = models.CharField(max_length = 9)

    def __str__(self):
        return "{}".format(self.studijsko_leto)

class Izpit(models.Model):
    predmet=models.ForeignKey('Predmet_leto', on_delete=models.CASCADE)
    datum=models.DateField(blank=False, null= False)
    cas = models.TimeField(blank=True, null= True)
    locked = models.BooleanField(default=False)
    izredni=models.BooleanField()
    prostor = models.CharField(max_length=100, blank=True, null= True)
    izprasevalec = models.ForeignKey('Profesor', on_delete=models.CASCADE, blank=True, null=True)
    def __str__(self):
        return self.predmet.predmet.naslov_predmeta

    class Meta:
        ordering = ('datum',)


class Smer(models.Model): #vs,uni
    smer=models.CharField(unique=True,max_length=3)
    ime = models.CharField(max_length=50)
    sifra = models.CharField(max_length=7, blank=True, null=True)
    def __str__(self):
        return "{} - {}".format(self.sifra,self.ime)


class Nacin_studij(models.Model): #redni,izredni
    nacin=models.CharField(max_length=10,unique=True)
    sifra = models.CharField(max_length=1)
    def __str__(self):
        return "{} - {}".format(self.sifra,self.nacin)

class Oblika_studij(models.Model):
    oblika = models.CharField(max_length=20)
    sifra = models.CharField(max_length=1)
    def __str__(self):
        return "{} - {}".format(self.sifra,self.oblika)

class Vrsta_vpis(models.Model): # prvi vpis, ponavljanje, nadaljevanje, vpis po merilih...
    vrsta=models.CharField(max_length=50,unique=True)
    sifra = models.CharField(max_length=2)
    def __str__(self):
        return "{} - {}".format(self.sifra,self.vrsta)


class Evidenca_izpit(models.Model):
    student=models.ForeignKey('Student', on_delete=models.CASCADE)
    izpit=models.ForeignKey('Izpit', on_delete=models.CASCADE)
    status=models.BooleanField(default=True)
    ocena = models.PositiveIntegerField(blank=True, null=True)
    ocena_pisni = models.PositiveIntegerField(blank=True, null=True)
    tocke = models.PositiveIntegerField(blank=True, null=True)
    datum_odjave=models.CharField(max_length=100, blank=True, null= True)
    odjavitelj=models.CharField(max_length=100, blank=True, null= True)
    def __str__(self):
        return self.student.last_name+" "+self.izpit.predmet.predmet.naslov_predmeta


class Vpis(models.Model):
    letnik=models.PositiveIntegerField()
    leto_vpisa=models.ForeignKey('Studijsko_leto', on_delete=models.CASCADE)
    potrjen=models.BooleanField(blank=False, null=False, default=False)
    student=models.ForeignKey('Student', on_delete=models.CASCADE)
    smer=models.ForeignKey('Smer', on_delete=models.CASCADE)
    tip=models.ForeignKey('Nacin_studij', on_delete=models.CASCADE)
    vrsta=models.ForeignKey('Vrsta_vpis', on_delete=models.CASCADE)

    class Meta:
        ordering = ('leto_vpisa',)


class Evidenca_student(models.Model):
    student=models.ForeignKey('Student', on_delete=models.CASCADE)
    predmet=models.ForeignKey('Predmet_leto', on_delete=models.CASCADE)
    ocena=models.PositiveIntegerField(blank=True, null=True)
    letnik_predmeta=models.PositiveIntegerField()

    def __str__(self):
        # Če nima ocene, vrne v izpisu presledek
        return "{} {}".format(self.predmet.predmet.naslov_predmeta,str(self.ocena or ' '))



class Evidenca_profesor(models.Model):
    profesor=models.ForeignKey('Profesor',on_delete=models.CASCADE)
    predmet=models.ForeignKey('Predmet', on_delete=models.CASCADE)

    def __str__(self):
        return self.profesor.last_name+" "+self.predmet.naslov_predmeta


class Leto(models.Model):
    st_narejenih=models.PositiveIntegerField()
    studijsko_leto=models.CharField(max_length=25) #nevemo
    evidenca_student=models.ForeignKey('Evidenca_student', on_delete=models.CASCADE)

    def __str__(self):
        return self.evidenca_student.student.last_name+" "+self.studijsko_leto


class PotrdiloVpisa(models.Model):
    stevilka=models.PositiveIntegerField()


class SteviloOpravljanj(models.Model):
    student=models.ForeignKey('Student', on_delete=models.CASCADE)
    predmet=models.ForeignKey('Predmet',on_delete=models.CASCADE)
    stVseh=models.PositiveIntegerField(default=0)
    stPop=models.PositiveIntegerField(default=0)


class NarociloPotrdil(models.Model):
    student = models.ForeignKey('Student', on_delete=models.CASCADE)
    stevilo = models.IntegerField(default=1,
        validators=[MaxValueValidator(6), MinValueValidator(1)]
    )

    status = models.BooleanField(blank=False, null=False, default=True)
    datum_narocila = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.student + " " + self.stevilo + " " + self.status