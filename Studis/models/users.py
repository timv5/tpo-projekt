from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True
    
    def _create_user(self, email, password=None, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        extra_fields.setdefault('is_staff',False)
        extra_fields.setdefault('is_superuser',False)
        user = self.model(
            email=self.normalize_email(email),
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)

""" 
Spisek vseh fieldov, ki jih AbstractUser prinaša : 
https://docs.djangoproject.com/en/2.0/ref/contrib/auth/#django.contrib.auth.models.User

Vključuje username(deleted), first_name, last_name, email, password, groups, user_permissions, is_staff,
is_active, is_superuser, last_login in date_joined

Use email as username for Django authentication.
1.Removing the username field.
2.Making the email field required and unique.
3.Telling Django that you are going to use the email field as the USERNAME_FIELD.
4.Removing the email field from the REQUIRED_FIELDS settings (it is automatically included as USERNAME_FIELD).
 """
class User(AbstractUser):
    username = None
    email = models.EmailField(unique=True,null=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()
    def __str__(self):
        return self.email

class Student(User):
    vpisna = models.CharField(unique=True,max_length=8)
    osebni_email = models.CharField(unique=True,blank=True,max_length=255)
    emso = models.CharField(max_length=13,blank=True, null=True)
    spol = models.ForeignKey('Spol', on_delete = models.CASCADE, blank=True, null= True)
    drzava= models.ForeignKey('Drzava', on_delete = models.CASCADE, related_name="user_to_drzava", blank=True, null= True)
    obcina_rojstva=models.ForeignKey('Obcina', on_delete = models.CASCADE, blank=True, null= True)
    kraj_rojstva=models.CharField(max_length=100, blank=True, null=True)
    drzavljanstvo=models.ForeignKey('Drzava', on_delete = models.CASCADE, related_name="user_to_drzavljanstvo", blank=True, null= True)
    tel = models.CharField(max_length=255,blank=True)
    ulica_naslov = models.CharField(max_length=255,blank=True)
    ulica_prejem = models.CharField(max_length=255,blank=True)
    datum_rojstva = models.DateField(blank=True, null = True)
    posta_prejem = models.ForeignKey('Posta', on_delete = models.CASCADE, related_name="user_to_posta_prejem", blank=True, null= True)
    posta_naslov = models.ForeignKey('Posta', on_delete = models.CASCADE, related_name="user_to_posta_naslov", blank=True, null = True)
    obcina_prejem = models.ForeignKey('Obcina', on_delete=models.CASCADE, related_name="user_to_obcina_prejem", blank=True, null=True)
    obcina_naslov = models.ForeignKey('Obcina', on_delete=models.CASCADE, related_name="user_to_obcina_naslov", blank=True, null=True)
    drzava_naslov= models.ForeignKey('Drzava', on_delete = models.CASCADE, related_name="user_to_drzava_naslov",blank=True, null= True)
    drzava_prejem= models.ForeignKey('Drzava', on_delete = models.CASCADE, related_name="user_to_drzava_prejem",blank=True, null= True)

    class Meta:
        verbose_name = 'Student'
        verbose_name_plural = 'Students'
        ordering = ('last_name',)
        permissions = (("lahko_vidi_seznam_studentov", "Uporabnik ima dostop do seznama vseh studentov"),)

    def __str__(self):
        return "{}".format(self.email) # pylint: disable=no-member

class Posta(models.Model):
    postna_stevilka = models.CharField(primary_key=True, unique=True, max_length=4)
    drzava = models.ForeignKey('Drzava', on_delete = models.CASCADE, blank=True, null= True)
    kraj = models.CharField(max_length=128)
    class Meta:
        verbose_name = 'Pošta'
        verbose_name_plural = 'Pošte'
    def __str__(self):
        return "{} - {}, {}".format(self.postna_stevilka,self.kraj,self.drzava.ime_drzave)

class Referent(User):
    sifra = models.CharField(unique=True, max_length=8)
    datum_rojstva = models.DateTimeField()
    emso = models.CharField(max_length=13, unique=True, null=True)
    drzavljanstvo = models.CharField(max_length=255)
    tel = models.CharField(max_length=255)
    ulica = models.CharField(max_length=255,blank=True)
    posta = models.ForeignKey('Posta', on_delete=models.CASCADE,blank=True)

    def __str__(self):
        return "{}".format(self.sifra)

class Profesor(User):
    sifra = models.CharField(unique=True, max_length=8)
    datum_rojstva = models.DateTimeField()
    emso = models.CharField(max_length=13, unique=True, null=True)
    drzavljanstvo = models.CharField(max_length=255)
    tel = models.CharField(max_length=255)
    ulica = models.CharField(max_length=255)
    posta = models.ForeignKey('Posta', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Professor"
        verbose_name_plural = "Professors"

    def __str__(self):
        return "{}".format(self.email)

class Skrbnik(User):
    sifra = models.CharField(unique=True, max_length=8)

class Drzava(models.Model):
    dvomestna_koda=models.CharField(max_length=2,default='aa')
    tromestna_oznaka=models.CharField(max_length=3,default='aaa')
    num_oznaka=models.CharField(max_length=5, default='11')
    ime_drzave=models.CharField(max_length=255)

    def __str__(self):
        return "{} - {}".format(self.num_oznaka,self.ime_drzave)

class Spol(models.Model):
    spol=models.CharField(max_length=6,default='moški')

    def __str__(self):
        return "{}".format(self.spol)

class Obcina(models.Model):
    ime = models.CharField(max_length=50)
    sifra = models.IntegerField()
    drzava = models.ForeignKey('Drzava', on_delete = models.CASCADE, blank=True, null= True)
    def __str__(self):
        return "{} - {}".format(self.sifra,self.ime)