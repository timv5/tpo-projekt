from django.db import models
from Studis.validators import validate_predmet_prof, validate_predmet_tip
from Studis.error_messages import *


class Predmet(models.Model):
    sifra=models.IntegerField(unique=True)
    naslov_predmeta=models.CharField(max_length=128)
    smer=models.ForeignKey('Smer', on_delete=models.CASCADE)
    kreditne_tocke = models.IntegerField(default=6)

    class Meta:
        verbose_name = 'Predmet'
        verbose_name_plural = 'Predmeti'
        ordering = ('naslov_predmeta',)

    def __str__(self):
        return "{} ({})".format(self.naslov_predmeta,self.sifra)

class Predmet_leto(models.Model):
    predmet = models.ForeignKey('Predmet', on_delete=models.CASCADE)
    studijsko_leto = models.ForeignKey('Studijsko_leto', on_delete=models.CASCADE)
    profesor = models.ForeignKey('Profesor', on_delete=models.CASCADE)

    class Meta:
        ordering = ('predmet',)

    def __str__(self):
        return "{}".format(self.predmet.naslov_predmeta)


class Nosilci(models.Model):
    predmet_leto = models.ForeignKey('Predmet_leto', on_delete=models.CASCADE)
    nosilec = models.ForeignKey('Profesor', on_delete=models.CASCADE)

    class Meta:
        ordering = ('predmet_leto',)

    def __str__(self):
        return "{}".format(self.predmet_leto.predmet.naslov_predmeta)


class Modulski_Predmet(Predmet):
    modul = models.ForeignKey('Modul', on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.naslov_predmeta)
        
class Splosno_izbirni(Predmet):
    def __str__(self):
        return "{}".format(self.naslov_predmeta)

class Strokovni_izbirni(Predmet):
    def __str__(self):
        return "{}".format(self.naslov_predmeta)

class Splosni_Predmet(Predmet):
    letnik = models.IntegerField()
    def __str__(self):
        return "{} {}".format(self.naslov_predmeta, self.sifra)

class Modul(models.Model):
    sifra=models.CharField(unique=True, max_length=8)
    ime_modula = models.CharField(max_length=50)

    def __str__(self):
        return "{}".format(self.ime_modula)
