from urllib.parse import urlencode
from django import template
from django.contrib.auth.models import Group
register = template.Library()

@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    query = context['request'].GET.dict()
    query.update(kwargs)
    return urlencode(query)

@register.filter(name='has_group')
def has_group(user, group_name): 
    group = Group.objects.get(name=group_name) 
    return True if group in user.groups.all() else False

@register.filter(name="index")
def index(sequence, position):
    if sequence is None:
        return None
    if position is None:
        return None
    if len(sequence) <= position:
        return None
    return sequence[position]

@register.filter(name='add_readonly')
def add_readonly(field, css):
    attrs = {}
    definition = css.split(',')
    #print("definition: ",definition)
    #print("css: ",css)
    #print("field: ",field)
    for d in definition:
        if ':' not in d:
            attrs['class'] = d
        else:
            key, val = d.split(':')
            attrs[key] = val
    attrs['readonly'] = ''
    return field.as_widget(attrs=attrs)