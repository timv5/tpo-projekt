from datetime import datetime
from Studis.models import Evidenca_student, Studijsko_leto


"""
Funkcija, ki vrne trenutno študijkso leto
"""
def studijskoLeto(prev=0):
    now = datetime.today()
    this_year = now.year-prev
    this_month = now.month
    studijsko_leto = ""
    if this_month > 9:
        studijsko_leto = str(this_year) + "/" + str(this_year+1)
    else:
        studijsko_leto = str(this_year-1) + "/" + str(this_year)

    studijsko_leto_tab = Studijsko_leto.objects.filter(studijsko_leto=studijsko_leto).last()

    return studijsko_leto_tab

"""
Funkcija, ki vrne omejitve pri izbiri predmetov glede na opravljene obveznosti iz prejšnjih letnikov
"""
def izracunajMejoKT(letnik, student, vrsta):
    prev_letnik = letnik - 1
    now_letnik = letnik
    if prev_letnik == 0:
        prev_letnik = 1

    if vrsta == "02":
        prev_letnik = letnik

    narejeni_now = None
    if vrsta == "01":
        narejeni_now = Evidenca_student.objects.prefetch_related("predmet", "predmet__predmet").filter(student=student,
                                                                                                       letnik_predmeta=now_letnik).all()

    """
        Preveri če je to prvi vpis
    """
    if now_letnik == 1 and vrsta == "01":
        return -1, 0
    """
    if Evidenca_student.objects.prefetch_related("predmet", "predmet__predmet").filter(student=student).count() == 0:
        return -1, 0
    """
    """
        Preveri če ponavlja 3. letnik -> nujno mora izrati vse predmete za nazaj in ne more izbrati novega predmeta
    """

    if vrsta == "02" and letnik == 3:
        return -2, 0

    narejeni_prev = Evidenca_student.objects.prefetch_related("predmet", "predmet__predmet").filter(student=student,
                                                                                               ocena__gte=6,
                                                                                               letnik_predmeta=prev_letnik).all()
    """
    TO-DO
        Dodaj preverjanje vrednosti točk predmetov za naprej
    """
    sumKT_prev = 0
    sumKT_now = 0

    for predmet_leto in narejeni_prev:
        sumKT_prev = sumKT_prev + predmet_leto.predmet.predmet.kreditne_tocke
        print(predmet_leto.predmet.predmet.naslov_predmeta, predmet_leto.ocena)


    if narejeni_now is not None:
        for predmet_leto in narejeni_now:
            print(predmet_leto.predmet.predmet.naslov_predmeta)
            sumKT_now = sumKT_now + predmet_leto.predmet.predmet.kreditne_tocke
            print(predmet_leto.predmet.predmet.naslov_predmeta, predmet_leto.ocena)

    meja = 0

    if sumKT_prev >= 53:
        meja = 60

    elif sumKT_prev >= 48:
        meja = 24

    elif sumKT_prev >= 42:
        meja = 18

    elif sumKT_prev >= 36:
        meja = 6
    """
    if letnik == 3 and sumKT_prev == 60:
        meja = meja - sumKT_now
    """
    print(meja)
    if now_letnik != prev_letnik:
        meja = meja - sumKT_now
    print("ASD:", meja, sumKT_prev, sumKT_now)

    return meja, sumKT_now