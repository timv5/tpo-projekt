from Studis.models import Posta
from Studis.models import Profesor, Student
from django.core.exceptions import ValidationError
from Studis.error_messages import *

def validate_posta(postna_stevilka):
    if not Posta.objects.filter(postna_stevilka=postna_stevilka).exists():
        raise ValidationError(
            INVALID_POSTAL_NUMBER,
            code='Poštna številka ne obstaja'
        )

def validate_tel(tel):
    cifra=tel.isdigit()
    if not cifra or not len(tel) == 9:
        raise ValidationError(
            REQUIRED_RIGHT_NUMBER,
            code='napacna stevilka'
        )
def validate_predmet_tip(tip):
    if(tip<5 and tip>0):
        return True
    else:
        raise ValidationError(
            INVALID_TYPE_SUBJECT,
            code='napacen tip predmeta'
        )
def validate_predmet_prof(profesor):
    if not Profesor.objects.filter(sifra=profesor).exists():
        raise ValidationError(
            INVALID_PROFESOR_NUMBER,
            code='Profesor ne obstaja'
        )
def validate_student(student):
    if not Student.objects.filter(vpisna=student).exists():
        raise ValidationError(
            INVALID_STUDENT_NUMBER,
            code='Student ne obstaja'
        )
def validate_vrsta_vpisa(vrsta):
    vrste_vpisa=["Prvi vpis","Dodatno leto", "Ponavljanje letnika", "Podalšanje statusa študenta", "Vpis po merilih za prehode", ""]
    if not vrste_vpisa.__contains__(vrsta):
        raise ValidationError(
            REQUIRED_VRSTA_VPISA,
            code='Nepravilni vpis'
        )
def validate_letnik(letnik):
    if not (letnik < 4 and letnik > 0):
        raise ValidationError(
            INVALID_LETNIK,
            code='napacen letnik'
        )
def validate_nacin_vpisa(nacin):
    vrste_vpisa = ["Redni", "Izredni","Na daljavo", "e-študij"]
    if not vrste_vpisa.__contains__(nacin):
        raise ValidationError(
            REQUIRED_VRSTA_VPISA,
            code='Nepravilni nacin'
        )
def validate_stevilka(stevilka):
    if not stevilka.isdigit():
        raise ValidationError(
            REQUIRED_DIGIT,
            code='Potrebna stevilka'
        )

def validate_smer(smer):
    smer_vpisa = ["UNI","VS"]
    if not smer_vpisa.__contains__(smer):
        raise ValidationError(
            REQUIRED_SMER_VPISA,
            code='Napacna smer'
        )
