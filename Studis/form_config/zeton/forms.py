from django import forms
import datetime
from Studis.validators import *
from Studis.error_messages import *
from django.db import models

class ZetonForm(forms.Form):
    student = forms.CharField(label='student', max_length=1000,validators=[validate_student],error_messages={'required': INVALID_STUDENT_NUMBER})
    vrsta_vpisa = forms.CharField(label='vrsta_vpisa', validators=[validate_vrsta_vpisa], error_messages={'required': REQUIRED_VRSTA_VPISA})    #redni, izredni
    letnik = forms.IntegerField(label='letnik',validators=[validate_letnik], error_messages={'required': INVALID_LETNIK},min_value=1,max_value=3)
    nacin = forms.CharField(label='nacin',validators=[validate_nacin_vpisa], error_messages={'required': REQUIRED_NACIN_VPISA})
    pravica_izbire = forms.BooleanField(label='pravica_izbire')
    kolicina = forms.CharField(label='kolicina',validators=[validate_stevilka], error_messages={'required': REQUIRED_DIGIT})
    datum = forms.CharField(initial=datetime.datetime.now().strftime("%d.%m.%Y"), label='datum')
    smer = models.ForeignKey('Smer', on_delete=models.CASCADE)

    def clean_datum(self):
        datum= self.cleaned_data.get('datum')
        dt = datetime.datetime.strptime(datum, "%d.%m.%Y")
        print(dt.strftime("%Y-%m-%d"))
        return dt.strftime("%Y-%m-%d")