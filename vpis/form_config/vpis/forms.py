from django import forms
import datetime
import unicodedata
import random
from Studis.error_messages import *
from Studis.models import Posta, Splosni_Predmet, Modulski_Predmet, Strokovni_izbirni, Splosno_izbirni,\
                        Evidenca_student, Student, Drzava, Spol, Predmet_leto, Obcina
from Studis.validators import validate_posta , validate_tel
from Studis.poslovna_logika.poslovna_logika import studijskoLeto, izracunajMejoKT
from django.utils.safestring import mark_safe
from django.conf import settings


class VpisForm(forms.Form):
    vpisna = forms.CharField(label='Vpisna številka', max_length='8', required  =False, disabled=True)
    email = forms.CharField(label='Univerzitetni e-mail', max_length='100', required=False, disabled=True)
    first_name = forms.CharField(label='Ime', max_length=100)
    last_name = forms.CharField(label='Priimek', max_length=100)
    datum_rojstva = forms.DateField(label='Datum rojstva', input_formats=settings.DATE_INPUT_FORMATS) #DD.MM.LLLL 20.5.1994
    kraj_rojstva = forms.CharField(label='Kraj rojstva', max_length=100)
    drzava = forms.ModelChoiceField(label="Država rojstva",queryset=Drzava.objects.all())
    obcina_rojstva = forms.ModelChoiceField(label="Občina rojstva",queryset=Obcina.objects.all() , required=False)
    #Država, občina rojstva npr. Slovenija, Kranj
    #Državljanstvo npr. Slovenija
    drzavljanstvo = forms.ModelChoiceField(label="Državljanstvo",queryset=Drzava.objects.all())

    spol=forms.ModelChoiceField(label='Spol', queryset=Spol.objects.all())
    emso = forms.CharField(label='EMŠO', max_length=13)
    osebni_email = forms.CharField(max_length=255, label="Osebni e-mail", required=False)
    tel = forms.CharField(label='Prenosni telefon', max_length=15,validators=[validate_tel], error_messages={'required': REQUIRED_PHONE_NUMBER}, required=False)
    
    # Stalno prebivališče -->
    RADIO_CHOICES = [['1','Stalno'],['2','Začasno']]
    vrocanje = forms.ChoiceField(label="Naslov za vročanje", widget=forms.RadioSelect(), choices=RADIO_CHOICES, required = False)
    drzava_naslov = forms.ModelChoiceField(label="Država",queryset=Drzava.objects.all())
    ulica_naslov = forms.CharField(label='Ulica', max_length=50)
    posta_naslov = forms.ModelChoiceField(label='Pošta', queryset=Posta.objects.all().prefetch_related("drzava") , required=False)
    obcina_naslov = forms.ModelChoiceField(label="Občina",queryset=Obcina.objects.all() , required=False)
    # # Občina stalnega prebivališča...rabimo šifrante

    # # Začasno prebivališče -->
    # # Oznaka za vročanje pošte...če je checked, potem je naslov začasno prebivališče in ga izpolniš...
    # # Logika na frontend?
    drzava_prejem = forms.ModelChoiceField(label="Država",queryset=Drzava.objects.all(), required = False)
    ulica_prejem = forms.CharField(label='Ulica ', max_length=50, required = False)
    posta_prejem = forms.ModelChoiceField(label='Pošta', queryset=Posta.objects.all().prefetch_related("drzava"), required = False)
    obcina_prejem = forms.ModelChoiceField(label="Občina",queryset=Obcina.objects.all(), required = False)
    # Občina za začasno prebivališče

    # Podatki iz žetonov...
    # 1. Študijski program
    studijski_program = forms.CharField(label="Študijski program", required = False, disabled = True)
    # 2. Smer, kaj je razlika? wtf aja tega ni
    # 3. Kraj izvajanja
    kraj_izvajanja = forms.CharField(label="Kraj izvajanja", required = False, disabled = True)
    # 4. Izbirna skupina...wtf? 
    # 5. Vrsta študija
    # 6. Vrsta vpisa
    vrsta_vpisa = forms.CharField(label="Vrsta vpisa", required = False, disabled = True)
    # 7. Letnik študija
    letnik_studija = forms.CharField(label="Letnik študija", required = False, disabled = True)
    # 8. Način študija
    nacin_studija = forms.CharField(label="Način študija", required = False, disabled = True)
    # 9. Oblika študija
    oblika_studija = forms.CharField(label="Oblika študija", required = False, disabled = True)
    # 10. Študijsko leto v kterga se vpisuješ
    # 11. soglasja...lol
    def clean_emso(self):

        emso = self.cleaned_data.get('emso')

        f = 7
        vsota = 0
        for e in emso[:-1]:
            vsota += f * int(e)
            f -= 1
            if f == 1:
                f = 7
        if (vsota + int(emso[-1])) % 11 != 0:
            raise forms.ValidationError("Napačna oblika emša!")

        return emso



class VpisPredmetiForm(forms.Form):
    splosni_predmet = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Splosni_Predmet.objects.prefetch_related("smer").all(),
                                                     required=False)
    informacijski_sistemi = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Modulski_Predmet.objects.prefetch_related("smer").none(), required=False)
    obvladovanje_informatike = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Modulski_Predmet.objects.prefetch_related("smer").none(), required=False)
    razvoj_programske_opreme = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Modulski_Predmet.objects.prefetch_related("smer").none(), required=False)
    računalniška_omrežja = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Modulski_Predmet.objects.prefetch_related("smer").none(), required=False)
    računalniški_sistemi = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Modulski_Predmet.objects.prefetch_related("smer").none(), required=False)
    algoritmi_in_sistemski_programi = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Modulski_Predmet.objects.prefetch_related("smer").none(), required=False)
    umetna_inteligenca = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Modulski_Predmet.objects.prefetch_related("smer").none(), required=False)
    medijske_tehnologije = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Modulski_Predmet.objects.prefetch_related("smer").none(), required=False)

    #modulski_predmet = forms.ModelMultipleChoiceField(queryset=Modulski_Predmet.objects.none(), required=False)
    strokovno_izbirni = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Strokovni_izbirni.objects.prefetch_related("smer").none(), required=False)
    splosno_izbirni = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Splosno_izbirni.objects.prefetch_related("smer").none(), required=False)
    neopravljeni_splosni_predmet = forms.ModelMultipleChoiceField(queryset=Splosni_Predmet.objects.prefetch_related("smer").none(), disabled=True,
                                                                 required=False)
    neopravljeni_modulski_predmet = forms.ModelMultipleChoiceField(queryset=Modulski_Predmet.objects.prefetch_related("smer").none(),
                                                                 required=False, disabled=True)
    neopravljeni_splosno_izbirni = forms.ModelMultipleChoiceField(queryset=Splosno_izbirni.objects.prefetch_related("smer").none(), required=False,
                                                                disabled=True)
    neopravljeni_strokovno_izbirni = forms.ModelMultipleChoiceField(queryset=Strokovni_izbirni.objects.prefetch_related("smer").none(),
                                                                  required=False, disabled=True)

    def __init__(self, letnik, smer, pk, vrsta, meja, *args, **kwargs):
        studijsko_leto = studijskoLeto()

        predmet_leto = Predmet_leto.objects.prefetch_related("predmet", "profesor", "studijsko_leto").filter(studijsko_leto=studijsko_leto)
        predmet_leto_pk = []
        for predmeti in predmet_leto:
            predmet_leto_pk.append(predmeti.predmet.pk)
            #print(predmeti.predmet.pk)

        student = Student.objects.filter(pk=pk).get()
        narejeni = Evidenca_student.objects.prefetch_related("predmet", "predmet__predmet").filter(student=student, ocena__gte=6)
        ponovno1 = Evidenca_student.objects.prefetch_related("predmet", "predmet__predmet").filter(student=student, ocena=None)
        ponovno2 = Evidenca_student.objects.prefetch_related("predmet", "predmet__predmet").filter(student=student, ocena__lte=5)
        names_to_exclude_opr = [o.predmet.predmet.naslov_predmeta for o in narejeni]
        names_to_exclude_pon1 = [o.predmet.predmet.naslov_predmeta for o in ponovno1]
        names_to_exclude_pon2 = [o.predmet.predmet.naslov_predmeta for o in ponovno2]
        names_to_exclude = names_to_exclude_opr
        names_to_exclude = names_to_exclude + names_to_exclude_pon1
        names_to_exclude = names_to_exclude + names_to_exclude_pon2
        names_to_exclude_pon = names_to_exclude_pon1 + names_to_exclude_pon2
        #print(names_to_exclude_pon)
        tmp_list = names_to_exclude_pon.copy()
        for s1 in names_to_exclude_pon:
            for s2 in names_to_exclude_opr:
                if s1 == s2 or s1 is s2:
                    index = names_to_exclude_pon.index(s1)
                    tmp_list.remove(s1)
        names_to_exclude_pon = tmp_list
        #print(names_to_exclude_opr)
        #print("pon:", names_to_exclude_pon)



        super(VpisPredmetiForm, self).__init__(*args, **kwargs)

        splosniPredmeti =Splosni_Predmet.objects.filter(letnik=letnik, smer=smer, pk__in=predmet_leto_pk).exclude(
            naslov_predmeta__in=names_to_exclude).all().select_related("smer")
        #print("SPLOSNI:", splosniPredmeti)
        self.fields['splosni_predmet'].queryset = splosniPredmeti
        #self.fields['splosni_predmet'].initial = [p for p in splosniPredmeti]

        neopravljeni_splosni_predmeti = Splosni_Predmet.objects.filter(smer=smer).filter(
            naslov_predmeta__in=names_to_exclude_pon).all().select_related("smer")
        self.fields['neopravljeni_splosni_predmet'].queryset = neopravljeni_splosni_predmeti
        self.fields['neopravljeni_splosni_predmet'].initial = [p for p in neopravljeni_splosni_predmeti]

        if letnik == 2 or letnik == 3:
            if meja != 0 and meja != -2:
                self.fields['strokovno_izbirni'].queryset = Strokovni_izbirni.objects.filter(smer=smer, pk__in=predmet_leto_pk).exclude(naslov_predmeta__in=names_to_exclude).all()
                self.fields['splosno_izbirni'].queryset = Splosno_izbirni.objects.filter(smer=smer, pk__in=predmet_leto_pk).exclude(naslov_predmeta__in=names_to_exclude).all()

            self.fields['neopravljeni_splosno_izbirni'].queryset = Splosno_izbirni.objects.filter(smer=smer).filter(
                naslov_predmeta__in=names_to_exclude_pon).all().prefetch_related("smer")
            self.fields['neopravljeni_splosno_izbirni'].initial = [p for p in
                                                                 Splosno_izbirni.objects.filter(smer=smer).filter(
                                                                     naslov_predmeta__in=names_to_exclude_pon).all().prefetch_related("smer")]

            self.fields['neopravljeni_strokovno_izbirni'].queryset = Strokovni_izbirni.objects.filter(smer=smer).filter(
                naslov_predmeta__in=names_to_exclude_pon).all()
            self.fields['neopravljeni_strokovno_izbirni'].initial = [p for p in
                                                                 Strokovni_izbirni.objects.filter(smer=smer).filter(
                                                                     naslov_predmeta__in=names_to_exclude_pon).all()]


        if letnik == 3 and letnik != 2:
            if meja != 0 and meja != -2:
            #self.fields['modulski_predmet'].queryset = Modulski_Predmet.objects.filter(smer=smer).exclude(naslov_predmeta__in=names_to_exclude).all()
                self.fields['informacijski_sistemi'].queryset = Modulski_Predmet.objects.filter(smer=smer, modul__ime_modula='Informacijski sistemi', pk__in=predmet_leto_pk).exclude(
                    naslov_predmeta__in=names_to_exclude).all().select_related("modul", "predmet_ptr", "smer")
                self.fields['obvladovanje_informatike'].queryset = Modulski_Predmet.objects.filter(smer=smer, modul__ime_modula='Obvladovanje informatike', pk__in=predmet_leto_pk).exclude(
                    naslov_predmeta__in=names_to_exclude).all().select_related("modul", "predmet_ptr", "smer")
                self.fields['razvoj_programske_opreme'].queryset = Modulski_Predmet.objects.filter(smer=smer, modul__ime_modula='Razvoj programske opreme', pk__in=predmet_leto_pk).exclude(
                    naslov_predmeta__in=names_to_exclude).all().select_related("modul", "predmet_ptr", "smer")
                self.fields['računalniška_omrežja'].queryset = Modulski_Predmet.objects.filter(smer=smer, modul__ime_modula='Računalniška omrežja', pk__in=predmet_leto_pk).exclude(
                    naslov_predmeta__in=names_to_exclude).all().select_related("modul", "predmet_ptr", "smer")
                self.fields['računalniški_sistemi'].queryset = Modulski_Predmet.objects.filter(smer=smer, modul__ime_modula='Računalniški sistemi',pk__in=predmet_leto_pk).exclude(
                    naslov_predmeta__in=names_to_exclude).all().select_related("modul", "predmet_ptr", "smer")
                self.fields['algoritmi_in_sistemski_programi'].queryset = Modulski_Predmet.objects.filter(smer=smer, modul__ime_modula='Algoritmi in sistemski programi', pk__in=predmet_leto_pk).exclude(
                    naslov_predmeta__in=names_to_exclude).all().select_related("modul", "predmet_ptr", "smer")
                self.fields['umetna_inteligenca'].queryset = Modulski_Predmet.objects.filter(smer=smer, modul__ime_modula='Umetna inteligenca', pk__in=predmet_leto_pk).exclude(
                    naslov_predmeta__in=names_to_exclude).all().select_related("modul", "predmet_ptr", "smer")
                self.fields['medijske_tehnologije'].queryset = Modulski_Predmet.objects.filter(smer=smer, modul__ime_modula='Medijske tehnologije', pk__in=predmet_leto_pk).exclude(
                    naslov_predmeta__in=names_to_exclude).all().select_related("modul", "predmet_ptr", "smer")

            self.fields['neopravljeni_modulski_predmet'].queryset = Modulski_Predmet.objects.filter(smer=smer).filter(
                naslov_predmeta__in=names_to_exclude).all().select_related("modul", "predmet_ptr", "smer")
            self.fields['neopravljeni_modulski_predmet'].initial = [p for p in
                                                                   Modulski_Predmet.objects.filter(smer=smer).filter(
                                                                       naslov_predmeta__in=names_to_exclude_pon).all().select_related("modul", "predmet_ptr", "smer")]