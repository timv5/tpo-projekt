from django import forms
import datetime
from Studis.validators import *
from Studis.error_messages import *
from Studis.models.storitve import Vrsta_vpis, Nacin_studij, Smer, Oblika_studij

class ZetonForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ZetonForm, self).__init__(*args, **kwargs)
        self.fields['smer'].empty_label = None
        self.fields['vrsta'].empty_label = None
        self.fields['nacin'].empty_label = None
        self.fields['oblika'].empty_label = None
    STATUS_CHOICES = (
    (1, ("1. letnik")),
    (2, ("2. letnik")),
    (3, ("3. letnik")))
    smer = forms.ModelChoiceField(queryset=Smer.objects.all(), required= True)
    letnik = forms.ChoiceField(
        required=True, choices=STATUS_CHOICES)
    vrsta = forms.ModelChoiceField(queryset=Vrsta_vpis.objects.all(), required= True)
    nacin = forms.ModelChoiceField(queryset=Nacin_studij.objects.all(), required= True)
    oblika = forms.ModelChoiceField(queryset=Oblika_studij.objects.all(), required= True)
    #kolicina = forms.CharField(label='kolicina',validators=[validate_stevilka], error_messages={'required': REQUIRED_DIGIT})
    #datum = forms.CharField(initial=datetime.datetime.now().strftime("%d.%m.%Y"), label='datum')
    pravica_izbire = forms.BooleanField(label='Pravica izbire', widget=forms.CheckboxInput(attrs={'style': 'display: none;'}), required= False)
    # def clean_datum(self):
    #     datum= self.cleaned_data.get('datum')
    #     dt = datetime.datetime.strptime(datum, "%d.%m.%Y")
    #     print(dt.strftime("%Y-%m-%d"))
    #     return dt.strftime("%Y-%m-%d")