import requests
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .form_config.vpis.forms import VpisForm, VpisPredmetiForm
from .form_config.zeton.forms import ZetonForm
from Studis.models import Student, Evidenca_student, Predmet, Coin, Predmet_leto, Drzava, Vpis, Studijsko_leto, \
    SteviloOpravljanj, Vrsta_vpis
from decorators import *
from django.contrib.auth.decorators import login_required, user_passes_test
from collections import defaultdict
from Studis.poslovna_logika.poslovna_logika import studijskoLeto, izracunajMejoKT


def dodajPredmete(data, zeton, student):
    studijsko_leto = studijskoLeto()
    # leto = Studijsko_leto.objects.all()
    # studijsko_leto=leto[4]
    ponavljanje = Vrsta_vpis.objects.filter(sifra='02').get()
    for tip in data.values():
        for predmet in tip.values():
            letnik = None
            predmet_obj = Predmet.objects.prefetch_related("smer").filter(pk=predmet["id"]).get()
            predmet_leto = Predmet_leto.objects.prefetch_related("predmet").filter(predmet=predmet_obj,
                                                                                   studijsko_leto=studijsko_leto).get()
            #print(predmet_obj)
            if 'letnik' in predmet:
                letnik = predmet["letnik"]
            elif 'modul_id' in predmet:
                letnik = 3
            else:
                letnik = zeton.letnik
            #print(predmet)
            stOpravljanj = SteviloOpravljanj.objects.filter(student=student, predmet=predmet_leto.predmet).last()
            if stOpravljanj is None:
                SteviloOpravljanj(student=student, predmet=predmet_leto.predmet, stPop=0, stVseh=0).save()
            else:
                if zeton.vrsta == ponavljanje:
                    if zeton.letnik == letnik.letnik_predmeta:
                        opravljal = stOpravljanj.stVseh
                        stOpravljanj.stPop = opravljal
                        stOpravljanj.save()

            evidenca = Evidenca_student(student=student, predmet=predmet_leto,
                                        letnik_predmeta=letnik)
            evidenca.save()

    zeton.used = True
    zeton.save()
    Vpis(letnik=zeton.letnik, leto_vpisa=studijsko_leto, potrjen=False, student=student, smer=zeton.smer,
         tip=zeton.nacin, vrsta=zeton.vrsta).save()
    return redirect('acc_detail')


def preveriModule(predmeti_modul):
    steviloCelotnihModulov = 0
    modulskiKotIzbitni = 0
    for key in predmeti_modul:
        if len(predmeti_modul[key]) == 3:
            steviloCelotnihModulov = steviloCelotnihModulov + 1
        if len(predmeti_modul[key]) == 1:
            modulskiKotIzbitni = modulskiKotIzbitni + 1

    return steviloCelotnihModulov, modulskiKotIzbitni


@login_required
def StudentVpisPredmeti(request):
    errors = []
    zeton = Coin.objects.prefetch_related("student").filter(student=request.user.pk).last()
    letnik = zeton.letnik
    student = Student.objects.prefetch_related("drzava", "posta_naslov", "posta_prejem").filter(
        pk=request.user.pk).get()
    meja, sumKT_now = izracunajMejoKT(letnik, student, zeton.vrsta.sifra)
    print(zeton.vrsta.sifra, meja)

    if zeton.vrsta.sifra == "02" and meja >= 6:
        print("NOT")
        letnik = letnik + 1

    if request.method == 'POST':
        form = VpisPredmetiForm(letnik, zeton.smer.pk, request.user.pk, zeton.vrsta.sifra, meja, request.POST)
        if form.is_valid():
            data = form.cleaned_data

            # meja = izracunajMejoKT(zeton.letnik, student, zeton.vrsta_id)
            sumKT = 0
            prejsnji = 0
            moduli = set()
            predmeti_modul = defaultdict(list)

            splosni_predmet_num = len(form.fields['splosni_predmet'].choices)
            splosni_predmet_selected = len(data["splosni_predmet"])
            splosni_predmet = splosni_predmet_selected == splosni_predmet_num

            strokovni_izbirni_fail = len(data['neopravljeni_strokovno_izbirni'])
            strokovni_izbirni_selectede = len(data['strokovno_izbirni'])
            strokovni_izbirni = False
            if strokovni_izbirni_fail > 0 or strokovni_izbirni_selectede > 0:
                strokovni_izbirni = True

            print(strokovni_izbirni)

            """
                1. Sprehodiš se čez vsa polja v formu
                2. Sprehodiš se čez vse predmete znotraj polj -> to so vsi predmeti ki jih še ni opravljal ali pa naredil
                3. evidenca = ... Priobiš zadnji vnos v evidenco študenta za trenutni predmet (vrne None če tega še ni not)
                4. Preveri če predmet obstaja v evidenci
                    4.1 Zabeleži število obveznosti(KT) iz prejšnjih let
                    4.2 Preveri če je predmet modul
                        4.2.1 V slovar si shranjuj module in njihove predmete
                        4.2.2 V list si shrani modul
                5. Če ni v evidenci predmeta:
                    5.1 Zabeleži število KT izbranih predmetov ki jih še ni opravljal
                    5.2 Preveri če je predmet modul
                        5.2.1 V slovar si shranjuj module in njihove predmete
                        5.2.2 V list si shrani modul

            """

            for tip in data.values():
                for predmet in tip.values():
                    predmet_obj = Predmet_leto.objects.select_related().filter(predmet_id=predmet["id"],
                                                                               studijsko_leto=studijskoLeto()).get()

                    evidenca = Evidenca_student.objects.select_related("predmet", "predmet__predmet").filter(
                        student=student, predmet=predmet_obj).last()
                    # print("evidenca:", evidenca)
                    if evidenca != None:
                        prejsnji = prejsnji + int(predmet["kreditne_tocke"])
                        if 'modul_id' in predmet:
                            predmeti_modul[str(predmet["modul_id"])].append(predmet)
                            moduli.add(predmet["modul_id"])
                            # print("notNone", sumKT, evidenca, evidenca.ocena, evidenca.letnik_predmeta)
                    else:
                        # print(predmet, predmet["kreditne_tocke"])
                        sumKT = sumKT + int(predmet["kreditne_tocke"])
                        if 'modul_id' in predmet:
                            predmeti_modul[str(predmet["modul_id"])].append(predmet)
                            moduli.add(predmet["modul_id"])
                        # print("nism not", predmet)
                    # print(sumKT)
            predmeti_evidenca = None
            """
            Mislim, da tega več ne rabim...za zihr še nism zbrisu haha
            try:
                predmeti_evidenca = Evidenca_student.objects.filter(student=student, letnik_predmeta=zeton.letnik).all().prefetch_related("predmet", "predmet__predmet")
            except:
                print(predmeti_evidenca)
            """
            # print(data)
            # print(sumKT)
            """
               0. Preveri če je to prvi vpis
               0.1 Preveri če ponavlja 3 letnik
               1. Preveri če ponavlja in nima možnosti izbire za naprej
               2. Preveri če nima možnosti izbire modulov in če je izbral več kot 2 modula
                    2.1 Preveri če je izbral modul kot izbirni predmet (torej 3 različne module)
                        2.1.1 Preveri če je izbral 2 celotna modula
                        2.1.2 Drugače zapiši napako
                    2.2 Če je izbral več kot 3 module zapiši napako
               3. Preveri če lahko izbere vse predmete (torej v vrednosti 60KT) -> nujno more izbrat vse splošne predmete
               4. Preveri če je izbral ustrezno število predmetov (torej manj ali enako kot ima mejo KT)
               5. Drugače zapiši napako
            """
            if meja == -1 and sumKT == 60:
                return dodajPredmete(data, zeton, student)
            elif meja == -2 and sumKT == 0:
                return dodajPredmete(data, zeton, student)
            elif meja == 0 and sumKT == meja:
                return dodajPredmete(data, zeton, student)
            elif zeton.pravica_izbire == 0 and len(moduli) > 2 and sumKT <= meja and meja < 60:
                if (len(moduli) == 3):
                    """Število modulskih predmetov izbranih kot izbirni -> lahko je samo en"""
                    steviloCelotnihModulov, modulskiKotIzbitni = preveriModule(predmeti_modul)
                    if steviloCelotnihModulov == 2 and modulskiKotIzbitni == 1:
                        if (sumKT_now + meja == 60) and splosni_predmet:
                            return dodajPredmete(data, zeton, student)
                        else:
                            errors.append("Izbrati morate vse splošne predmete!")
                    elif modulskiKotIzbitni == 3:
                        return dodajPredmete(data, zeton, student)
                    else:
                        errors.append("Nimate pravice do proste izbire modulov!")
                else:
                    errors.append("Nimate pravice do proste izbire modulov!")
            elif zeton.pravica_izbire == 0 and len(moduli) > 2 and sumKT == meja and meja == 60:
                if splosni_predmet:
                    if (len(moduli) == 3):
                        """Število modulskih predmetov izbranih kot izbirni -> lahko je samo en"""
                        steviloCelotnihModulov, modulskiKotIzbitni = preveriModule(predmeti_modul)
                        if steviloCelotnihModulov == 2 and modulskiKotIzbitni == 1:
                            return dodajPredmete(data, zeton, student)
                        else:
                            errors.append("Nimate pravice do proste izbire modulov!")
                    else:
                        errors.append("Nimate pravice do proste izbire modulov!")
                else:
                    errors.append("Izbrati morate vse splošne predmete!")
            elif sumKT == meja and meja == 60 and splosni_predmet is True:
                if zeton.letnik != 2:
                    return dodajPredmete(data, zeton, student)
                elif zeton.letnik == 2 and strokovni_izbirni:
                    return dodajPredmete(data, zeton, student)
                else:
                    errors.append("Izbrati morate vsaj en strokovno izbirni predmet!")
            elif sumKT <= meja and zeton.vrsta.sifra != "01":

                print("NOTR")
                if sumKT_now + meja < 60:
                    return dodajPredmete(data, zeton, student)
                elif splosni_predmet is True and zeton.letnik != 2:
                    return dodajPredmete(data, zeton, student)
                elif splosni_predmet is True and zeton.letnik == 2:
                    if strokovni_izbirni:
                        return dodajPredmete(data, zeton, student)
                    else:
                        errors.append("Izbrati morate vsaj en strokovno izbirni predmet!")
                else:
                    errors.append("Izbrati morate vse splošne predmete!")
            elif sumKT == meja and splosni_predmet is True:
                if zeton.letnik != 2:
                    return dodajPredmete(data, zeton, student)
                elif strokovni_izbirni:
                    return dodajPredmete(data, zeton, student)
                else:
                    errors.append("Izbrati morate vsaj en strokovno izbirni predmet!")
            elif sumKT == meja and splosni_predmet is False:
                errors.append("Izbrati morate vse splošne predmete!")
            else:
                omejitev = meja
                if meja == -1:
                    omejitev = 60
                elif meja == -2:
                    omejitev = 0

                error_KT = "Vsota kreditnih točk izbranih predmetov je: " + str(sumKT)
                errors.append(error_KT)
                error_meja = "Vsota kreditnih točk izbranih predmetov mora biti enaka " + str(omejitev) + "!"
                errors.append(error_meja)

            # r = requests.post('http://127.0.0.1:8000/api/students/', json=data)


    else:
        """   
            TO JE GET obrazca za izbiro predmetov  

            Uporaba:
                VpisPredmetiForm(zeton.letnik, zeton.smer, pk studenta)
        """
        form = VpisPredmetiForm(letnik, zeton.smer.pk, request.user.pk, zeton.vrsta.sifra, meja)

    return render(request, 'vpis_predmeti.html',
                  {'form': form, 'errors': errors, 'letnik': letnik, 'vrsta': zeton.vrsta.sifra})


"""
    Zaenkrat se tukaj statično kreirajo podatki.
    Potrebno bo spisat poslovno logiko za kreiranje vpisne stevilke in maila
"""


@login_required
def StudentVpis(request):
    if request.method == 'POST':
        form = VpisForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            if (data["vrocanje"] == '1'):
                data["posta_prejem"] = data["posta_naslov"]
                data["ulica_prejem"] = data["ulica_naslov"]
                data["drzava_prejem"] = data["drzava_naslov"]
                data["obcina_prejem"] = data["obcina_naslov"]
            Student.objects.filter(pk=request.user.pk).update(first_name=data["first_name"],
                                                              last_name=data["last_name"],
                                                              emso=data["emso"], tel=data["tel"], drzava=data["drzava"],
                                                              spol=data["spol"],
                                                              ulica_naslov=data["ulica_naslov"],
                                                              ulica_prejem=data["ulica_prejem"],
                                                              datum_rojstva=data["datum_rojstva"],
                                                              posta_naslov=data["posta_naslov"],
                                                              posta_prejem=data["posta_prejem"],
                                                              osebni_email=data["osebni_email"],
                                                              drzavljanstvo=data["drzavljanstvo"],
                                                              kraj_rojstva=data["kraj_rojstva"],
                                                              obcina_naslov=data["obcina_naslov"],
                                                              obcina_rojstva=data["obcina_rojstva"],
                                                              obcina_prejem=data["obcina_prejem"],
                                                              drzava_naslov=data["drzava_naslov"],
                                                              drzava_prejem=data["drzava_prejem"]
                                                              )

            # r = requests.post('http://127.0.0.1:8000/api/students/', json=data)

            return redirect('student_vpis_predmet')
    else:
        student = request.user
        try:
            data = Student.objects.prefetch_related("spol", "drzava", "posta_naslov", "posta_prejem").filter(
                pk=student.pk).get()
            coin = Student.objects.get(pk=student.pk).coin_set.filter(used=False).latest('id')
        except:
            print("student ne obstaja")
        # ime=data.drzava.ime_drzave
        # drzava=Drzava.objects.filter(ime_drzave=ime).first()
        # print(data.drzava.id)
        # print(data.datum_rojstva)
        form = VpisForm(initial={'first_name': data.first_name, 'last_name': data.last_name, 'emso': data.emso,
                                 'datum_rojstva': data.datum_rojstva, 'drzava': data.drzava, 'spol': data.spol,
                                 'tel': data.tel, 'ulica_prejem': data.ulica_prejem, 'posta_prejem': data.posta_prejem,
                                 'ulica_naslov': data.ulica_naslov, 'posta_naslov': data.posta_naslov,
                                 'email': data.email,
                                 'vpisna': data.vpisna, 'studijski_program': coin.smer, 'kraj_izvajanja': 'Ljubljana',
                                 'vrsta_vpisa': coin.vrsta, 'letnik_studija': coin.letnik, 'nacin_studija': coin.nacin,
                                 'oblika_studija': coin.oblika, 'osebni_email': data.osebni_email,
                                 'kraj_rojstva': data.kraj_rojstva,
                                 'drzavljanstvo': data.drzavljanstvo, 'drzava_naslov': data.drzava_naslov,
                                 'drzava_prejem': data.drzava_prejem,
                                 'obcina_naslov': data.obcina_naslov,
                                 'obcina_rojstva': data.obcina_rojstva,
                                 'obcina_prejem': data.obcina_prejem,
                                 })
    return render(request, 'vpis.html', {'form': form})