from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path
from . import views as vpis_views

urlpatterns = [
    url(r'^students/$', vpis_views.StudentVpis, name='student_vpis'),
    url(r'^predmet/$', vpis_views.StudentVpisPredmeti, name='student_vpis_predmet'),
]

urlpatterns = format_suffix_patterns(urlpatterns)